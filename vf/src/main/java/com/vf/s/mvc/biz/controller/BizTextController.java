package com.vf.s.mvc.biz.controller;


import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.s.common.model.biz.BizText;

public class BizTextController extends BaseController {
	
	public void index() {
		this.render("list.html");
	}
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String sqlExceptSelect = " from " + BizText.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		Page<BizText> page = BizText.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}

	public void save() {
		BizText bizText = getModel(BizText.class);
		BizText dbBizText=BizText.dao.findById(bizText.getId());
		if(dbBizText!=null) {
			if (bizText.update()) {
				renderSuccess("更新成功！",bizText);
			} else {
				renderError("更新失败！");
			}
		}else {
			if (bizText.save()) {
				renderSuccess("保存成功！",bizText);
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		BizText model = BizText.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}


}
