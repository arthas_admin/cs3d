package com.vf.s.mvc.biz.controller;


import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.model.ZtreeNode;
import com.vf.core.render.RenderLayuiBean;
import com.vf.core.util.PinYinUtil;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizLayer;
import com.vf.s.common.model.scene.BizSceneMenu;

public class BizSceneMenuController extends BaseController{
	
	protected final Log LOG = Log.getLog(getClass());
	
	public void index() {
		this.set("sceneId", this.get("sceneId"));
		render("list.html");
	}
	
	
	public void listData() {
		List<BizSceneMenu> list = BizSceneMenu.dao.find(
				"SELECT T.*, (CASE  WHEN (SELECT COUNT(ID) AS COUNT FROM "+BizSceneMenu.TABLE_NAME+" WHERE PARENTID=T.ID)>0 THEN TRUE ELSE  FALSE END ) AS HAVECHILD   FROM "+BizSceneMenu.TABLE_NAME+"  T WHERE T.SCENEID=? ORDER BY T.SORT ASC",this.getPara("sceneId"));
		RenderLayuiBean renderBean = new RenderLayuiBean();
		renderBean.setCode(0);
		renderBean.setMsg("查询成功");
		renderBean.setData(list);
		renderBean.setCount(list.size());
		this.renderJson(renderBean);
	}
	
	public void itemIndex() {
		this.set("sceneId", this.get("sceneId"));
		this.set("menuId", this.get("menuId"));
		render("item.html");
	}
	
	public void getBizSceneMenuById() throws Exception {
		String id = getPara("id");
		BizSceneMenu bizSceneMenu = BizSceneMenu.dao.findById(id);
		if (bizSceneMenu != null) {
			renderSuccess("成功！", bizSceneMenu);
		} else {
			renderError("数据已经不存在！");
		}
	}
	
	public void getTree() {
		boolean open = true;// 是否展开所有
		boolean ifOnlyLeaf = false;// 是否只选叶子
		if (StrKit.notBlank(getPara("ifOnlyLeaf"))) {// 是否查询所有孩子
			if ("1".equals(getPara("ifOnlyLeaf"))) {
				ifOnlyLeaf = true;
			}
		}
		String sceneId = getPara("sceneId");
		String type = getPara("type");
		List<BizSceneMenu> modelList = BizSceneMenu.dao.getChildrenAllTree(sceneId,type);
		List<ZtreeNode> nodelist = BizSceneMenu.dao.toZTreeNode(modelList, open, ifOnlyLeaf);// 数据库中的菜单
		renderJson(nodelist);
	}
	
	public void save() {
		BizSceneMenu model = getModel(BizSceneMenu.class);
		if (StrKit.notBlank(model.getId())) {
			if(StrKit.isBlank(model.getCode())) {
				model.setCode(PinYinUtil.getPinyin(model.getName()));
			}
			if (model.update()) {
				renderSuccess("更新成功！", model);
			} else {
				renderError("更新失败！");
			}
		} else {
			model.setId(UuidUtil.getUUID());
			if(StrKit.isBlank(model.getCode())) {
				model.setCode(PinYinUtil.getPinyin(model.getName()));
			}
			if (StrKit.isBlank(model.getParentId()))
				model.setParentId("root");
			if (model.save()) {
				renderSuccess("保存成功！", model);
			} else {
				renderError("保存失败！");
			}
		}
	}
	
//	public void getAsyncTreeData() {
//		String id = this.get("id", "root");
//		String sceneId = this.get("sceneId");
//		String menuId = this.get("menuId");
//		String sql = "SELECT a.*,(CASE WHEN (select count(id) from biz_layer where a.id=parentId ) >0 THEN true ELSE false END) AS isParent FROM biz_layer a where a.parentId=? and sceneId=? order by a.sort asc ";
//		List<BizLayer> olist = BizLayer.dao.find(sql, id, sceneId);
//		List<ZtreeNode> nodelist = new ArrayList<ZtreeNode>();
//		for (BizLayer o : olist) {
//			ZtreeNode node = BizLayer.dao.toZTreeNode(o);
//			node.setIsParent(StrKit.equals("1", o.get("isParent") + "") ? true : false);
//			node.setParentId(o.getParentId());
//			BizSceneMenuItem bizSceneMenuItem = BizSceneMenuItem.dao.findFirst(
//					"select * from "+BizSceneMenuItem.TABLE_NAME+" where objId=? and menuId=?",node.getId(),menuId);
//			node.setChecked(bizSceneMenuItem!=null ? true : false);
//			nodelist.add(node);
//		}
//		renderJson(nodelist);
//	}
	
//	public void saveItem() {
//		BizSceneMenuItem model = getModel(BizSceneMenuItem.class);
//		if (StrKit.notBlank(model.getId())) {
//			if (model.update()) {
//				renderSuccess("更新成功！", model);
//			} else {
//				renderError("更新失败！");
//			}
//		} else {
//			model.setId(UuidUtil.getUUID());
//			if (model.save()) {
//				renderSuccess("保存成功！", model);
//			} else {
//				renderError("保存失败！");
//			}
//		}
//	}
//	
//	public void deleteItem() {
//		BizSceneMenuItem bizSceneMenuItem = BizSceneMenuItem.dao.findFirst(
//				"select * from "+BizSceneMenuItem.TABLE_NAME+" where type=? and objId=? and menuId=?",getPara("type"),getPara("objId"),getPara("menuId"));
//		if(bizSceneMenuItem!=null) {
//			if(bizSceneMenuItem.delete()) {
//				renderSuccess("取消成功！", bizSceneMenuItem);
//			}else {
//				renderError("取消失败！");
//			}
//		}else {
//			renderError("数据不存在，请刷新重试！");
//		}
//	}
	
	public void delete() throws Exception {
		String id = getPara("id");
		Page<BizSceneMenu> page = BizSceneMenu.dao.paginate(1, 1, "SELECT * ",
				" FROM " + BizSceneMenu.TABLE_NAME + " WHERE PARENTID=? ", id);
		if (page.getTotalRow() == 0) {
			BizSceneMenu bizSceneMenu = BizSceneMenu.dao.findById(id);
			if (bizSceneMenu != null) {
				if (bizSceneMenu.delete()) {
					renderSuccess("删除成功", bizSceneMenu);
				} else {
					renderError("删除失败！");
				}
			} else {
				renderError("无效操作，数据不存在!");
			}

		} else {
			renderError("当前节点有子节点,不允许删除!");
			return;
		}
	}
	
	public void getSceneMenus() {
		String sceneId = this.get("sceneId");
		List<BizSceneMenu> bizSceneMenus = BizSceneMenu.dao.find("select * from "+BizSceneMenu.TABLE_NAME+" where sceneId=? order by sort",sceneId);
		if(bizSceneMenus!=null && bizSceneMenus.size()>0) {
			renderSuccess("查询成功！", bizSceneMenus);
		}else {
			renderError("场景未配置菜单，请联系管理员！");
		}
	}
	
//	public void getBizSceneModelByCode() throws Exception {
//		String code = getPara("code");
//		BizSceneMenu bizSceneMenu = BizSceneMenu.dao.findFirst("select * from "+BizSceneMenu.TABLE_NAME+" where code=? ",code);
//		if(bizSceneMenu!=null) {
//			List<BizLayer> layers = BizLayer.dao.find("select * from "+BizLayer.TABLE_NAME+" where id in ("
//					+ " select objId from "+BizSceneMenuItem.TABLE_NAME+" where menuId=? ) ",bizSceneMenu.getId());
//			if(layers!=null && layers.size()>0) {
//				renderSuccess("查询成功！", layers);
//			}else {
//				renderError("该模块无配置项！");
//			}
//		}else {
//			renderError("code无效，未查到数据！");
//		}
//	}
	
	public void getAsyncTreeMenu() {
		String id = this.get("id", "root");
		String sceneId = this.get("sceneId");
		String menuId = this.get("menuId");
		String sql = "SELECT a.*,(CASE WHEN (select count(id) from biz_layer where a.id=parentId ) >0 THEN true ELSE false END) AS isParent "
				+ " FROM biz_layer a where id in (select objId from biz_scene_menu_item where menuId=?) and a.parentId=? and sceneId=? order by a.sort asc ";
		List<BizLayer> olist = BizLayer.dao.find(sql, menuId, id, sceneId);
		List<ZtreeNode> nodelist = new ArrayList<ZtreeNode>();
		for (BizLayer o : olist) {
			ZtreeNode node = BizLayer.dao.toZTreeNode(o);
			node.setIsParent(StrKit.equals("1", o.get("isParent") + "") ? true : false);
			node.setParentId(o.getParentId());
			node.setChecked(StrKit.equals("1", o.getShow() + "") ? true : false);
			nodelist.add(node);
		}
		renderJson(nodelist);
	}
	
	
	public void getLayerTree() {
    	String sceneId=this.getPara("sceneId");
    	String parentId=this.getPara("parentId");
		List<BizSceneMenu> list=BizSceneMenu.dao.find("select t.id,t.parentId as pId, t.name, t.code,t.type,t.sort, "
				+ " (CASE  WHEN (SELECT count(id) as count from biz_scene_menu where layerId=t.id  and parentId=? and sceneId=? )>0 THEN true ELSE  false END ) as checked   "
				+ " from biz_layer  t where t.sceneId=? ORDER BY t.sort asc",parentId,sceneId,sceneId);
		this.renderJson(list);
	}
	
	
	public void saveLayerNode() {
    	String sceneId=this.getPara("sceneId");
    	String parentId=this.getPara("parentId");
		String nodes=this.getPara("nodes");
		if(!StrKit.isBlank(nodes)) {
			net.sf.json.JSONArray array=net.sf.json.JSONArray.fromObject(nodes);
			for(Object obj:array) {
				net.sf.json.JSONObject jsonObj=net.sf.json.JSONObject.fromObject(obj);
				if(jsonObj!=null && jsonObj.has("id") && jsonObj.has("name") && jsonObj.has("code") && jsonObj.has("type")  && jsonObj.has("sort") ) {
					BizSceneMenu bizSceneMenu=BizSceneMenu.dao.findFirst("SELECT * FROM "+BizSceneMenu.TABLE_NAME+" WHERE LAYERID=? and parentId=? ",jsonObj.getString("id"),parentId);
					if(bizSceneMenu==null) {
						bizSceneMenu=new BizSceneMenu();
						bizSceneMenu.setId(UuidUtil.getUUID());
						bizSceneMenu.setSceneId(sceneId);
						bizSceneMenu.setParentId(parentId);
						bizSceneMenu.setName(jsonObj.getString("name"));
						bizSceneMenu.setCode(jsonObj.getString("code"));
						bizSceneMenu.setLayerId(jsonObj.getString("id"));
						bizSceneMenu.setType(jsonObj.getString("type"));
						//bizSceneMenu.setSort(()jsonObj.getDouble("sort"));
						bizSceneMenu.save();
					}
				}
			}
			renderSuccess("成功！");
		}else {
			renderError("参数必填！");
		}
	}
	
	
}
