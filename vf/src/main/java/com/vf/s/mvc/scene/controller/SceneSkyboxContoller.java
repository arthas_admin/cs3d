package com.vf.s.mvc.scene.controller;

import java.io.File;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.upload.UploadFile;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.scene.SceneSkybox;

public class SceneSkyboxContoller extends BaseController {
	
	private String rootPath = PathKit.getWebRootPath();
	public void upload() {
		UploadFile uploadFile = getFile();
		
		String type = getPara("type");
		String sceneId = getPara("sceneId");
		
		if(StrKit.isBlank(type)) {
			renderError("上传失败！");
			return;
		}
		
		if(StrKit.isBlank(sceneId)) {
			renderError("上传失败！");
			return;
		}
		
		try {
			String path = "/upload/skybox" + "/" + uploadFile.getFileName().replace(" ", "");
			File file = new File(rootPath+path);
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			if (file.exists()) {
				file.delete();
			}
			uploadFile.getFile().renameTo(file);
			
			SceneSkybox sceneSkybox = SceneSkybox.dao.findFirst("select * from "+SceneSkybox.TABLE_NAME+" where sceneId=? ",sceneId);
			if(sceneSkybox==null) {
				sceneSkybox = new SceneSkybox();
				sceneSkybox.setId(UuidUtil.getUUID());
				sceneSkybox.setSceneId(sceneId);
				sceneSkybox.save();
			}
			if(StrKit.equals("posx", type)) {
				sceneSkybox.setPosx(path);
			}
			if(StrKit.equals("posy", type)) {
				sceneSkybox.setPosy(path);
			}
			if(StrKit.equals("posz", type)) {
				sceneSkybox.setPosz(path);
			}
			if(StrKit.equals("negx", type)) {
				sceneSkybox.setNegx(path);
			}
			if(StrKit.equals("negy", type)) {
				sceneSkybox.setNegy(path);
			}
			if(StrKit.equals("negz", type)) {
				sceneSkybox.setNegz(path);
			}
			if(sceneSkybox.update()) {
				renderSuccess("上传成功！",sceneSkybox);
			}else {
				renderError("上传失败！");
			}
		} catch (Exception e) {
			renderError(e.getMessage());
		}
	}
	
}
