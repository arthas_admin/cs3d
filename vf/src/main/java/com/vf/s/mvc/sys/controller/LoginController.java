/**
 * @开发版权 云南赢中吉洋智能技术有限公司（YNYZ）
 * @项目名称 vfcore
 * @版本信息 v1.0
 * @开发人员 zhous
 * @开发日期 2019-09-23
 * @修订日期
 * @描述  LoginController
 */
package com.vf.s.mvc.sys.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.SessionException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ThreadContext;

import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.jfinal.weixin.sdk.kit.IpKit;
import com.vf.core.controller.BaseController;
import com.vf.s.common.model.sys.SysUser;
import com.vf.s.common.plugins.shiro.ShiroFormAuthenticationInterceptor;
import com.vf.s.common.plugins.shiro.ShiroKit;
import com.vf.s.common.plugins.shiro.SimpleUser;

public class LoginController extends BaseController {

	protected final Log LOG = Log.getLog(getClass());
	

	/**
	 * 显示登录界面
	 */
	public void index() {
    	Subject subject = ThreadContext.getSubject();
    	if(subject.isAuthenticated()){
    		this.redirect(ShiroKit.getSuccessUrl());
    	}else{
    		render("login.html");
    	}
	}
	
	@Before({ShiroFormAuthenticationInterceptor.class})
	public void doLogin() {
		try {
			UsernamePasswordToken token = getAttr("shiroToken");
			Subject subject = SecurityUtils.getSubject();
			ThreadContext.bind(subject);
			Map<String, Object> loginMap = new HashMap<String, Object>();
			SimpleUser simpleUser = null;
			if (subject.isAuthenticated()) {
				subject.logout();
			} else {
				subject.login(token);
			}
			simpleUser=ShiroKit.getLoginUser();
			loginMap.put("token", subject.getSession().getId());
			loginMap.put("user", simpleUser);
			
			try {
				SysUser sysUser=SysUser.dao.findById(simpleUser.getId());
				if(sysUser!=null) {
					sysUser.setLASTLOGINTIME(new Date(System.currentTimeMillis()));
					sysUser.setLASTLOGINIP(IpKit.getRealIp(this.getRequest()));
					sysUser.update();
				}
			}catch (Exception e) {
			}finally {
				this.renderSuccess("登录成功！",loginMap);
			}
        }catch (Exception e) {
        	this.renderError( e.getMessage());
        } 
	}
	
    public void logout() {
        Subject currentUser = SecurityUtils.getSubject();
        try {
            currentUser.logout();
            this.removeSessionAttr("user");
            this.redirect("/login");
        } catch (SessionException ise) {
            LOG.debug("Encountered session exception during logout.  This can generally safely be ignored.", ise);
        } catch (Exception e) {
            LOG.debug("登出发生错误", e);
        }
    }
    
    public void logoutForFront() {
        Subject currentUser = SecurityUtils.getSubject();
        try {
            currentUser.logout();
            this.removeSessionAttr("user");
            this.renderSuccess("注销成功！");
        } catch (SessionException ise) {
            LOG.debug("Encountered session exception during logout.  This can generally safely be ignored.", ise);
        } catch (Exception e) {
            LOG.debug("登出发生错误", e);
        }
    }
	
}
