package com.vf.s.mvc.sys.controller;

import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.model.ZtreeNode;
import com.vf.core.render.RenderLayuiBean;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.sys.SysDic;
import com.vf.s.common.model.sys.SysDicItem;
import com.vf.s.mvc.sys.service.DicService;

public class DicController extends BaseController {

	protected final Log LOG = Log.getLog(getClass());

	@Inject
	private DicService srv;

	public void index() {
		render("list.html");
	}

	/**
	 * 加载菜单列表
	 */
	public void listData() {
		RenderLayuiBean renderBean = srv.listData(this);
		this.renderJson(renderBean);
	}

	/***
	 * 保存
	 */
	public void save() {
		SysDic menu = getModel(SysDic.class);
		if (StrKit.notBlank(menu.getID())) {
			SysDic codeMenu = SysDic.dao.findFirst("SELECT * FROM " + SysDic.TABLE_NAME + " WHERE CODE=? AND ID!=?",
					menu.getCODE(), menu.getID());
			if (codeMenu != null) {
				renderError("标识已存在！");
				return;
			}
			SysDic nameMenu = SysDic.dao.findFirst("SELECT * FROM " + SysDic.TABLE_NAME + " WHERE NAME=? AND ID!=?",
					menu.getNAME(), menu.getID());
			if (nameMenu != null) {
				renderError("名称已存在！");
				return;
			}
			if (menu.update()) {
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			SysDic codeMenu = SysDic.dao.findFirst("SELECT * FROM " + SysDic.TABLE_NAME + " WHERE CODE=?",
					menu.getCODE());
			if (codeMenu != null) {
				renderError("标识已存在！");
				return;
			}
			SysDic nameMenu = SysDic.dao.findFirst("SELECT * FROM " + SysDic.TABLE_NAME + " WHERE NAME=?",
					menu.getNAME());
			if (nameMenu != null) {
				renderError("名称已存在！");
				return;
			}
			menu.setID(UuidUtil.getUUID());
			if (StrKit.isBlank(menu.getPARENTID()))
				menu.setPARENTID("root");
			if (menu.save()) {
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}

	}

	/***
	 * 删除
	 * 
	 * @throws Exception
	 */
	public void delete() throws Exception {
		String id = getPara("id");
		List<SysDic> list = srv.getChildrenByPid(id, "","");
		if (list.size() <= 0) {
			SysDicItem dicItem = SysDicItem.dao.findFirst("SELECT * FROM "+SysDicItem.TABLE_NAME+" WHERE DICID=? ",id);
			if(dicItem!=null) {
				renderError("当前节点拥有字典项信息，不可删除!");
				return;
			}
			SysDic menu = SysDic.dao.findById(id);
			if (menu != null) {
				if (menu.delete()) {
					renderSuccess("删除成功！");
				} else {
					renderError("删除失败！");
				}
			} else {
				renderError("数据不存在,请刷新后再试!");
			}
		} else {
			renderError("当前节点有子节点,不允许删除!");
			return;
		}
	}

	/***
	 * 获取树
	 */
	public void selectTree() {
		Boolean open = true;// 是否展开所有
		Boolean ifOnlyLeaf = false;// 是否只选叶子
		if (StrKit.notBlank(getPara("ifOnlyLeaf"))) {// 是否查询所有孩子
			if ("1".equals(getPara("ifOnlyLeaf"))) {
				ifOnlyLeaf = true;
			}
		}
		List<SysDic> menuList = srv.getChildrenAllTree("root", null,getPara("code"));
		List<ZtreeNode> nodelist = srv.toZTreeNode(menuList, open, ifOnlyLeaf);// 数据库中的菜单
		System.out.println(JsonKit.toJson(nodelist));
		renderJson(nodelist);
	}

	public void listDataForItem() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String keyword = getPara("keyword");
		String dicId = getPara("dicId");
		String sqlExceptSelect = " FROM " + SysDicItem.TABLE_NAME + " o  WHERE DICID='" + dicId + "' ";
		if (!StrKit.isBlank(keyword)) {
			sqlExceptSelect += " AND ( O.NAME LIKE '%" + keyword + "%' OR   O.CODE LIKE '%" + keyword + "%' ) ";
		}
		sqlExceptSelect += "  ORDER BY   O.SORT ASC ";
		Page<SysDicItem> page = SysDicItem.dao.paginate(pageNumber, pageSize, "SELECT * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}

	/***
	 * 保存
	 */
	public void saveItem() {
		SysDicItem menu = getModel(SysDicItem.class);
		if (StrKit.notBlank(menu.getID())) {
			SysDicItem codeMenu = SysDicItem.dao.findFirst(
					"SELECT * FROM " + SysDicItem.TABLE_NAME + " WHERE CODE=? AND ID!=? and DICID=? ", menu.getCODE(),
					menu.getID(), menu.getDICID());
			if (codeMenu != null) {
				renderError("标识已存在！");
				return;
			}
			SysDic nameMenu = SysDic.dao.findFirst(
					"SELECT * FROM " + SysDicItem.TABLE_NAME + " WHERE NAME=? AND ID!=? and DICID=? ", menu.getNAME(), menu.getID(), menu.getDICID());
			if (nameMenu != null) {
				renderError("名称已存在！");
				return;
			}
			if (menu.update()) {
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			SysDicItem codeMenu = SysDicItem.dao.findFirst(
					"SELECT * FROM " + SysDicItem.TABLE_NAME + " WHERE CODE=? and DICID=? ", menu.getCODE(),
					menu.getDICID());
			if (codeMenu != null) {
				renderError("标识已存在！");
				return;
			}
			SysDicItem nameMenu = SysDicItem.dao.findFirst(
					"SELECT * FROM " + SysDicItem.TABLE_NAME + " WHERE NAME=? and DICID=? ", menu.getNAME(),
					menu.getDICID());
			if (nameMenu != null) {
				renderError("名称已存在！");
				return;
			}
			menu.setID(UuidUtil.getUUID());
			if (menu.save()) {
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}
	}

	/***
	 * 删除
	 * 
	 * @throws Exception
	 */
	public void deleteItem() throws Exception {
		String id = getPara("id");
		SysDicItem menu = SysDicItem.dao.findById(id);
		if (menu != null) {
			if (menu.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}

}
