package com.vf.s.mvc.biz.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.vf.core.model.ZtreeNode;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizLayer;
import com.vf.s.common.model.biz.BizLine;
import com.vf.s.common.model.biz.BizModel;
import com.vf.s.common.model.biz.Point;
import com.vf.s.common.model.biz.BizPolygon;
import com.vf.s.common.model.biz.BizVideoModel;
import com.vf.s.common.model.biz.BizVideoPlane;
import com.vf.s.common.model.biz.BizVideoShed3d;
import com.vf.s.common.model.scene.BizSceneModel;
import com.vf.s.common.vector.kml.KmlLine;
import com.vf.s.common.vector.kml.KmlPoint;
import com.vf.s.common.vector.kml.KmlPolygon;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Placemark;

public class BizLayerService {

	
	public List<ZtreeNode> getAsyncTreeData(Controller para) {
		String id = para.get("id", "root");
		String sceneId = para.get("sceneId");
		String type = para.get("type");
		String code = para.get("code");

		
		List<ZtreeNode> nodelist = new ArrayList<ZtreeNode>();
		//点集合
		if(StrKit.equals("Points", type)) {
			List<Point> olist=Point.dao.find("SELECT * FROM "+Point.TABLE_NAME+" WHERE LAYERID=? and sceneId=?",id,sceneId);
			for (Point o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("Point");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		}
		//底图集合
		else if(StrKit.equals("Maps", type)) {
		}
		//模型集合
		else if(StrKit.equals("Models", type)) {
			List<BizSceneModel> olist=BizSceneModel.dao.find("SELECT A.*,B.URL AS URI FROM "+BizSceneModel.TABLE_NAME+" A, "+BizModel.TABLE_NAME+" B "
					+ " WHERE A.MODELID=B.ID AND A.SCENEID=? AND A.LAYERID=?  ",sceneId,id);
			for (BizSceneModel o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("Model");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(id);
				nodelist.add(node);
			}
		}
		//点集合
		if(StrKit.equals("Polylines", type)) {
			List<BizLine> olist=BizLine.dao.find("SELECT * FROM "+BizLine.TABLE_NAME+" WHERE LAYERID=? and sceneId=?",id,sceneId);
			for (BizLine o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("Polyline");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		}
		//面集合
		if(StrKit.equals("Polygons", type)) {
			List<BizPolygon> olist=BizPolygon.dao.find("SELECT * FROM "+BizPolygon.TABLE_NAME+" WHERE LAYERID=? and sceneId=?",id,sceneId);
			for (BizPolygon o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("Polygon");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		}
		//单体集合
		if(StrKit.equals("Features", type)) {
		}
		if(StrKit.equals("VideoPlanes", type)) {
			List<BizVideoPlane> olist=BizVideoPlane.dao.find("SELECT * FROM "+BizVideoPlane.TABLE_NAME+" WHERE LAYERID=? and sceneId=?",id,sceneId);
			for (BizVideoPlane o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("VideoPlane");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		}
		if(StrKit.equals("VideoShed3ds", type)) {
			List<BizVideoShed3d> olist=BizVideoShed3d.dao.find("SELECT * FROM "+BizVideoShed3d.TABLE_NAME+" WHERE LAYERID=? and sceneId=?",id,sceneId);
			for (BizVideoShed3d o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("VideoShed3d");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		}
		if(StrKit.equals("VideoModels", type)) {
			List<BizVideoModel> olist=BizVideoModel.dao.find("SELECT * FROM "+BizVideoModel.TABLE_NAME+" WHERE LAYERID=? and sceneId=?",id,sceneId);
			for (BizVideoModel o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("VideoModel");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		}
		if(StrKit.equals("Roams", type)) {
		}
		if(StrKit.equals("Roam", type)) {
		}
		else {
			String sql = "SELECT a.*,(CASE WHEN (select count(id) from biz_layer where a.id=parentId ) >0 THEN true ELSE false END) AS isParent FROM biz_layer a where a.parentId=? and sceneId=? ";
//			if(!StrKit.isBlank(type)) {
//				sql += " and a.type='"+type+"' ";
//			}
			if(!StrKit.isBlank(code)) {
				sql += " and a.code='"+code+"' ";
			}
			sql += " order by a.sort asc ";
			List<BizLayer> olist = BizLayer.dao.find(sql, id, sceneId);
			for (BizLayer o : olist) {
				ZtreeNode node = BizLayer.dao.toZTreeNode(o);
				node.setIsParent(true);
				node.setParentId(o.getParentId());
				node.setDefaultStyleId(o.getDefaultStyleId());
				node.setHoverStyleId(o.getHoverStyleId());
				node.setSelectedStyleId(o.getSelectedStyleId());
				nodelist.add(node);
			}
		}
		return nodelist;
	}
	
	public void batchUpdataStyle(BizLayer model) {
		if(StrKit.equals("Points", model.getType())) {
    		List<Point> bizPoints = Point.dao.find("SELECT * FROM "+Point.TABLE_NAME+" WHERE LAYERID=? ",model.getId());
    		for (Point bizPoint : bizPoints) {
				bizPoint.update();
			}
		}else if(StrKit.equals("Polylines", model.getType()) ) {
			List<BizLine> bizLines = BizLine.dao.find("SELECT * FROM "+BizLine.TABLE_NAME+" WHERE LAYERID=? ",model.getId());
			for (BizLine bizLine : bizLines) {
				bizLine.update();
			}
		}else if(StrKit.equals("Polygons", model.getType())) {
			List<BizPolygon> bizPolygons = BizPolygon.dao.find("SELECT * FROM "+BizPolygon.TABLE_NAME+" WHERE LAYERID=? ",model.getId());
			for (BizPolygon bizPolygon : bizPolygons) {
				bizPolygon.update();
			}
		}
	}
	
	public void saveForBizPoint(String layerId,KmlPoint point) {
		List<Coordinate> points=point.getPoints();
		Placemark placemark=point.getPlacemark();
		if(points!=null && points.size()>0) {
			for (Coordinate coordinate : points) {
				savePoint(layerId, placemark, coordinate);
			}
		}
	}
	
	private void savePoint(String layerId,Placemark placemark,Coordinate coordinate) {
		Point bizPoint=Point.dao.findFirst("SELECT * FROM "+Point.TABLE_NAME+" WHERE x=? AND  y=? AND  z=?  AND name=?  ",coordinate.getLongitude(),coordinate.getLatitude(),coordinate.getAltitude(),placemark.getName());
		if(bizPoint==null) {
			bizPoint=new Point();
			bizPoint.setId(UuidUtil.getUUID());
			bizPoint.setName(placemark.getName());
			bizPoint.setX(coordinate.getLongitude());
			bizPoint.setY(coordinate.getLatitude());
			bizPoint.setZ(coordinate.getAltitude());
			bizPoint.setTelephone(placemark.getPhoneNumber());
			bizPoint.setAdress(placemark.getAddress());
			bizPoint.setLayerId(layerId);
			bizPoint.save();
			if(bizPoint.getX()!=null && bizPoint.getY()!=null) {
				Db.update("update "+Point.TABLE_NAME+" set location=POINT('"+bizPoint.getX()+"','"+bizPoint.getY()+"')  where id=? ",bizPoint.getId());
			}
		}
	}
	
	public void saveForBizLine(String layerId,KmlLine line) {
		List<Coordinate> points=line.getPoints();
		Placemark placemark=line.getPlacemark();
		if(points!=null && points.size()>0) {
			saveLine(  layerId, placemark, points);
		}
	}
	
	private void saveLine(String layerId,Placemark placemark,List<Coordinate> points) {
		BizLine bizLine=new BizLine();
		bizLine.setId(UuidUtil.getUUID());
		bizLine.setName(placemark.getName());
		bizLine.setLayerId(layerId);
		List<Map<String, Object>> ps=new LinkedList<Map<String, Object>>();
		StringBuffer sb=new StringBuffer();
		for (Coordinate coordinate : points) {
			Map<String, Object> obj=new HashMap<String, Object>();
			obj.put("x", coordinate.getLongitude());
			obj.put("y", coordinate.getLatitude());
			obj.put("z", coordinate.getAltitude());
			sb.append(coordinate.getLongitude()).append(" ").append(coordinate.getLatitude()).append(",");
			ps.add(obj);
		}
		bizLine.setPoints(JsonKit.toJson(ps));
		bizLine.save();
		Db.update("update "+BizLine.TABLE_NAME+" set gem=ST_GeometryFromText('LINESTRING("+(sb.substring(0,sb.length()-1))+")') where id=? ",bizLine.getId());
	}
	
	public void saveForKmlPolygon(String layerId,KmlPolygon polygon) {
		List<Coordinate> points=polygon.getPoints();
		Placemark placemark=polygon.getPlacemark();
		
		if(points!=null && points.size()>0) {
			savePolygon(layerId, placemark,points) ;
		}
	}
	
	private void savePolygon(String layerId,Placemark placemark,List<Coordinate> points) {
		BizPolygon bizPolygon=new BizPolygon();
		bizPolygon.setId(UuidUtil.getUUID());
		bizPolygon.setName(placemark.getName());
		bizPolygon.setLayerId(layerId);
		StringBuffer sb=new StringBuffer();
		List<Map<String, Object>> ps=new LinkedList<Map<String, Object>>();
		for (Coordinate coordinate : points) {
			Map<String, Object> obj=new HashMap<String, Object>();
			obj.put("x", coordinate.getLongitude());
			obj.put("y", coordinate.getLatitude());
			obj.put("z", coordinate.getAltitude());
			ps.add(obj);
			sb.append(coordinate.getLongitude()).append(" ").append(coordinate.getLatitude()).append(",");
		}
		bizPolygon.setPoints(JsonKit.toJson(ps));
		bizPolygon.save();
		Db.update("update "+BizLine.TABLE_NAME+" set gem=ST_POLYGONFROMTEXT('POLYGON( ("+(sb.substring(0,sb.length()-1))+"))') where id=? ",bizPolygon.getId());
	
	}
	
}
