package com.vf.s.mvc.biz.controller;

import java.io.File;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.Provider;
import com.vf.s.common.plugins.shiro.ShiroKit;
import com.vf.s.common.plugins.shiro.SimpleUser;

public class ProviderController extends BaseController {
	
	public void index() {
		this.render("list.html");
	}
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String dataType = getPara("dataType");
		String isPublic = getPara("isPublic");
		String select = " select *  ";
		String sqlExceptSelect = " from " + Provider.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		if (!StrKit.isBlank(dataType)) {
			sqlExceptSelect += " and o.dataType = '" + dataType + "'  ";
		}
		if (!StrKit.isBlank(isPublic)) {
			sqlExceptSelect += " and o.isPublic = '" + isPublic + "'  ";
		}
		
		Page<Provider> page = Provider.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}

	public void save() {
		Provider model = getModel(Provider.class);
		if (StrKit.notBlank(model.getId())) {
			if (model.update()) {
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			model.setId(UuidUtil.getUUID());
			SimpleUser simpleUser = ShiroKit.getLoginUser();
			if (simpleUser != null) {
				model.setCreateUserId(simpleUser.getId());
				model.setCreateUserName(simpleUser.getName());
			}
			if (model.save()) {
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		Provider model = Provider.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
				try {
					if(!StrKit.isBlank(model.getImg())) {
						File file = new File(rootPath+model.getImg());
						if(file.exists()) {
							file.delete();
						}
					}
				}catch (Exception e) {
				}
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
	public void getById() {
		Provider model = Provider.dao.findById(this.getPara("id"));
		if(model!=null) {
			renderSuccess("查询成功！",model);
		}else {
			renderError("数据不存在!");
		}
	}
	
	private String rootPath = PathKit.getWebRootPath();
	public void upload() {
		UploadFile uploadFile = getFile();
		try {
			String id=this.getPara("id");

			if(StrKit.isBlank(id)) {
				renderError("参数必填！");
				return;
			}
			
			Provider model = Provider.dao.findById(id);
			if(model==null) {
				renderError("参数无效！");
				return;
			}
			String path = "/upload/property/"+id+"/" + uploadFile.getFileName().replace(" ", "");
			File file = new File(rootPath+path);
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			if (file.exists()) {
				file.delete();
			}
			model.setImg(path);
			if(uploadFile.getFile().renameTo(file)) {
				model.update();
				renderSuccess("成功！",model);
			}else {
				renderError("失败！");
			}
		} catch (Exception e) {
			renderError(e.getMessage());
		}
	}
	
	
}
