package com.vf.s.mvc.scene.controller;


import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.vf.core.controller.BaseController;
import com.vf.core.util.PinYinUtil;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.cache.style.MapLayerCache;
import com.vf.s.common.cache.style.MapStyleCache;
import com.vf.s.common.model.scene.*;
import com.vf.s.mvc.sys.service.DicService;
public class SceneContoller  extends BaseController{

	@Inject
	private DicService dicSrv;
	
	public void editor() {
		this.setAttr("scene",Scene.dao.findById(this.getPara("sceneId")));
		this.setAttr("types",dicSrv.findItemList("sceneMenuType"));
		render("editor.html");
	}

	public void setting() {
		this.setAttr("scene", Scene.dao.findById(this.getPara("sceneId")));
		this.setAttr("skybox", SceneSkybox.dao.findFirst("select * from "+SceneSkybox.TABLE_NAME+" where sceneId=? ",this.getPara("sceneId")));
		render("setting.html");
	}
	
	public void save() {
		Scene model = getModel(Scene.class);
		if (StrKit.notBlank(model.getId())) {
			if(StrKit.isBlank(model.getCode()) &&!StrKit.isBlank(model.getTitle())) {
				model.setCode(PinYinUtil.getPinyin(model.getTitle()));
			}
			if (model.update()) {
				renderSuccess("更新成功！", Scene.dao.findById(model.getId()));
			} else {
				renderError("更新失败！");
			}
		} else {
			model.setId(UuidUtil.getUUID());
			if(StrKit.isBlank(model.getCode()) && !StrKit.isBlank(model.getTitle())) {
				model.setCode(PinYinUtil.getPinyin(model.getTitle()));
			}
			if (model.save()) {
				renderSuccess("保存成功！",Scene.dao.findById(model.getId()));
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	/**
	 * 获取场景
	 */
	public void getById() {
		String sceneId = getPara("sceneId");
		Scene scene = Scene.dao.findById(sceneId);
		if(scene==null) {
			renderError("场景不存在!");
			return;
		}
		scene.setMapStyle(MapStyleCache.gets());
		scene.setMapLayer(MapLayerCache.gets());
		renderSuccess("成功！",scene);
	}
	
}
