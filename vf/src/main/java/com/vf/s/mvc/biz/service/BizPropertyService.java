package com.vf.s.mvc.biz.service;

import com.jfinal.kit.StrKit;
import com.vf.s.common.model.biz.BizPage;
import com.vf.s.common.model.biz.BizPanorama;
import com.vf.s.common.model.biz.BizPicture;
import com.vf.s.common.model.biz.BizProperty;
import com.vf.s.common.model.biz.BizText;
import com.vf.s.common.model.biz.BizVideo;
import com.vf.s.common.render.PropertyRender;

public class BizPropertyService {

	public PropertyRender getObjProperty(String objId, String event,String property) {
		PropertyRender propertyRender = new PropertyRender();
		// 文本
		if(StrKit.isBlank(property) || StrKit.equals("txt", property)) {
			propertyRender.setBizText(BizText.dao
					.findFirst("SELECT * FROM " + BizText.TABLE_NAME + " WHERE ID=? AND EVENT=? ",objId, event));
		}
		// 页面
		if(StrKit.isBlank(property) || StrKit.equals("page", property)) {
			propertyRender.setBizPages(BizPage.dao
					.find("SELECT * FROM " + BizPage.TABLE_NAME + " WHERE  objId=? AND event=? ", objId, event));
		}
		// 属性
		if(StrKit.isBlank(property) || StrKit.equals("property", property)) {
			propertyRender.setBizPropertys(BizProperty
					.dao.find("SELECT * FROM " + BizProperty.TABLE_NAME + " WHERE  objId=? AND event=? ", objId, event));
		}
		// 图片
		if(StrKit.isBlank(property) || StrKit.equals("picture", property)) {
			propertyRender.setBizPictures(BizPicture
					.dao.find("SELECT * FROM " + BizPicture.TABLE_NAME + " WHERE  objId=? AND event=? ", objId, event));
		}
		// 全景
		if(StrKit.isBlank(property) || StrKit.equals("panorama", property)) {
			propertyRender.setBizPanoramas(BizPanorama
					.dao.find("SELECT * FROM " + BizPanorama.TABLE_NAME + " WHERE  objId=? AND event=? ", objId, event));
		}
		// 视频
		if(StrKit.isBlank(property) || StrKit.equals("video", property)) {
			propertyRender.setBizVideos(BizVideo
					.dao.find("SELECT * FROM " + BizVideo.TABLE_NAME + " WHERE  objId=? AND event=? ", objId, event));
		}
		return propertyRender;
	}

}
