package com.vf.s.mvc.event.controller;

import java.util.List;

import com.vf.core.controller.BaseController;
import com.vf.s.common.model.biz.BizPage;
import com.vf.s.common.model.biz.BizProperty;
import com.vf.s.common.model.biz.BizText;
import com.vf.s.common.render.MouseEventRender;

public class BizMouseEventController extends BaseController {
	
	public void index() {
		this.set("objId", this.getPara("objId"));
		this.set("sceneId", this.getPara("sceneId"));
		this.set("event", this.getPara("event"));
		BizText bizText=BizText.dao.findById(getPara("objId"));
		this.set("bizText", bizText);
		this.render("list.html");
	}
	
	
	
	public void getInfo() {
		String objId=this.getPara("objId");
		String event=this.getPara("event");
		MouseEventRender mouseEventRender=new MouseEventRender();
		//文本
		BizText bizText=BizText.dao.findFirst("SELECT * FROM "+BizText.TABLE_NAME+" WHERE ID=? AND EVENT=? ",objId,event);
		//页面
		List<BizPage> bizPages=BizPage.dao.find("SELECT * FROM "+BizPage.TABLE_NAME+" WHERE  objId=? AND event=? ",objId,event);
		//属性
		List<BizProperty> bizPropertys=BizProperty.dao.find("SELECT * FROM "+BizProperty.TABLE_NAME+" WHERE  objId=? AND event=? ",objId,event);
		
		mouseEventRender.setBizText(bizText);
		mouseEventRender.setBizPages(bizPages);
		mouseEventRender.setBizPropertys(bizPropertys);
		
		
		renderSuccess("成功！", mouseEventRender);
	}
	
	

}
