package com.vf.s.mvc.biz.controller;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizVideoShed3d;

public class BizVideoShed3dController extends BaseController {
	
	public void index() {
		this.render("list.html");
	}
	
	public void operation() {
		this.set("sceneId", this.get("sceneId"));
		this.set("shed3dId", this.get("shed3dId"));
		BizVideoShed3d bizVideoShed3d=BizVideoShed3d.dao.findById(get("shed3dId"));
		this.set("bizVideoShed3d", bizVideoShed3d);
		render("operation.html");
	}
	
	public void edit() {
		this.set("sceneId", this.get("sceneId"));
		BizVideoShed3d bizVideoShed3d=BizVideoShed3d.dao.findById(get("shed3dId"));
		this.set("bizVideoShed3d", bizVideoShed3d);
		render("edit.html");
	}

	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String sceneId = getPara("sceneId");
		String layersId = getPara("layersId");
		
		String sqlExceptSelect = " from " + BizVideoShed3d.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		if (!StrKit.isBlank(layersId)) {
			sqlExceptSelect += " AND O.layersId='"+layersId+"' ";
		}
		if (!StrKit.isBlank(sceneId)) {
			sqlExceptSelect += " AND O.SCENEID='"+sceneId+"' ";
		}
		Page<BizVideoShed3d> page = BizVideoShed3d.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	
	public void save() {
		BizVideoShed3d model = getModel(BizVideoShed3d.class);
		if(!StrKit.isBlank(model.getId())) {
			if (model.update() ) {
				renderSuccess("更新成功！",BizVideoShed3d.dao.findById(model.getId()));
			} else {
				renderError("更新失败！");
			}
		}else {
			model.setId(UuidUtil.getUUID());
			if(model.save()) {
				renderSuccess("保存成功！",BizVideoShed3d.dao.findById(model.getId()));
			} else {
				renderError("保存失败！");
			}
		}
	}

	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		BizVideoShed3d model = BizVideoShed3d.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}

}
