package com.vf.s.mvc.scene.controller;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.cache.style.MapStyleCache;
import com.vf.s.common.model.biz.BizPolygon;
import com.vf.s.common.model.biz.BizStylePolygon;
import com.vf.s.common.model.scene.SceneLayer;
public class BizScenePolygonContoller  extends BaseController{
	
	public void setting() {
		this.setAttr("layer",SceneLayer.dao.findById(this.getPara("id")));
		render("setting.html");
	}
	public void edit() {
		this.set("bizPolygon", BizPolygon.dao.findById(get("polygonId")));
		render("edit.html");
	}
	public void findStyleById() {
		BizStylePolygon model = BizStylePolygon.dao.findById(this.getPara("id"));
		if (model!=null) {
			renderSuccess("成功！",model);
		} else {
			renderError("记录不存在！");
		}
	}
	
//	public void saveStyle() {
//		BizStylePolygon model = getModel(BizStylePolygon.class);
//		if (StrKit.notBlank(model.getId()) &&  BizStylePolygon.dao.findById(model.getId())!=null) {
//			if (model.update()) {
//				model=BizStylePolygon.dao.findById(model.getId());
//				renderSuccess("更新成功！", model);
//				MapStyleCache.put(model.getId(), model);
//			} else {
//				renderError("更新失败！");
//			}
//		} else {
//			if(StrKit.isBlank(model.getId()))
//				model.setId(UuidUtil.getUUID());
//			if (model.save()) {
//				model=BizStylePolygon.dao.findById(model.getId());
//				renderSuccess("保存成功！", model);
//				MapStyleCache.put(model.getId(), model);
//			} else {
//				renderError("保存失败！");
//			}
//		}
//	}
	
	public void getList() {
		String layerId = getPara("layerId");
		String whereForBizPoint="FROM "+BizPolygon.TABLE_NAME+" P  WHERE 1=1 ";
		if(!StrKit.isBlank(layerId)) {
			whereForBizPoint+=" and P.LAYERID ='"+layerId+"' ";
		}
		List<BizPolygon> bizPolygonList=BizPolygon.dao.find("SELECT P.*  "+whereForBizPoint);
		if(bizPolygonList!=null && bizPolygonList.size()>0) {
			for(BizPolygon bizPolygon:bizPolygonList) {
				BizStylePolygon bizStylePolygon=BizStylePolygon.dao.findFirst("SELECT a.* FROM "+BizStylePolygon.TABLE_NAME+" a, biz_style_obj b where a.id=b.styleId and a.code='DEFAULT' and b.objid=? ",bizPolygon.getId());
				if(bizStylePolygon!=null) {
					bizPolygon.setDefaultStyleId(bizStylePolygon.getId());
				}
				 bizStylePolygon=BizStylePolygon.dao.findFirst("SELECT a.* FROM "+BizStylePolygon.TABLE_NAME+" a, biz_style_obj b where a.id=b.styleId and a.code='HOVER' and b.objid=? ",bizPolygon.getId());
				if(bizStylePolygon!=null) {
					bizPolygon.setHoverStyleId(bizStylePolygon.getId());
				}
				 bizStylePolygon=BizStylePolygon.dao.findFirst("SELECT a.* FROM "+BizStylePolygon.TABLE_NAME+" a, biz_style_obj b where a.id=b.styleId and a.code='SELECTED' and b.objid=? ",bizPolygon.getId());
				if(bizStylePolygon!=null) {
					bizPolygon.setSelectedStyleId(bizStylePolygon.getId());
				}
			}
			this.renderSuccess("成功！", bizPolygonList);
		}else {
			renderError("未查询到数据！");
		}
	}
	
	public void getById() {
		BizPolygon bizPolygon=BizPolygon.dao.findById(getPara("id"));
		if(bizPolygon!=null) {
			
			BizStylePolygon bizStylePolygon=BizStylePolygon.dao.findFirst("SELECT a.* FROM "+BizStylePolygon.TABLE_NAME+" a, biz_style_obj b where a.id=b.styleId and a.code='DEFAULT' and b.objid=? ",bizPolygon.getId());
			if(bizStylePolygon!=null) {
				bizPolygon.setDefaultStyleId(bizStylePolygon.getId());
			}
			 bizStylePolygon=BizStylePolygon.dao.findFirst("SELECT a.* FROM "+BizStylePolygon.TABLE_NAME+" a, biz_style_obj b where a.id=b.styleId and a.code='HOVER' and b.objid=? ",bizPolygon.getId());
			if(bizStylePolygon!=null) {
				bizPolygon.setHoverStyleId(bizStylePolygon.getId());
			}
			 bizStylePolygon=BizStylePolygon.dao.findFirst("SELECT a.* FROM "+BizStylePolygon.TABLE_NAME+" a, biz_style_obj b where a.id=b.styleId and a.code='SELECTED' and b.objid=? ",bizPolygon.getId());
			if(bizStylePolygon!=null) {
				bizPolygon.setSelectedStyleId(bizStylePolygon.getId());
			}
			
			this.renderSuccess("成功！", bizPolygon);
		}else {
			renderError("未查询到数据！");	
		}
	}
	
	public void getStyleList() {
		String sqlExceptSelect = " from "+BizStylePolygon.TABLE_NAME+" a, biz_style_obj b where a.id=b.styleId and b.objid='"+getPara("id")+"' ";
		sqlExceptSelect += " order by a.code ";
		Page<BizStylePolygon> page = BizStylePolygon.dao.paginate(1, 100, "select a.* ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	
	public void saveStyle() {
		String polygonId=this.getPara("polygonId");
		if(StrKit.isBlank(polygonId)) {
			renderError("请指定对象Id！");
			return;
		}
		BizStylePolygon model = getModel(BizStylePolygon.class);
		if (StrKit.notBlank(model.getId())) {
			if (model.update()) {
				MapStyleCache.put(model.getId(), BizStylePolygon.dao.findById(model.getId()));
				renderSuccess("更新成功！",model);
			} else {
				renderError("更新失败！");
			}
		} else {
			model.setId(UuidUtil.getUUID());
			if (model.save()) {
				MapStyleCache.put(model.getId(), BizStylePolygon.dao.findById(model.getId()));
				renderSuccess("保存成功！",model);
				Record record=new Record();
				record.set("id", UuidUtil.getUUID());
				record.set("objid", polygonId);
				record.set("styleId",model.getId());
				Db.save("biz_style_obj", "id", record);
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	public void deleteStyle() {
		String id=this.getPara("id");
		BizStylePolygon model = BizStylePolygon.dao.findById(id);
		if (model!=null) {
			if (model.delete()) {
				MapStyleCache.remove(model.getId());
				renderSuccess("删除成功！",model);
				Db.delete("delete from biz_style_obj where styleId=? ",id);
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在！");
		}
	}
	
		
}
