package com.vf.s.mvc.scene.controller;

import java.util.LinkedList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.core.controller.BaseController;
import com.vf.s.common.model.biz.BizVideo;
import com.vf.s.common.model.biz.BizVideoShed3d;
public class BizSceneVShed3dContoller  extends BaseController{
	
	public void getList() {
		String layerId = getPara("layerId");
		String whereForBizPoint="FROM "+BizVideoShed3d.TABLE_NAME+" P   WHERE 1=1 ";
		if(!StrKit.isBlank(layerId)) {
			whereForBizPoint+=" and P.LAYERID ='"+layerId+"' ";
		}
		List<BizVideoShed3d> bizLineList=BizVideoShed3d.dao.find("SELECT P.*  "+whereForBizPoint);
		
		
		List<BizVideoShed3d> list=new LinkedList<BizVideoShed3d>();
		for(BizVideoShed3d videoShed3d :bizLineList) {
			BizVideo bizVideo=BizVideo.dao.findById(videoShed3d.getVideoId());
			videoShed3d.setVideo(bizVideo);
			list.add(videoShed3d);
		}
		
		if(list!=null && list.size()>0) {
			renderSuccess("成功！",list);
		}else {
			renderError("信息不存在！");
		}
	}
	
	public void getById() {
		BizVideoShed3d bizVideoPlane=BizVideoShed3d.dao.findById(this.getPara("id"));
		if(bizVideoPlane!=null) {
			BizVideo bizVideo=BizVideo.dao.findById(bizVideoPlane.getVideoId());
			bizVideoPlane.setVideo(bizVideo);
			renderSuccess("成功！",bizVideoPlane);
		}else {
			renderError("信息不存在！");
		}
	}
		
}
