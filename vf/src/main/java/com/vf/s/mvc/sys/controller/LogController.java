package com.vf.s.mvc.sys.controller;


import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.s.common.model.sys.SysLog;

public class LogController extends BaseController {

	public void index() {
		render("list.html");
	}
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String type = getPara("type");
		String ip = getPara("ip");

		String sqlExceptSelect = " from " + SysLog.tableName + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		if (!StrKit.isBlank(type)) {
			sqlExceptSelect += " and   o.type like '%" + type + "%'";
		}
		if (!StrKit.isBlank(ip)) {
			sqlExceptSelect += " and   o.ip like '%" + ip + "%'";
		}
		sqlExceptSelect += "  order by   o.requestTime desc ";
		Page<SysLog> page = SysLog.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}


	/***
	 * 删除
	 * 
	 * @throws Exception
	 */
	public void delete() {
		SysLog menu = SysLog.dao.findById(this.getPara("id"));
		if (menu != null) {
			if (menu.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("不存在,请刷新后再试!");
		}
	}


}
