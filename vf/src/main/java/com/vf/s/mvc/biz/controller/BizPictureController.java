package com.vf.s.mvc.biz.controller;

import java.io.File;
import java.util.List;

import javax.imageio.ImageIO;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizPicture;

public class BizPictureController extends BaseController {
	
	public void index() {
		this.render("list.html");
	}

	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String objId = getPara("objId");
		String event = getPara("event");
		
		String sqlExceptSelect = " from " + BizPicture.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		if (!StrKit.isBlank(objId)) {
			sqlExceptSelect += " AND O.OBJID='"+objId+"' ";
		}
		if (!StrKit.isBlank(event)) {
			sqlExceptSelect += " AND O.EVENT='"+event+"' ";
		}
		sqlExceptSelect += "  order by   o.createTime desc ";
		Page<BizPicture> page = BizPicture.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}

	public void allList() {
		List<BizPicture> BizPictures = BizPicture.dao.findAll();
		if(BizPictures!=null && BizPictures.size()>0) {
			renderSuccess("查询成功！", BizPictures);
		}else {
			renderError("无数据！");
		}
	}
	
	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		BizPicture model = BizPicture.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				File file = new File(rootPath+model.getPath());
				if (file!=null && file.exists()) {
					file.delete();
				}
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}

	private String rootPath = PathKit.getWebRootPath();
	public void upload() {
		UploadFile uploadFile = getFile();
		String objId = getPara("objId");
		try {
			String path = "/upload/picture" + "/" + uploadFile.getFileName().replace(" ", "");
			File file = new File(rootPath+path);
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			if (file.exists()) {
				file.delete();
			}
			java.awt.image.BufferedImage image = ImageIO.read(uploadFile.getFile());
			double width = image.getWidth();
			double height = image.getHeight();
			uploadFile.getFile().renameTo(file);
			BizPicture bizPicture = new BizPicture();
			bizPicture.setId(UuidUtil.getUUID());
			bizPicture.setName(uploadFile.getFileName());
			bizPicture.setWidth(width);
			bizPicture.setHeight(height);
			bizPicture.setPath(path);
			bizPicture.setSceneId(this.getPara("sceneId"));
			bizPicture.setObjId(objId);
			bizPicture.setEvent(getPara("event"));
			if(bizPicture.save()) {
				renderSuccess("上传成功！",bizPicture);
			}else {
				renderError("上传失败！");
			}
		} catch (Exception e) {
			renderError(e.getMessage());
		}
	}

}
