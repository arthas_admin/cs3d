package com.vf.s.mvc.scene.controller;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.cache.style.MapStyleCache;
import com.vf.s.common.model.biz.Point;
import com.vf.s.common.model.biz.BizStylePoint;
import com.vf.s.common.model.scene.SceneLayer;
public class BizScenePointContoller  extends BaseController{
	
	public void edit() {
		this.set("bizPoint", Point.dao.findById(get("pointId")));
		render("edit.html");
	}
	
	public void setting() {
		this.setAttr("layer",SceneLayer.dao.findById(this.getPara("id")));
		render("setting.html");
	}
	
	public void findStyleById() {
		BizStylePoint model = BizStylePoint.dao.findById(this.getPara("id"));
		if (model!=null) {
			renderSuccess("成功！",model);
		} else {
			renderError("记录不存在！");
		}
	}
	
	public void saveStyle() {
		BizStylePoint model = getModel(BizStylePoint.class);
		if (StrKit.notBlank(model.getId()) &&  BizStylePoint.dao.findById(model.getId())!=null) {
			if (model.update()) {
				model= BizStylePoint.dao.findById(model.getId());
				renderSuccess("更新成功！", model);
				MapStyleCache.put(model.getId(), model);
			} else {
				renderError("更新失败！");
			}
		} else {
			if(StrKit.isBlank(model.getId()))
				model.setId(UuidUtil.getUUID());
			if (model.save()) {
				model= BizStylePoint.dao.findById(model.getId());
				renderSuccess("保存成功！", model);
				MapStyleCache.put(model.getId(), model);
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	public void getList() {
		String layerId = getPara("layerId");
		String whereForBizPoint="FROM "+Point.TABLE_NAME+" P  WHERE 1=1 ";
		if(!StrKit.isBlank(layerId)) {
			whereForBizPoint+=" and P.LAYERID ='"+layerId+"' ";
		}
		List<Point> bizPointList=Point.dao.find("SELECT P.*  "+whereForBizPoint);
		if(bizPointList!=null && bizPointList.size()>0) {
			this.renderSuccess("成功！", bizPointList);
		}else {
			renderError("未查询到数据！");
		}
	}
	
	public void getById() {
		Point bizPoint=Point.dao.findById(getPara("id"));
		if(bizPoint!=null) {
			this.renderSuccess("成功！", bizPoint);
		}else {
			renderError("未查询到数据！");	
		}
	}
		
}
