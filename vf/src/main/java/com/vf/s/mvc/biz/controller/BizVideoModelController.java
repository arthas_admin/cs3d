package com.vf.s.mvc.biz.controller;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizVideoModel;

public class BizVideoModelController extends BaseController {
	
	public void index() {
		this.render("list.html");
	}
	
	public void operation() {
		this.set("sceneId", this.get("sceneId"));
		this.set("modelId", this.get("modelId"));
		BizVideoModel bizVideoModel=BizVideoModel.dao.findById(get("modelId"));
		this.set("bizVideoModel", bizVideoModel);
		render("operation.html");
	}
	
	public void edit() {
		this.set("sceneId", this.get("sceneId"));
		BizVideoModel bizVideoModel=BizVideoModel.dao.findById(get("modelId"));
		this.set("bizVideoModel", bizVideoModel);
		render("edit.html");
	}

	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String sceneId = getPara("sceneId");
		String layersId = getPara("layersId");
		
		String sqlExceptSelect = " from " + BizVideoModel.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		if (!StrKit.isBlank(layersId)) {
			sqlExceptSelect += " AND O.layersId='"+layersId+"' ";
		}
		if (!StrKit.isBlank(sceneId)) {
			sqlExceptSelect += " AND O.SCENEID='"+sceneId+"' ";
		}
		Page<BizVideoModel> page = BizVideoModel.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	
	public void save() {
		BizVideoModel model = getModel(BizVideoModel.class);
		if(!StrKit.isBlank(model.getId())) {
			if (model.update() ) {
				renderSuccess("更新成功！",BizVideoModel.dao.findById(model.getId()));
			} else {
				renderError("更新失败！");
			}
		}else {
			model.setId(UuidUtil.getUUID());
			if(model.save()) {
				renderSuccess("保存成功！",BizVideoModel.dao.findById(model.getId()));
			} else {
				renderError("保存失败！");
			}
		}
	}

	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		BizVideoModel model = BizVideoModel.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}

}
