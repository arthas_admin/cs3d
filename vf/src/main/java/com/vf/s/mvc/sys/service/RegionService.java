package com.vf.s.mvc.sys.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.vf.core.model.ZtreeNode;
import com.vf.core.render.RenderLayuiBean;
import com.vf.s.common.model.sys.SysRegion;

public class RegionService{
	
	protected final Log LOG = Log.getLog(getClass());

	public List<SysRegion> getMenusForUser() {
		return getChildrenAllTree("root",null);
	}

	public List<SysRegion> getChildrenAllTree(String pId, String userId) {
		List<SysRegion> list = getChildrenByPid(pId, userId);
		for (SysRegion o : list) {
			o.setChildren(getChildrenAllTree(o.getID(),userId));
		}
		return list;
	}
	
	/***
	 * 根据id 查询孩子
	 * @param id
	 * @return
	 */
	public List<SysRegion> getChildrenByPid(String id,String userid) {
		String sql = "SELECT * FROM "+SysRegion.TABLE_NAME+" M WHERE M.PARENTID='" + id + "'   ";
		sql = sql + " ORDER BY M.SORT";
		return SysRegion.dao.find(sql);
	}

	public RenderLayuiBean listData(Controller menuController) {
		List<SysRegion> list=SysRegion.dao.find("SELECT T.*, (CASE  WHEN (SELECT COUNT(ID) AS COUNT FROM "+SysRegion.TABLE_NAME+" WHERE PARENTID=T.ID)>0 THEN TRUE ELSE  FALSE END ) AS HAVECHILD   FROM "+SysRegion.TABLE_NAME+"  T  ORDER BY T.SORT ASC");
		RenderLayuiBean renderBean=new RenderLayuiBean();
		renderBean.setCode(0);
		renderBean.setMsg("查询成功");
		renderBean.setData(list);
		renderBean.setCount(list.size());
		return renderBean ;
	}

	/***
	 * 菜单转成ZTreeNode
	 * @param olist 数据 open 是否展开所有 ifOnlyLeaf 是否只选叶子
	 * @return
	 */
	public List<ZtreeNode> toZTreeNode(List<SysRegion> olist, Boolean open, Boolean ifOnlyLeaf) {
		List<ZtreeNode> list = new ArrayList<ZtreeNode>();
		for (SysRegion o : olist) {
			ZtreeNode node = toZTreeNode(o);
			if (o.getChildren() != null && o.getChildren().size() > 0) {// 如果有孩子
				node.setChildren(toZTreeNode(o.getChildren(), open, ifOnlyLeaf));
				if (ifOnlyLeaf) {// 如果只选叶子
					node.setNocheck(true);
				}
				node.setIsParent(true);
			}
			node.setOpen(open);
			list.add(node);
		}
		return list;
	}
	
	public ZtreeNode toZTreeNode(SysRegion org) {
		ZtreeNode node = new ZtreeNode();
		node.setId(org.getID());
		node.setName(org.getNAME());
		node.setParentId(org.getPARENTID());
		return node;
	}

}
