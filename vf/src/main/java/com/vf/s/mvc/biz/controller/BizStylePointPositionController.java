package com.vf.s.mvc.biz.controller;


import java.util.Date;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.cache.style.MapStyleCache;
import com.vf.s.common.model.biz.BizStylePoint;
import com.vf.s.common.model.biz.BizStylePointPosition;

public class BizStylePointPositionController extends BaseController {
	
	public void index() {
		this.render("list.html");
	}

	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String sqlExceptSelect = " from " + BizStylePointPosition.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		sqlExceptSelect += " order by o.createTime  desc ";
		Page<BizStylePointPosition> page = BizStylePointPosition.dao.paginate (pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}

	public void save() {
		BizStylePointPosition model = getModel(BizStylePointPosition.class);
		model.setStatus("1");
		if (StrKit.notBlank(model.getId())) {
			BizStylePointPosition bizStylePointPosition=BizStylePointPosition.dao.findFirst("SELECT * FROM "+BizStylePointPosition.TABLE_NAME +" WHERE NAME=? AND ID!=?",model.getName(),model.getId());
			if(bizStylePointPosition!=null) {
				renderError("名称已存在！");
				return ;
			}
			if (model.update()) {
				MapStyleCache.put(model.getId(), BizStylePoint.dao.findById(model.getId()));
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			model.setCreateTime(new Date());
			BizStylePointPosition bizStylePointPosition=BizStylePointPosition.dao.findFirst("SELECT * FROM "+BizStylePointPosition.TABLE_NAME +" WHERE NAME=?",model.getName());
			if(bizStylePointPosition!=null) {
				renderError("名称已存在！");
				return ;
			}
			model.setId(UuidUtil.getUUID());
			if (model.save()) {
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}

	}
	
	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		BizStylePointPosition model = BizStylePointPosition.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}

 
	
	
}
