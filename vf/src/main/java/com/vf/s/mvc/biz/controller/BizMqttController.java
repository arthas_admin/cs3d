package com.vf.s.mvc.biz.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.kit.StrKit;
import com.vf.core.controller.BaseController;
import com.vf.core.render.RenderLayuiBean;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizMqtt;
import com.vf.s.common.plugins.mqtt.MQTTPlugin;

public class BizMqttController extends BaseController {
	
	public final static  Map<String ,MQTTPlugin> poolForMQTTPlugin=new HashMap<String ,MQTTPlugin>(); 
	
	
	public void index() {
		this.render("list.html");
	}
	
	public void listData() {
		List<BizMqtt> list = BizMqtt.dao.find(
				"SELECT T.*, (CASE  WHEN (SELECT COUNT(ID) AS COUNT FROM BIZ_MQTT WHERE PARENTID=T.ID)>0 THEN TRUE ELSE  FALSE END ) AS HAVECHILD   FROM BIZ_MQTT  T  ORDER BY T.CREATETIME ASC");
		RenderLayuiBean renderBean = new RenderLayuiBean();
		renderBean.setCode(0);
		renderBean.setMsg("查询成功");
		renderBean.setData(list);
		renderBean.setCount(list.size());
		this.renderJson(renderBean);
	}
	

	public void save() {
		BizMqtt role = getModel(BizMqtt.class);
		if (StrKit.notBlank(role.getId())) {
			BizMqtt namerole=BizMqtt.dao.findFirst("SELECT * FROM "+BizMqtt.TABLE_NAME +" WHERE NAME=? AND ID!=?",role.getName(),role.getId());
			if(namerole!=null) {
				renderError("名称已存在！");
				return ;
			}
			if (role.update()) {
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			BizMqtt namerole=BizMqtt.dao.findFirst("SELECT * FROM "+BizMqtt.TABLE_NAME +" WHERE NAME=?",role.getName());
			if(namerole!=null) {
				renderError("名称已存在！");
				return ;
			}
			role.setId(UuidUtil.getUUID());
			if (role.save()) {
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}

	}
	
	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		BizMqtt model = BizMqtt.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
	/**
	 * 启动服务
	 */
	public void start() {
		BizMqtt model = BizMqtt.dao.findById(this.getPara("id"));
		if (model != null) {
			if(!poolForMQTTPlugin.containsKey(model.getId())){
				MQTTPlugin mqttPlugin=new MQTTPlugin(model);
				if(mqttPlugin.start()) {
					model.setStatus("1");
					poolForMQTTPlugin.put(model.getId(), mqttPlugin);
					model.update();
					renderSuccess("启动成功！");
				}else {
					renderError("启动失败！");
				}
			}else {
				renderError("服务已经启动！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
	/**
	 * 停止
	 */
	public void stop() {
		BizMqtt model = BizMqtt.dao.findById(this.getPara("id"));
		if (model != null) {
			if(poolForMQTTPlugin.containsKey(model.getId())) {
				if(poolForMQTTPlugin.get(model.getId()).stop()) {
					model.setStatus("0");model.update();
					poolForMQTTPlugin.remove(model.getId());
					renderSuccess("停止成功！");
				}else {
					renderError("停止失败!");
				}
			}else {
				renderError("服务已停止!");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
}
