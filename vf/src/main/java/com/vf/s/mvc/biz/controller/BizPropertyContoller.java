package com.vf.s.mvc.biz.controller;

import java.util.LinkedList;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.vf.core.controller.BaseController;
import com.vf.core.render.HttpClientResult;
import com.vf.core.util.HttpUtil;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizProperty;

public class BizPropertyContoller  extends BaseController{
	public void index() {
		set("event", getPara("event"));
		set("objId", getPara("objId"));
		render("list.html");
	}
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String keyword = getPara("keyword");
		String objId = getPara("objId");
		String event = getPara("event");
		
		String sqlExceptSelect = " FROM " + BizProperty.TABLE_NAME + " o  WHERE o.OBJID='"+objId+"' ";
		if (!StrKit.isBlank(keyword)) {
			sqlExceptSelect += " AND ( O.NAME LIKE '%" + keyword + "%' ) ";
		}
		if (!StrKit.isBlank(event)) {
			sqlExceptSelect += " AND   O.EVENT = '" + event + "' ";
		}
		
		sqlExceptSelect += "  ORDER BY   O.SORT ASC ";
		Page<BizProperty> page = BizProperty.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	
	/***
	 * 保存
	 */
	public void save() {
		BizProperty model = getModel(BizProperty.class);
		if (StrKit.notBlank(model.getId())) {
			if (model.update()) {
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			model.setId(UuidUtil.getUUID());
			if (model.save()) {
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}
	}

	/***
	 * 删除
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void delete() throws Exception {
		String id = getPara("id");
		BizProperty model = BizProperty.dao.findById(id);
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
	public void findById() throws Exception {
		String id = getPara("id");
		BizProperty model = BizProperty.dao.findById(id);
		if (model != null) {
			renderSuccess("成功！",model);
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
	/**
	 * 获取消毒详情-房间列表
	 * @throws Exception
	 */
	public void findCleanerSceneListById() throws Exception {
		String id = getPara("id");
		HttpClientResult result = HttpUtil.doGet("https://ejoydg.tianview.com/sg-front-api-data-xiaodu_org_info.html?org_uuid="+id, null, null);
		if (result.getCode() == 200) {
			if (!StrKit.isBlank(result.getContent())) {
				JSONObject srmResult = JSONObject.parseObject(result.getContent());
				if(!srmResult.isEmpty()) {
					if(srmResult.getInteger("error")==0) {
						JSONObject data = JSONObject.parseObject(srmResult.getString("data"));
						JSONArray scene_list = JSONArray.parseArray(data.getString("scene_list"));
						if(scene_list!=null) {
							renderSuccess("查询成功！", scene_list);
						}else {
							renderError("未查到数据！");
						}
					}else {
						renderError(srmResult.getInteger("error"));
					}
				}else {
					renderError("未返回数据！");
				}
			}else {
				renderError("未返回数据！");
			}
		}else {
			renderError(result.getContent());
		}
	}
	
	/**
	 * 获取消毒详情-房间消毒记录
	 * @throws Exception
	 */
	public void findCleanListByScene() throws Exception {
		String id = getPara("id");
		String room = getPara("room");
		List<JSONObject> res = new LinkedList<JSONObject>();
		HttpClientResult result = HttpUtil.doGet("https://ejoydg.tianview.com/sg-front-api-data-xiaodu_org_info.html?org_uuid="+id, null, null);
		if (result.getCode() == 200) {
			if (!StrKit.isBlank(result.getContent())) {
				JSONObject srmResult = JSONObject.parseObject(result.getContent());
				if(!srmResult.isEmpty()) {
					if(srmResult.getInteger("error")==0) {
						JSONObject data = JSONObject.parseObject(srmResult.getString("data"));
						JSONArray clean_list = JSONArray.parseArray(data.getString("clean_list"));
						if(clean_list!=null) {
							for (Object object : clean_list) {
								JSONObject cObj = (JSONObject) object;
								if(StrKit.equals(room, cObj.getString("scene_name"))) {
									res.add(cObj);
								}
							}
							renderSuccess("查询成功！", res);
						}else {
							renderError("未查到数据！");
						}
					}else {
						renderError(srmResult.getInteger("error"));
					}
				}else {
					renderError("未返回数据！");
				}
			}else {
				renderError("未返回数据！");
			}
		}else {
			renderError(result.getContent());
		}
	}

}
