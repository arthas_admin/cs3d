package com.vf.s.mvc.scene.controller;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.cache.style.MapStyleCache;
import com.vf.s.common.model.biz.BizLine;
import com.vf.s.common.model.biz.BizStyleLine;
import com.vf.s.common.model.scene.SceneLayer;
public class BizScenePolylineContoller  extends BaseController{
	
	public void setting() {
		this.setAttr("layer",SceneLayer.dao.findById(this.getPara("id")));
		render("setting.html");
	}
	
	public void edit() {
		this.set("bizLine", BizLine.dao.findById(get("lineId")));
		render("edit.html");
	}
	
	public void findStyleById() {
		BizStyleLine model = BizStyleLine.dao.findById(this.getPara("id"));
		if (model!=null) {
			renderSuccess("成功！",model);
		} else {
			renderError("记录不存在！");
		}
	}
	
	public void saveStyle() {
		BizStyleLine model = getModel(BizStyleLine.class);
		if (StrKit.notBlank(model.getId()) &&  BizStyleLine.dao.findById(model.getId())!=null) {
			if (model.update()) {
				model=BizStyleLine.dao.findById(model.getId());
				renderSuccess("更新成功！", model);
				MapStyleCache.put(model.getId(), model);
			} else {
				renderError("更新失败！");
			}
		} else {
			if(StrKit.isBlank(model.getId()))
				model.setId(UuidUtil.getUUID());
			if (model.save()) {
				model=BizStyleLine.dao.findById(model.getId());
				renderSuccess("保存成功！", model);
				MapStyleCache.put(model.getId(), model);
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	public void getList() {
		String layerId = getPara("layerId");
		String whereForBizPoint="FROM "+BizLine.TABLE_NAME+" P  WHERE 1=1 ";
		if(!StrKit.isBlank(layerId)) {
			whereForBizPoint+=" and P.LAYERID ='"+layerId+"' ";
		}
		List<BizLine> bizLineList=BizLine.dao.find("SELECT P.*  "+whereForBizPoint);
		if(bizLineList!=null && bizLineList.size()>0) {
			this.renderSuccess("成功！", bizLineList);
		}else {
			renderError("未查询到数据！");
		}
	}
	
	public void getById() {
		BizLine bizLine=BizLine.dao.findById(getPara("id"));
		if(bizLine!=null) {
			this.renderSuccess("成功！", bizLine);
		}else {
			renderError("未查询到数据！");	
		}
	}
		
}
