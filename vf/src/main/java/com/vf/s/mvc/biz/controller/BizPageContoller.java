package com.vf.s.mvc.biz.controller;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizPage;

public class BizPageContoller extends BaseController {
	
	public void index() {
		set("objId", getPara("objId"));
		render("list.html");
	}
	
	
	public void edit() {
		set("objId", getPara("objId"));
		set("sceneId", getPara("sceneId"));
		set("event", getPara("event"));
		BizPage bizPage=BizPage.dao.findById(getPara("id"));
		set("bizPage", bizPage);
		render("edit.html");
	}
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String keyword = getPara("keyword");
		String objId = getPara("objId");
		String event = getPara("event");
		
		String sqlExceptSelect = " FROM " + BizPage.TABLE_NAME + " O  WHERE O.OBJID='" + objId + "' ";
		if (!StrKit.isBlank(keyword)) {
			sqlExceptSelect += " AND ( O.NAME LIKE '%" + keyword + "%' ) ";
		}
		if (!StrKit.isBlank(event)) {
			sqlExceptSelect += " AND  O.EVENT = '" + event + "'  ";
		}
		sqlExceptSelect += "  ORDER BY   O.CREATETIME DESC ";
		Page<BizPage> page = BizPage.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}

	/***
	 * 保存
	 */
	public void save() {
		BizPage model = getModel(BizPage.class);
		if (StrKit.notBlank(model.getId())) {
			if (model.update()) {
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			model.setId(UuidUtil.getUUID());
			if (model.save()) {
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}
	}

	/***
	 * 删除
	 * 
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void delete() throws Exception {
		String id = getPara("id");
		BizPage model = BizPage.dao.findById(id);
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}

	public void findById() throws Exception {
		String id = getPara("id");
		BizPage model = BizPage.dao.findById(id);
		if (model != null) {
			renderSuccess("成功！", model);
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}

}
