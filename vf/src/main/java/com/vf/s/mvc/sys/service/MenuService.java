package com.vf.s.mvc.sys.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.vf.core.model.ZtreeNode;
import com.vf.core.render.RenderLayuiBean;
import com.vf.s.common.model.sys.SysMenu;
import com.vf.s.common.plugins.shiro.ShiroKit;
import com.vf.s.common.plugins.shiro.SimpleUser;

public class MenuService {

	protected final Log LOG = Log.getLog(getClass());

	public List<SysMenu> getMenusForUser() {
		SimpleUser simpleUser = ShiroKit.getLoginUser();
		if (simpleUser != null && !StrKit.equals(simpleUser.getUsername(), "system")) {
			return getChildrenAllTree("root", simpleUser.getId());
		}
		return getChildrenAllTree("root", null);
	}

	public List<SysMenu> getChildrenAllTree(String pId, String userId) {
		List<SysMenu> list = getChildrenByPid(pId, userId);
		for (SysMenu o : list) {
			o.setChildren(getChildrenAllTree(o.getID(), userId));
		}
		return list;
	}

	/***
	 * 根据id 查询孩子
	 * 
	 * @param id
	 * @return
	 */
	public List<SysMenu> getChildrenByPid(String id, String userid) {
		String sql = "SELECT * FROM SYS_MENU M WHERE M.PARENTID='" + id + "' AND  M.TYPE!='2'   ";
		if (userid != null && !StrKit.isBlank(userid)) {
			String authSql = " (SELECT B.MENUID FROM SYS_USER_ROLE A,SYS_ROLE_PERMISSIONS B WHERE A.USERID='" + userid
					+ "' AND  A.ROLEID=B.ROLEID) ";
			sql = sql + " and m.id in  " + authSql + " ";
		}
		sql = sql + " ORDER BY M.SORT";
		return SysMenu.dao.find(sql);
	}

	public RenderLayuiBean listData(Controller menuController) {
		List<SysMenu> list = SysMenu.dao.find(
				"SELECT T.*, (CASE  WHEN (SELECT COUNT(ID) AS COUNT FROM SYS_MENU WHERE PARENTID=T.ID)>0 THEN TRUE ELSE  FALSE END ) AS HAVECHILD   FROM SYS_MENU  T  ORDER BY T.SORT ASC");
		RenderLayuiBean renderBean = new RenderLayuiBean();
		renderBean.setCode(0);
		renderBean.setMsg("查询成功");
		renderBean.setData(list);
		renderBean.setCount(list.size());
		return renderBean;
	}

	/***
	 * 菜单转成ZTreeNode
	 * @param olist 数据 open 是否展开所有 ifOnlyLeaf 是否只选叶子
	 * @return
	 */
	public List<ZtreeNode> toZTreeNode(List<SysMenu> olist, Boolean open, Boolean ifOnlyLeaf) {
		List<ZtreeNode> list = new ArrayList<ZtreeNode>();
		for (SysMenu o : olist) {
			ZtreeNode node = toZTreeNode(o);
			if (o.getChildren() != null && o.getChildren().size() > 0) {// 如果有孩子
				node.setChildren(toZTreeNode(o.getChildren(), open, ifOnlyLeaf));
				if (ifOnlyLeaf) {// 如果只选叶子
					node.setNocheck(true);
				}
				node.setIsParent(true);
			}
			node.setOpen(open);
			list.add(node);
		}
		return list;
	}

	public ZtreeNode toZTreeNode(SysMenu org) {
		ZtreeNode node = new ZtreeNode();
		node.setId(org.getID());
		node.setName(org.getNAME());
		node.setType(org.getTYPE());
		node.setParentId(org.getPARENTID());
		return node;
	}

	public List<SysMenu> authTree(String roleId) {
		return getAuthChildrenAllTree("root", roleId);
	}

	public List<SysMenu> getAuthChildrenAllTree(String pId, String userId) {
		List<SysMenu> list = getChildrenByPidAndRoleId(pId, userId);
		for (SysMenu o : list) {
			o.setChildren(getAuthChildrenAllTree(o.getID(), userId));
		}
		return list;
	}

	/***
	 * 根据id 查询孩子
	 * 
	 * @param id
	 * @return
	 */
	public List<SysMenu> getChildrenByPidAndRoleId(String id, String roleId) {
		return SysMenu.dao.find("select t.id,t.parentId as pId, t.name, "
				+ " (CASE  WHEN (SELECT count(id) as count from sys_role_permissions where menuId=t.id and roleId=? )>0 THEN true ELSE  false END ) as checked   "
				+ " from sys_menu  t  ORDER BY t.sort asc", roleId);
	}

}
