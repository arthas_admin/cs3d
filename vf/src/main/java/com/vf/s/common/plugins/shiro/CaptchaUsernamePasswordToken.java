package com.vf.s.common.plugins.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

/**  
 * @ClassName: CaptchaUsernamePasswordToken  
 * @Description: 在用户名和密码的基础上添加验证码的Token  
 */
public class CaptchaUsernamePasswordToken extends UsernamePasswordToken {
	
    private static final long serialVersionUID = 4676958151524148623L;
    
    private String captcha;
    
    /**
     * 0--有验证码
     */
    private String loginType="0";
    
    public CaptchaUsernamePasswordToken(String username, String password, boolean rememberMe, String host, String captcha) {
        super(username, password, rememberMe, host);
        this.captcha = captcha;
    }
    
    public CaptchaUsernamePasswordToken(String username, String password, boolean rememberMe, String host, String captcha,String loginType) {
        super(username, password, rememberMe, host);
        this.captcha = captcha;
        this.loginType=loginType;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
    
    
}
