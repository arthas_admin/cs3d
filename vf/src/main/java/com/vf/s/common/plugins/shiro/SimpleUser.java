package com.vf.s.common.plugins.shiro;

import java.io.Serializable;

@SuppressWarnings({ "serial"})
public class SimpleUser implements Serializable {

	private final String id;
	private final String username;
	private String name;
	
	public SimpleUser(String id, String username, String name) {
		super();
		this.id = id;
		this.username = username;
		this.name = name;
	}


	public String getId() {
		return id;
	}


	public String getUsername() {
		return username;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


}
