package com.vf.s.common.render;

import java.util.List;

import com.vf.s.common.model.biz.BizLine;
import com.vf.s.common.model.biz.BizStyleLine;

public class PolyLineRender {
	private BizLine line;
	private List<BizStyleLine> styles;
	public BizLine getLine() {
		return line;
	}
	public void setLine(BizLine line) {
		this.line = line;
	}
	public List<BizStyleLine> getStyles() {
		return styles;
	}
	public void setStyles(List<BizStyleLine> styles) {
		this.styles = styles;
	}
	
	
	
	
}
