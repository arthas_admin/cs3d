package com.vf.s.common.plugins.shiro;

import java.io.Serializable;
import java.util.Collection;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;

import com.jfinal.log.Log;

public class ShrioSessionDao extends AbstractSessionDAO {

	protected final Log LOG = Log.getLog(getClass());

	public void update(Session session) throws UnknownSessionException {
		LOG.debug("session-update");
	}

	public void delete(Session session) {
		LOG.debug("session-update");
	}

	public Collection<Session> getActiveSessions() {
		LOG.debug("session-update");
		return null;
	}

	protected Serializable doCreate(Session session) {
		LOG.debug("session-update");
		return null;
	}

	@Override
	protected Session doReadSession(Serializable sessionId) {
		LOG.debug("session-update");
		return null;
	}

}
