package com.vf.s.common.render;

import java.util.LinkedList;
import java.util.List;

import com.vf.s.common.model.biz.BizPage;
import com.vf.s.common.model.biz.BizPanorama;
import com.vf.s.common.model.biz.BizPicture;
import com.vf.s.common.model.biz.BizProperty;
import com.vf.s.common.model.biz.BizText;
import com.vf.s.common.model.biz.BizVideo;
import com.vf.s.common.model.biz.BizView;

public class ViewRender {
	private BizView view;
	private BizText text;
	private List<BizPanorama> panoramas=new LinkedList<BizPanorama>();
	private List<BizPicture> pictures =new LinkedList<BizPicture>();
	private List<BizVideo> videos =new LinkedList<BizVideo>();
	private List<BizPage> pages =new LinkedList<BizPage>();
	private List<BizProperty> properties =new LinkedList<BizProperty>();
	public BizView getView() {
		return view;
	}
	public void setView(BizView view) {
		this.view = view;
	}
	
	public BizText getText() {
		return text;
	}
	public void setText(BizText text) {
		this.text = text;
	}
	public List<BizPanorama> getPanoramas() {
		return panoramas;
	}
	public void setPanoramas(List<BizPanorama> panoramas) {
		this.panoramas = panoramas;
	}
	public List<BizPicture> getPictures() {
		return pictures;
	}
	public void setPictures(List<BizPicture> pictures) {
		this.pictures = pictures;
	}
	public List<BizVideo> getVideos() {
		return videos;
	}
	public void setVideos(List<BizVideo> videos) {
		this.videos = videos;
	}
	public List<BizPage> getPages() {
		return pages;
	}
	public void setPages(List<BizPage> pages) {
		this.pages = pages;
	}
	public List<BizProperty> getProperties() {
		return properties;
	}
	public void setProperties(List<BizProperty> properties) {
		this.properties = properties;
	}
	
	
	
}
