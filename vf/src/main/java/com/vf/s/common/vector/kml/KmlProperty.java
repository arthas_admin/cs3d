package com.vf.s.common.vector.kml;

import java.util.List;

import de.micromata.opengis.kml.v_2_2_0.Folder;

public class KmlProperty {
	
	private List<Folder> folders;
    private List<KmlPoint> kmlPoints;
    private List<KmlLine> kmlLines;
    private List<KmlPolygon> kmlPolygons;
 
    public List<Folder> getFolders() {
		return folders;
	}

	public void setFolders(List<Folder> folders) {
		this.folders = folders;
	}

	public List<KmlPoint> getKmlPoints() {
        return kmlPoints;
    }
 
    public void setKmlPoints(List<KmlPoint> kmlPoints) {
        this.kmlPoints = kmlPoints;
    }
 
    public List<KmlLine> getKmlLines() {
        return kmlLines;
    }
 
    public void setKmlLines(List<KmlLine> kmlLines) {
        this.kmlLines = kmlLines;
    }
 
    public List<KmlPolygon> getKmlPolygons() {
        return kmlPolygons;
    }
 
    public void setKmlPolygons(List<KmlPolygon> kmlPolygons) {
        this.kmlPolygons = kmlPolygons;
    }
}