package com.vf.s.common.plugins.shiro;

import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpSessionListener;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListenerAdapter;

import com.jfinal.log.Log;

public class ShiroSessionListener  extends SessionListenerAdapter implements HttpSessionListener{

	protected final Log LOG = Log.getLog(getClass());

    private final AtomicInteger sessionCount = new AtomicInteger(0);

	
	@Override
	public void onStart(Session session) {
       sessionCount.incrementAndGet();
       LOG.debug(String.format("当前在线人数(登录成功+1)：%s", sessionCount.get()));
	}

	@Override
	public void onStop(Session session) {
        sessionCount.decrementAndGet();
        LOG.debug(String.format("当前在线人数(退出登录-1)：%s", sessionCount.get()));
	}

	@Override
	public void onExpiration(Session session) {
        sessionCount.decrementAndGet();
        LOG.debug(String.format("当前在线人数(登陆过期-1)：%s", sessionCount.get()));
	}

}
