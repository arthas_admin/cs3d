package com.vf.s.common.model.scene.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseSceneLayer<M extends BaseSceneLayer<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setSceneId(java.lang.String sceneId) {
		set("sceneId", sceneId);
		return (M)this;
	}
	
	public java.lang.String getSceneId() {
		return getStr("sceneId");
	}

	public M setParentId(java.lang.String parentId) {
		set("parentId", parentId);
		return (M)this;
	}
	
	public java.lang.String getParentId() {
		return getStr("parentId");
	}

	public M setName(java.lang.String name) {
		set("name", name);
		return (M)this;
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}

	public M setCode(java.lang.String code) {
		set("code", code);
		return (M)this;
	}
	
	public java.lang.String getCode() {
		return getStr("code");
	}

	public M setType(java.lang.String type) {
		set("type", type);
		return (M)this;
	}
	
	public java.lang.String getType() {
		return getStr("type");
	}

	public M setIcon(java.lang.String icon) {
		set("icon", icon);
		return (M)this;
	}
	
	public java.lang.String getIcon() {
		return getStr("icon");
	}

	public M setSort(java.lang.Integer sort) {
		set("sort", sort);
		return (M)this;
	}
	
	public java.lang.Integer getSort() {
		return getInt("sort");
	}

	public M setDefaultStyleId(java.lang.String defaultStyleId) {
		set("defaultStyleId", defaultStyleId);
		return (M)this;
	}
	
	public java.lang.String getDefaultStyleId() {
		return getStr("defaultStyleId");
	}

	public M setHoverStyleId(java.lang.String hoverStyleId) {
		set("hoverStyleId", hoverStyleId);
		return (M)this;
	}
	
	public java.lang.String getHoverStyleId() {
		return getStr("hoverStyleId");
	}

	public M setSelectedStyleId(java.lang.String selectedStyleId) {
		set("selectedStyleId", selectedStyleId);
		return (M)this;
	}
	
	public java.lang.String getSelectedStyleId() {
		return getStr("selectedStyleId");
	}

	public M setIsShow(java.lang.String isShow) {
		set("isShow", isShow);
		return (M)this;
	}
	
	public java.lang.String getIsShow() {
		return getStr("isShow");
	}

}
