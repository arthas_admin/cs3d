package com.vf.s.common.render;

import java.util.List;

import com.vf.s.common.model.biz.BizPage;
import com.vf.s.common.model.biz.BizProperty;
import com.vf.s.common.model.biz.BizText;

public class MouseEventRender {
	
	private BizText bizText;
	private List<BizPage> bizPages;
	List<BizProperty> bizPropertys;
	
	public BizText getBizText() {
		return bizText;
	}
	public void setBizText(BizText bizText) {
		this.bizText = bizText;
	}
	public List<BizPage> getBizPages() {
		return bizPages;
	}
	public void setBizPages(List<BizPage> bizPages) {
		this.bizPages = bizPages;
	}
	public List<BizProperty> getBizPropertys() {
		return bizPropertys;
	}
	public void setBizPropertys(List<BizProperty> bizPropertys) {
		this.bizPropertys = bizPropertys;
	}

	
	
	
	
}
