package com.vf.s.common.model.scene;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.core.model.ZtreeNode;
import com.vf.s.common.model.scene.base.BaseBizSceneMenu;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class BizSceneMenu extends BaseBizSceneMenu<BizSceneMenu> {
	public static final BizSceneMenu dao = new BizSceneMenu().dao();
	public static final String TABLE_NAME = "BIZ_SCENE_MENU";
	
	private List<BizSceneMenu> children;

	public List<BizSceneMenu> getChildren() {
		if (super.get("children") == null) {
			super.put("children", this.children);
		}
		return super.get("children");
	}

	public void setChildren(List<BizSceneMenu> children) {
		super.put("children", children);
	}
	
	
	
	
	public List<BizSceneMenu> getChildrenAllTree(String pId,String type) {
		List<BizSceneMenu> list = getChildrenByPid(pId,type);
		for (BizSceneMenu o : list) {
			o.setChildren(getChildrenAllTree(o.getId(),type));
		}
		return list;
	}
	
	public List<BizSceneMenu> getChildrenByPid(String id,String type) {
		String sql = "select * from "+TABLE_NAME+" m where m.parentId='" + id + "'  ";
		if(!StrKit.isBlank(type)) {
			//sql = sql + " and m.type in () ";
		}
		sql = sql + " order by m.sort asc";
		return BizSceneMenu.dao.find(sql);
	}
	
	
	public List<ZtreeNode> toZTreeNode(List<BizSceneMenu> olist, Boolean open, Boolean ifOnlyLeaf) {
		List<ZtreeNode> list = new ArrayList<ZtreeNode>();
		for (BizSceneMenu o : olist) {
			ZtreeNode node = toZTreeNode(o);
			if (o.getChildren() != null && o.getChildren().size() > 0) {// 如果有孩子
				node.setChildren(toZTreeNode(o.getChildren(), open, ifOnlyLeaf));
				if (ifOnlyLeaf) {// 如果只选叶子
					node.setNocheck(true);
				}
				node.setIsParent(true);
			}
			node.setOpen(false);
//			if ("1".equals(node.getType())) {
//				node.setOpen(true);
//			} else {
//				node.setOpen(open);
//			}
			list.add(node);
		}
		return list;
	}
	
	public ZtreeNode toZTreeNode(BizSceneMenu layer) {
		ZtreeNode node = new ZtreeNode();
		node.setId(layer.getId());
		node.setName(layer.getName());
		node.setLayerId(layer.getLayerId());
		node.setLabel(layer.getName());
		node.setParentId(layer.getParentId());
		node.setType(layer.getType());
		node.setCode(layer.getCode());
	//	node.setUrl(layer.getUrl());
		node.setIcon(layer.getIcon());
		node.setCameraX(layer.getCameraX());
		node.setCameraY(layer.getCameraY());
		node.setCameraZ(layer.getCameraZ());
		node.setHeading(layer.getHeading());
		node.setPitch(layer.getPitch());
		node.setRoll(layer.getRoll());
		return node;
	}
}
