package com.vf.s.common.model.biz.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseBizStyleLine<M extends BaseBizStyleLine<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setName(java.lang.String name) {
		set("name", name);
		return (M)this;
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}

	public M setCode(java.lang.String code) {
		set("code", code);
		return (M)this;
	}
	
	public java.lang.String getCode() {
		return getStr("code");
	}

	public M setStatus(java.lang.String status) {
		set("status", status);
		return (M)this;
	}
	
	public java.lang.String getStatus() {
		return getStr("status");
	}

	public M setDistanceDisplayConditionX(java.lang.Double distanceDisplayConditionX) {
		set("distanceDisplayConditionX", distanceDisplayConditionX);
		return (M)this;
	}
	
	public java.lang.Double getDistanceDisplayConditionX() {
		return getDouble("distanceDisplayConditionX");
	}

	public M setDistanceDisplayConditionY(java.lang.Double distanceDisplayConditionY) {
		set("distanceDisplayConditionY", distanceDisplayConditionY);
		return (M)this;
	}
	
	public java.lang.Double getDistanceDisplayConditionY() {
		return getDouble("distanceDisplayConditionY");
	}

	public M setCreateTime(java.util.Date createTime) {
		set("createTime", createTime);
		return (M)this;
	}
	
	public java.util.Date getCreateTime() {
		return get("createTime");
	}

	public M setLoop(java.lang.String loop) {
		set("loop", loop);
		return (M)this;
	}
	
	public java.lang.String getLoop() {
		return getStr("loop");
	}

	public M setSort(java.lang.Integer sort) {
		set("sort", sort);
		return (M)this;
	}
	
	public java.lang.Integer getSort() {
		return getInt("sort");
	}

	public M setWidth(java.lang.Double width) {
		set("width", width);
		return (M)this;
	}
	
	public java.lang.Double getWidth() {
		return getDouble("width");
	}

	public M setMaterialType(java.lang.String materialType) {
		set("materialType", materialType);
		return (M)this;
	}
	
	public java.lang.String getMaterialType() {
		return getStr("materialType");
	}

	public M setClampToGround(java.lang.String clampToGround) {
		set("clampToGround", clampToGround);
		return (M)this;
	}
	
	public java.lang.String getClampToGround() {
		return getStr("clampToGround");
	}

	public M setColor(java.lang.String color) {
		set("color", color);
		return (M)this;
	}
	
	public java.lang.String getColor() {
		return getStr("color");
	}

	public M setGapColor(java.lang.String gapColor) {
		set("gapColor", gapColor);
		return (M)this;
	}
	
	public java.lang.String getGapColor() {
		return getStr("gapColor");
	}

	public M setDashLength(java.lang.Double dashLength) {
		set("dashLength", dashLength);
		return (M)this;
	}
	
	public java.lang.Double getDashLength() {
		return getDouble("dashLength");
	}

	public M setDashPattern(java.lang.String dashPattern) {
		set("dashPattern", dashPattern);
		return (M)this;
	}
	
	public java.lang.String getDashPattern() {
		return getStr("dashPattern");
	}

	public M setOutlineWidth(java.lang.Double outlineWidth) {
		set("outlineWidth", outlineWidth);
		return (M)this;
	}
	
	public java.lang.Double getOutlineWidth() {
		return getDouble("outlineWidth");
	}

	public M setOutlineColor(java.lang.String outlineColor) {
		set("outlineColor", outlineColor);
		return (M)this;
	}
	
	public java.lang.String getOutlineColor() {
		return getStr("outlineColor");
	}

	public M setGlowPower(java.lang.Double glowPower) {
		set("glowPower", glowPower);
		return (M)this;
	}
	
	public java.lang.Double getGlowPower() {
		return getDouble("glowPower");
	}

	public M setInterval(java.lang.Integer interval) {
		set("interval", interval);
		return (M)this;
	}
	
	public java.lang.Integer getInterval() {
		return getInt("interval");
	}

	public M setOdType(java.lang.String odType) {
		set("odType", odType);
		return (M)this;
	}
	
	public java.lang.String getOdType() {
		return getStr("odType");
	}

	public M setDirection(java.lang.Integer direction) {
		set("direction", direction);
		return (M)this;
	}
	
	public java.lang.Integer getDirection() {
		return getInt("direction");
	}

	public M setImageUrl(java.lang.String imageUrl) {
		set("imageUrl", imageUrl);
		return (M)this;
	}
	
	public java.lang.String getImageUrl() {
		return getStr("imageUrl");
	}

	public M setEffect(java.lang.String effect) {
		set("effect", effect);
		return (M)this;
	}
	
	public java.lang.String getEffect() {
		return getStr("effect");
	}

}
