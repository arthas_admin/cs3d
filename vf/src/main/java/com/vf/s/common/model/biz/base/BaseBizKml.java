package com.vf.s.common.model.biz.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseBizKml<M extends BaseBizKml<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setName(java.lang.String name) {
		set("name", name);
		return (M)this;
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}

	public M setPath(java.lang.String path) {
		set("path", path);
		return (M)this;
	}
	
	public java.lang.String getPath() {
		return getStr("path");
	}

	public M setSceneId(java.lang.String sceneId) {
		set("sceneId", sceneId);
		return (M)this;
	}
	
	public java.lang.String getSceneId() {
		return getStr("sceneId");
	}

	public M setSceneName(java.lang.String sceneName) {
		set("sceneName", sceneName);
		return (M)this;
	}
	
	public java.lang.String getSceneName() {
		return getStr("sceneName");
	}

	public M setCreateTime(java.util.Date createTime) {
		set("createTime", createTime);
		return (M)this;
	}
	
	public java.util.Date getCreateTime() {
		return get("createTime");
	}

	public M setCreateUserId(java.lang.String createUserId) {
		set("createUserId", createUserId);
		return (M)this;
	}
	
	public java.lang.String getCreateUserId() {
		return getStr("createUserId");
	}

	public M setCreateUserName(java.lang.String createUserName) {
		set("createUserName", createUserName);
		return (M)this;
	}
	
	public java.lang.String getCreateUserName() {
		return getStr("createUserName");
	}

	public M setStatus(java.lang.String status) {
		set("status", status);
		return (M)this;
	}
	
	public java.lang.String getStatus() {
		return getStr("status");
	}

	public M setRemark(java.lang.String remark) {
		set("remark", remark);
		return (M)this;
	}
	
	public java.lang.String getRemark() {
		return getStr("remark");
	}

}
