package com.vf.s.common.model.sys;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.vf.s.common.model.sys.base.*;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class SysRole extends BaseSysRole<SysRole> {
	public static final SysRole dao = new SysRole().dao();
	public static final String TABLE_NAME = "SYS_ROLE";
	public static boolean excuteSql(List<String> sqlList) {
		int[] d= Db.use(dao._getConfig().getName()).batch(sqlList, sqlList.size());
		return d!=null && d.length>0 ?true:false;
	}
}
