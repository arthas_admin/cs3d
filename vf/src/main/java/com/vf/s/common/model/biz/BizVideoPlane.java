package com.vf.s.common.model.biz;

import com.vf.s.common.model.biz.base.BaseBizVideoPlane;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class BizVideoPlane extends BaseBizVideoPlane<BizVideoPlane> {
	public static final BizVideoPlane dao = new BizVideoPlane().dao();
	public static final String TABLE_NAME = "BIZ_VIDEO_PLANE";
	private BizVideo video;
	public BizVideo getVideo() {
		if (super.get("video") == null) {
			super.put("video", this.video);
		}
		return super.get("video");
	}
	public void setVideo(BizVideo video) {
		super.put("video", video);
	}
}
