package com.vf.s.common.cache.style;

import java.util.HashMap;
import java.util.Map;

public class MapLayerCache {
	private static Map<String, Object> LAYER_STYLE = new HashMap<String, Object>();

	public static void put(String key, Object stylePoint) {
		LAYER_STYLE.put(key, stylePoint);
	}

	public static Object get(String key) {
		return LAYER_STYLE.get(key);
	}

	public static void remove(String key) {
		LAYER_STYLE.remove(key);
	}
	public static Map<String, Object> gets() {
		return LAYER_STYLE;
	}

}
