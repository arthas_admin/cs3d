package com.vf.s.common.model.scene;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.core.model.ZtreeNode;
import com.vf.s.common.model.scene.base.BaseSceneLayer;

@SuppressWarnings("serial")
public class SceneLayer extends BaseSceneLayer<SceneLayer> {
	public static final SceneLayer dao = new SceneLayer().dao();
	public static final String TABLE_NAME = "BIZ_SCENE_LAYER";
	
	private List<SceneLayer> children;

	public List<SceneLayer> getChildren() {
		if (super.get("children") == null) {
			super.put("children", this.children);
		}
		return super.get("children");
	}
	public void setChildren(List<SceneLayer> children) {
		super.put("children", children);
	}
	
	
	public ZtreeNode toZTreeNode(SceneLayer layer) {
		ZtreeNode node = new ZtreeNode();
		node.setId(layer.getId());
		node.setName(layer.getName());
		node.setType(layer.getType());
		if(StrKit.equals(layer.getIsShow(),"1")) {
			node.setChecked(true);
		}else {
			node.setChecked(false);
		}
		return node;
	}

}
