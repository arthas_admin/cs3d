package com.vf.s.common.model.biz.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseBizVideo<M extends BaseBizVideo<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setSceneId(java.lang.String sceneId) {
		set("sceneId", sceneId);
		return (M)this;
	}
	
	public java.lang.String getSceneId() {
		return getStr("sceneId");
	}

	public M setClassifyId(java.lang.String classifyId) {
		set("classifyId", classifyId);
		return (M)this;
	}
	
	public java.lang.String getClassifyId() {
		return getStr("classifyId");
	}

	public M setName(java.lang.String name) {
		set("name", name);
		return (M)this;
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}

	public M setCode(java.lang.String code) {
		set("code", code);
		return (M)this;
	}
	
	public java.lang.String getCode() {
		return getStr("code");
	}

	public M setType(java.lang.String type) {
		set("type", type);
		return (M)this;
	}
	
	public java.lang.String getType() {
		return getStr("type");
	}

	public M setUrl(java.lang.String url) {
		set("url", url);
		return (M)this;
	}
	
	public java.lang.String getUrl() {
		return getStr("url");
	}

	public M setIp(java.lang.String ip) {
		set("ip", ip);
		return (M)this;
	}
	
	public java.lang.String getIp() {
		return getStr("ip");
	}

	public M setUsername(java.lang.String username) {
		set("username", username);
		return (M)this;
	}
	
	public java.lang.String getUsername() {
		return getStr("username");
	}

	public M setPassword(java.lang.String password) {
		set("password", password);
		return (M)this;
	}
	
	public java.lang.String getPassword() {
		return getStr("password");
	}

	public M setPort(java.lang.String port) {
		set("port", port);
		return (M)this;
	}
	
	public java.lang.String getPort() {
		return getStr("port");
	}

	public M setChannelNum(java.lang.String channelNum) {
		set("channelNum", channelNum);
		return (M)this;
	}
	
	public java.lang.String getChannelNum() {
		return getStr("channelNum");
	}

	public M setDeviceType(java.lang.String deviceType) {
		set("deviceType", deviceType);
		return (M)this;
	}
	
	public java.lang.String getDeviceType() {
		return getStr("deviceType");
	}

	public M setDeviceStatus(java.lang.String deviceStatus) {
		set("deviceStatus", deviceStatus);
		return (M)this;
	}
	
	public java.lang.String getDeviceStatus() {
		return getStr("deviceStatus");
	}

	public M setMonitorType(java.lang.String monitorType) {
		set("monitorType", monitorType);
		return (M)this;
	}
	
	public java.lang.String getMonitorType() {
		return getStr("monitorType");
	}

	public M setProtocol(java.lang.String protocol) {
		set("protocol", protocol);
		return (M)this;
	}
	
	public java.lang.String getProtocol() {
		return getStr("protocol");
	}

	public M setAddress(java.lang.String address) {
		set("address", address);
		return (M)this;
	}
	
	public java.lang.String getAddress() {
		return getStr("address");
	}

	public M setSynStatus(java.lang.String synStatus) {
		set("synStatus", synStatus);
		return (M)this;
	}
	
	public java.lang.String getSynStatus() {
		return getStr("synStatus");
	}

	public M setCreateTime(java.util.Date createTime) {
		set("createTime", createTime);
		return (M)this;
	}
	
	public java.util.Date getCreateTime() {
		return get("createTime");
	}

	public M setCreateUserId(java.lang.String createUserId) {
		set("createUserId", createUserId);
		return (M)this;
	}
	
	public java.lang.String getCreateUserId() {
		return getStr("createUserId");
	}

	public M setCreateUserName(java.lang.String createUserName) {
		set("createUserName", createUserName);
		return (M)this;
	}
	
	public java.lang.String getCreateUserName() {
		return getStr("createUserName");
	}

	public M setEvent(java.lang.String event) {
		set("event", event);
		return (M)this;
	}
	
	public java.lang.String getEvent() {
		return getStr("event");
	}

	public M setFar(java.lang.String far) {
		set("far", far);
		return (M)this;
	}
	
	public java.lang.String getFar() {
		return getStr("far");
	}

	public M setFov(java.lang.String fov) {
		set("fov", fov);
		return (M)this;
	}
	
	public java.lang.String getFov() {
		return getStr("fov");
	}

	public M setNear(java.lang.String near) {
		set("near", near);
		return (M)this;
	}
	
	public java.lang.String getNear() {
		return getStr("near");
	}

	public M setHeading(java.lang.String heading) {
		set("heading", heading);
		return (M)this;
	}
	
	public java.lang.String getHeading() {
		return getStr("heading");
	}

	public M setPitch(java.lang.String pitch) {
		set("pitch", pitch);
		return (M)this;
	}
	
	public java.lang.String getPitch() {
		return getStr("pitch");
	}

	public M setRoll(java.lang.String roll) {
		set("roll", roll);
		return (M)this;
	}
	
	public java.lang.String getRoll() {
		return getStr("roll");
	}

	public M setAspectRatio(java.lang.String aspectRatio) {
		set("aspectRatio", aspectRatio);
		return (M)this;
	}
	
	public java.lang.String getAspectRatio() {
		return getStr("aspectRatio");
	}
	
	public M setGroupName(java.lang.String groupName) {
		set("groupName", groupName);
		return (M)this;
	}
	
	public java.lang.String getGroupName() {
		return getStr("groupName");
	}

}
