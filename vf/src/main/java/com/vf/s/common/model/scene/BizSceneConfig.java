package com.vf.s.common.model.scene;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.core.model.ZtreeNode;
import com.vf.s.common.model.scene.base.BaseBizSceneConfig;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class BizSceneConfig extends BaseBizSceneConfig<BizSceneConfig> {
	public static final BizSceneConfig dao = new BizSceneConfig().dao();
	public static final String TABLE_NAME = "BIZ_SCENE_CONFIG";
	
	
	private List<BizSceneConfig> children;

	public List<BizSceneConfig> getChildren() {
		if (super.get("children") == null) {
			super.put("children", this.children);
		}
		return super.get("children");
	}

	public void setChildren(List<BizSceneConfig> children) {
		super.put("children", children);
	}
	
	
	public List<BizSceneConfig> getChildrenAllTree(String pId,String type) {
		List<BizSceneConfig> list = getChildrenByPid(pId,type);
		for (BizSceneConfig o : list) {
			o.setChildren(getChildrenAllTree(o.getId(),type));
		}
		return list;
	}
	
	public List<BizSceneConfig> getChildrenByPid(String id,String type) {
		String sql = "select * from "+TABLE_NAME+" m where m.parentId='" + id + "'  ";
		if(!StrKit.isBlank(type)) {
			//sql = sql + " and m.type in () ";
		}
		sql = sql + " order by m.sort asc";
		return BizSceneConfig.dao.find(sql);
	}
	
	
	public List<ZtreeNode> toZTreeNode(List<BizSceneConfig> olist, Boolean open, Boolean ifOnlyLeaf) {
		List<ZtreeNode> list = new ArrayList<ZtreeNode>();
		for (BizSceneConfig o : olist) {
			ZtreeNode node = toZTreeNode(o);
			if (o.getChildren() != null && o.getChildren().size() > 0) {// 如果有孩子
				node.setChildren(toZTreeNode(o.getChildren(), open, ifOnlyLeaf));
				if (ifOnlyLeaf) {// 如果只选叶子
					node.setNocheck(true);
				}
				node.setIsParent(true);
			}
			node.setOpen(false);
			list.add(node);
		}
		return list;
	}
	
	public ZtreeNode toZTreeNode(BizSceneConfig layer) {
		ZtreeNode node = new ZtreeNode();
		node.setId(layer.getId());
		node.setName(layer.getName());
		node.setLayerId(layer.getLayerId());
		node.setLabel(layer.getName());
		node.setParentId(layer.getParentId());
		node.setType(layer.getType());
		node.setCode(layer.getCode());
		node.setIcon(layer.getIcon());
		node.setCameraX(layer.getX());
		node.setCameraY(layer.getY());
		node.setCameraZ(layer.getZ());
		node.setHeading(layer.getHeading());
		node.setPitch(layer.getPitch());
		node.setRoll(layer.getRoll());
		return node;
	}
	
}
