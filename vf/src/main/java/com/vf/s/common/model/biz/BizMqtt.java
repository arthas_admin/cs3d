package com.vf.s.common.model.biz;

import com.vf.s.common.model.biz.base.BaseBizMqtt;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class BizMqtt extends BaseBizMqtt<BizMqtt> {
	public static final BizMqtt dao = new BizMqtt().dao();
	public static final String TABLE_NAME = "BIZ_MQTT";

}
