package com.vf.s.common.vector.kml;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Placemark;

import java.util.List;
 
public class KmlLine {
    private List<Coordinate> points;
    private Placemark placemark;
 
    public List<Coordinate> getPoints() {
        return points;
    }
 
    public void setPoints(List<Coordinate> points) {
        this.points = points;
    }

	public Placemark getPlacemark() {
		return placemark;
	}

	public void setPlacemark(Placemark placemark) {
		this.placemark = placemark;
	}
    
    
    
}