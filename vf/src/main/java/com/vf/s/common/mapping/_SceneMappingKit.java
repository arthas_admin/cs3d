package com.vf.s.common.mapping;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.vf.s.common.model.scene.*;

public class _SceneMappingKit {
	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping(Scene.TABLE_NAME, "id", Scene.class);
		arp.addMapping(SceneProvider.TABLE_NAME, "id", SceneProvider.class);
		arp.addMapping(BizSceneConfig.TABLE_NAME, "id", BizSceneConfig.class);
		arp.addMapping(SceneLayer.TABLE_NAME, "id", SceneLayer.class);
		arp.addMapping(BizSceneMenu.TABLE_NAME, "id", BizSceneMenu.class);
		arp.addMapping(BizSceneFeature.TABLE_NAME, "id", BizSceneFeature.class);
		arp.addMapping(BizSceneModel.TABLE_NAME, "id", BizSceneModel.class);
		arp.addMapping(BizSceneRoam.TABLE_NAME, "id", BizSceneRoam.class);
		arp.addMapping(BizSceneRoamPoint.TABLE_NAME, "id", BizSceneRoamPoint.class);
		arp.addMapping(SceneSkybox.TABLE_NAME, "id", SceneSkybox.class);
	}
}
