package com.vf.s.common.model.biz;

import com.vf.s.common.model.biz.base.BaseBizStyleLine;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class BizStyleLine extends BaseBizStyleLine<BizStyleLine> {
	public static final BizStyleLine dao = new BizStyleLine().dao();
	public static final String TABLE_NAME = "BIZ_STYLE_LINE";

}
