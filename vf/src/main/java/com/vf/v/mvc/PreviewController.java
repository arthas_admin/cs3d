package com.vf.v.mvc;

import com.vf.core.controller.BaseController;

public class PreviewController extends BaseController{
	
	public void index() {
		this.setAttr("sceneId", this.getPara("sceneId"));
		this.render("index.html");
	}

}
