/**
 * @开发版权 云南赢中吉洋智能技术有限公司（YNYZ）
 * @项目名称 vf
 * @版本信息 v1.0
 * @开发人员 zhous
 * @开发日期 2018-11-20
 * @修订日期
 * @描述  GlobalHandler
 */
package com.vf.core.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.jfinal.handler.Handler;
import com.jfinal.log.Log;
import com.vf.core.util.IpUtil;

/**
 * 全局Handler，设置一些通用功能 描述：主要是一些全局变量的设置，再就是日志记录开始和结束操作
 */
public class GlobalHandler extends Handler {

	protected final Log LOG = Log.getLog(getClass());

	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		try {
			String ctx = request.getContextPath();
			request.setAttribute("ctx", ctx);
//			LOG.debug(String.format("当前请求类型：{%s}，ip:%s",request.getMethod(),IpUtil.getIpAddress(request)));
//			if (RequestUtil.isAjaxRequest(request)) {
//				LOG.debug("ajaxRequest");
//			} else {
//				LOG.debug("normal");
//			}
		}catch (Exception e) {
			LOG.error(String.format("当前请求类型：{%s}，ip:%s，ip:%s",request.getMethod(),IpUtil.getIpAddress(request),e.getMessage()));

		}
		next.handle(target, request, response, isHandled);
	}

}