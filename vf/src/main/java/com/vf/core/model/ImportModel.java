package com.vf.core.model;

import java.util.List;
import java.util.Map;

public class ImportModel {
	
	private String filePath;
	
	private List<Map<String, String>> sheets;
	
	private Map<String, List<Map<String, String>>> fields;
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public List<Map<String, String>> getSheets() {
		return sheets;
	}

	public void setSheets(List<Map<String, String>> sheets) {
		this.sheets = sheets;
	}

	public Map<String, List<Map<String, String>>> getFields() {
		return fields;
	}

	public void setFields(Map<String, List<Map<String, String>>> fields) {
		this.fields = fields;
	}
	
}
