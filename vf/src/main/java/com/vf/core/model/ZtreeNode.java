package com.vf.core.model;


import java.util.List;

public class ZtreeNode {
	
	/**
	 * 节点id
	 */
	private String id;

	private String type;
	/**
	 * 节点名称
	 */
	private String name;
	
	private String label;
	/***
	 * 是否展开
	 */
	private boolean open;

	/**
	 * 是否上级节点
	 */
	private boolean isParent;

	/**
	 * 是否选中
	 */
	private boolean checked;

	/**
	 * 是否选中
	 */
	private boolean nocheck;
	
	/**
	 * 节点图标
	 */
	private String icon;
	
	private long level;
	private String code;
	private String url;
	private String parentId;
	private String layerId;
	private String layerType;
	private String heading;
	private String pitch;
	private String roll;
	private String cameraX;
	private String cameraY;
	private String cameraZ;

	private String defaultStyleId;
	private String hoverStyleId;
	private String selectedStyleId;
	
	private int count;
	
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	/**
	 * 子节点数据
	 */
	private List<ZtreeNode> children;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(boolean isParent) {
		this.isParent = isParent;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isNocheck() {
		return nocheck;
	}

	public void setNocheck(boolean nocheck) {
		this.nocheck = nocheck;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public List<ZtreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<ZtreeNode> children) {
		this.children = children;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public void setParent(boolean isParent) {
		this.isParent = isParent;
	}
	public long getLevel() {
		return level;
	}

	public void setLevel(long level) {
		this.level = level;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public String getPitch() {
		return pitch;
	}

	public void setPitch(String pitch) {
		this.pitch = pitch;
	}

	public String getRoll() {
		return roll;
	}

	public void setRoll(String roll) {
		this.roll = roll;
	}

	public String getCameraX() {
		return cameraX;
	}

	public void setCameraX(String cameraX) {
		this.cameraX = cameraX;
	}

	public String getCameraY() {
		return cameraY;
	}

	public void setCameraY(String cameraY) {
		this.cameraY = cameraY;
	}

	public String getCameraZ() {
		return cameraZ;
	}

	public void setCameraZ(String cameraZ) {
		this.cameraZ = cameraZ;
	}

	public String getLayerId() {
		return layerId;
	}

	public void setLayerId(String layerId) {
		this.layerId = layerId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDefaultStyleId() {
		return defaultStyleId;
	}

	public void setDefaultStyleId(String defaultStyleId) {
		this.defaultStyleId = defaultStyleId;
	}

	public String getHoverStyleId() {
		return hoverStyleId;
	}

	public void setHoverStyleId(String hoverStyleId) {
		this.hoverStyleId = hoverStyleId;
	}

	public String getSelectedStyleId() {
		return selectedStyleId;
	}

	public void setSelectedStyleId(String selectedStyleId) {
		this.selectedStyleId = selectedStyleId;
	}

	public String getLayerType() {
		return layerType;
	}

	public void setLayerType(String layerType) {
		this.layerType = layerType;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	
}
