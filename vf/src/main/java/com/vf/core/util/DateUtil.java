package com.vf.core.util;

/*
*java中对日期的加减操作
*gc.add(1,-1)表示年份减一.
*gc.add(2,-1)表示月份减一.
*gc.add(3.-1)表示周减一.
*gc.add(5,-1)表示天减一.
*以此类推应该可以精确的毫秒吧.没有再试.大家可以试试.
*GregorianCalendar类的add(int field,int amount)方法表示年月日加减.
*field参数表示年,月.日等.
*amount参数表示要加减的数量.
*
* DateUtil.java 测试如下:
*/

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;

public class DateUtil {

	
	public static String calFullDate(Date date, int yearNum, int monthNum, int dateNum,int hourNum) {
        String result = "";
        try {
            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.YEAR, yearNum);//年
            cal.add(Calendar.MONTH, monthNum);//月
            cal.add(Calendar.DATE, dateNum);//日
            cal.add(Calendar.HOUR, hourNum);//时
            cal.add(Calendar.MINUTE, hourNum);//分
            cal.add(Calendar.SECOND, hourNum);//秒
            result = sd.format(cal.getTime());
        } catch (Exception e) {
        }
        return result;
    }
	

	public static String getYears(Date d) {
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		gc.setTime(d);
		gc.add(1, +1);
		gc.set(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DATE));

		return sf.format(gc.getTime());
	}

	public String getHalfYear(Date d) {
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		gc.setTime(d);
		gc.add(2, +6);
		gc.set(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DATE));

		return sf.format(gc.getTime());
	}

	public String getQuarters(Date d) {
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		gc.setTime(d);
		gc.add(2, +3);
		gc.set(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DATE));
		return sf.format(gc.getTime());
	}

	public String getLocalDate(Date d) {
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		return sf.format(d);
	}

	public static void main(String[] args) {
	}

}