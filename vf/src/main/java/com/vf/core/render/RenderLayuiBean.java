package com.vf.core.render;


import java.io.Serializable;

/**
 * Render返回JSON数据封装
 * @author 
 */
public class RenderLayuiBean implements Serializable{
	
	private static final long serialVersionUID = -1126196958137979710L;

	
	private int code;
	
	private String msg = "";

	private Object data;
	
	private int count;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	
}
