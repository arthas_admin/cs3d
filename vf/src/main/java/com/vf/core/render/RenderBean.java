package com.vf.core.render;


import java.io.Serializable;

/**
 * Render返回JSON数据封装
 * @author 
 */
public class RenderBean implements Serializable{
	
	private static final long serialVersionUID = -1126196958137979710L;

	/**
	 * 状态码
	 */
	private int code;

	/**
	 * 描述
	 */
	private String message;
	
	/**
	 * 正常情况下返回的数据
	 */
	private Object data;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
	
}
