/**
 * @开发版权 云天化集团有限责任公司
 * @项目名称 supplychain
 * @版本信息 v1.0
 * @开发人员 zhous
 * @开发日期 2020-04-03
 * @修订日期
 * @描述  GeneratorModel 
 */
package com.vf.dev;

import javax.sql.DataSource;

import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

public class GeneratorModel {

	public static DataSource getDataSource() {
		DruidPlugin druidPlugin = new DruidPlugin("jdbc:mysql://localhost:3306/vf?characterEncoding=utf8&useUnicode=true&characterEncoding=utf-8&useSSL=false&zeroDateTimeBehavior=convertToNull&serverTimezone=UTC&nullNamePatternMatchesAll=true&autoReconnect=true", "root", "root");
		druidPlugin.start();
		return druidPlugin.getDataSource();
	}

	public static void main(String[] args) {
		genSysModel();

	}
	
	public static void genSysModel() {
		// base model 所使用的包名
		String baseModelPackageName = "com.vf.s.common.model.biz.base";
		// base model 文件保存路径
		String baseModelOutputDir = "D:\\develop\\vfs\\base";

		// model 所使用的包名 (MappingKit 默认使用的包名)
		String modelPackageName = "com.vf.s.common.model.biz";
		// model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
		String modelOutputDir = baseModelOutputDir + "/..";

		// 创建生成器
		Generator generator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName,
				modelOutputDir);
		// generator.setMetaBuilder(new _MetaBuilder(getDataSource()));
		// 设置是否生成链式 setter 方法
		generator.setGenerateChainSetter(false);
		// 添加不需要生成的表名
		generator.addExcludedTable("");
		;
		// 设置是否在 Model 中生成 dao 对象
		generator.setGenerateDaoInModel(true);
		// 设置是否生成链式 setter 方法
		generator.setGenerateChainSetter(true);
		// 设置是否生成字典文件
		generator.setGenerateDataDictionary(false);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非
		// OscUser
		generator.setRemovedTableNamePrefixes("biz_");
		// 生成
		generator.generate();
	}
	
}