package com.vf.i.mvc.controller;

import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.vf.core.controller.BaseController;
import com.vf.i.mvc.service.SceneService;
import com.vf.s.common.cache.style.MapLayerCache;
import com.vf.s.common.cache.style.MapStyleCache;
import com.vf.s.common.model.scene.Scene;

public class SceneController extends BaseController {

	protected final Log LOG = Log.getLog(getClass());
	
	@Inject
	private SceneService srv;
	
	public void get() {
		String sceneId = this.getPara("sceneId");
		if(StrKit.isBlank(sceneId)) {
			this.renderError("参数必填！");
			return;
		}
		Scene scene=srv.get(sceneId);
		if(scene!=null) {
			scene.setMapStyle(MapStyleCache.gets());
			scene.setMapLayer(MapLayerCache.gets());
			this.renderSuccess("成功！",scene);
		}else {
			this.renderError("场景不存在！");
		}
	}

}
