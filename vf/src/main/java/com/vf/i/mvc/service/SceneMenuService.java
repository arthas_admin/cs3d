package com.vf.i.mvc.service;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.s.common.model.scene.SceneLayer;
import com.vf.s.common.model.scene.BizSceneMenu;
import com.vf.s.common.plugins.shiro.ShiroKit;
import com.vf.s.common.plugins.shiro.SimpleUser;

public class SceneMenuService {

	public List<BizSceneMenu> gets(String sceneId) {
		SimpleUser simpleUser = ShiroKit.getLoginUser();
		if (simpleUser != null && !StrKit.equals(simpleUser.getUsername(), "system")) {
			return getChildrenAllTree(sceneId, sceneId, simpleUser.getId());
		}
		return getChildrenAllTree(sceneId, sceneId, null);
	}

	public List<BizSceneMenu> getChildrenAllTree(String pId, String sceneId, String userId) {
		List<BizSceneMenu> list = getChildrenByPid(pId, sceneId, userId);
		for (BizSceneMenu o : list) {
			o.setChildren(getChildrenAllTree(o.getId(), sceneId, userId));
		}
		return list;
	}

	/***
	 * 根据id 查询孩子
	 * 
	 * @param id
	 * @return
	 */
	public List<BizSceneMenu> getChildrenByPid(String id, String sceneId, String userid) {
		String sql = "SELECT * FROM " + BizSceneMenu.TABLE_NAME + " M WHERE M.PARENTID='" + id + "' ";
		if (userid != null && !StrKit.isBlank(userid)) {
			String authSql = " (SELECT B.MENUID FROM SYS_USER_ROLE A,SYS_ROLE_PERMISSIONS B WHERE A.USERID='" + userid
					+ "' AND  A.ROLEID=B.ROLEID and B.TARGETID='" + sceneId + "') ";
			sql = sql + " and m.id in  " + authSql + " ";
		}
		sql = sql + " ORDER BY M.SORT";
		return BizSceneMenu.dao.find(sql);
	}
	

	public List<SceneLayer> getLayers(String sceneId, String menuId) {
		List<SceneLayer> layers=SceneLayer.dao.find("select * from "+SceneLayer.TABLE_NAME+" where id in( SELECT layerId FROM "+BizSceneMenu.TABLE_NAME+" WHERE parentId=? AND sceneId=?  and type=? ) ",menuId,sceneId,"3");
		return layers;
	}

	public List<BizSceneMenu> getChildrens(String sceneId, String code) {
		return null;
	}

}
