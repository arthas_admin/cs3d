package com.vf.i.mvc.controller;

import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.vf.core.controller.BaseController;
import com.vf.i.mvc.service.SceneMenuService;
import com.vf.s.common.model.scene.SceneLayer;
import com.vf.s.common.model.scene.BizSceneMenu;

public class SceneMenuController extends BaseController {

	protected final Log LOG = Log.getLog(getClass());
	
	@Inject
	private SceneMenuService srv;
	
	/**
	 * 场景菜单
	 */
	public void gets() {
		String sceneId = this.getPara("sceneId");
		if(StrKit.isBlank(sceneId)) {
			this.renderError("参数必填！");
			return;
		}
		
		List<BizSceneMenu> list=srv.gets(sceneId);
		if(list!=null  && list.size()>0) {
			this.renderSuccess("成功！",list);
		}else {
			this.renderError("场景不存在！");
		}
	}
	

	public void getLayers() {
		String sceneId = this.getPara("sceneId");
		String menuId = this.getPara("menuId");
		if (StrKit.isBlank(sceneId) || StrKit.isBlank(menuId)) {
			this.renderError("参数必填！");
			return;
		}
		List<SceneLayer> layers = srv.getLayers(sceneId, menuId);
		if (layers != null && layers.size() > 0) {
			renderSuccess("成功！", layers);
		} else {
			this.renderError("未查询到数据！");
		}
	}
	
	//暂时不用
	public void getChildrens() {
		String sceneId = this.getPara("sceneId");
		String code = this.getPara("code");
		if (StrKit.isBlank(sceneId) || StrKit.isBlank(code)) {
			this.renderError("参数必填！");
			return;
		}
		List<BizSceneMenu> layers = srv.getChildrens(sceneId, code);
		if (layers != null && layers.size() > 0) {
			renderSuccess("成功！", layers);
		} else {
			this.renderError("未查询到数据！");
		}
	}

}
