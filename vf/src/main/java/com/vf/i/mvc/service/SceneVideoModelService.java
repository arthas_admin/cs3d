package com.vf.i.mvc.service;

import java.util.LinkedList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.s.common.model.biz.BizModel;
import com.vf.s.common.model.biz.BizVideo;
import com.vf.s.common.model.biz.BizVideoModel;

public class SceneVideoModelService {

	public List<BizVideoModel> gets(String layerId) {
		String whereForBizPoint = "FROM " + BizVideoModel.TABLE_NAME + " P   WHERE 1=1 ";
		if (!StrKit.isBlank(layerId)) {
			whereForBizPoint += " and P.LAYERID ='" + layerId + "' ";
		}
		List<BizVideoModel> bizLineList = BizVideoModel.dao.find("SELECT P.*  " + whereForBizPoint);
		List<BizVideoModel> list = new LinkedList<BizVideoModel>();
		for (BizVideoModel videoShed3d : bizLineList) {
			BizVideo bizVideo = BizVideo.dao.findById(videoShed3d.getVideoId());
			videoShed3d.setVideo(bizVideo);
			BizModel bizModel = BizModel.dao.findById(videoShed3d.getModelId());
			videoShed3d.setModel(bizModel);
			list.add(videoShed3d);
		}
		return list;
	}

	public BizVideoModel get(String entityId) {
		BizVideoModel bizVideoPlane=BizVideoModel.dao.findById(entityId);
		if(bizVideoPlane!=null) {
			BizVideo bizVideo=BizVideo.dao.findById(bizVideoPlane.getVideoId());
			bizVideoPlane.setVideo(bizVideo);
			BizModel bizModel=BizModel.dao.findById(bizVideoPlane.getModelId());
			bizVideoPlane.setModel(bizModel);
			return bizVideoPlane;
		}
		return null;
	}

}
