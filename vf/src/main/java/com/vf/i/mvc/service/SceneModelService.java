package com.vf.i.mvc.service;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.s.common.model.biz.BizModel;
import com.vf.s.common.model.scene.BizSceneModel;

public class SceneModelService {

	public List<BizSceneModel> gets(String layerId) {
		String whereForBizModelParam = "FROM " + BizSceneModel.TABLE_NAME + " A, " + BizModel.TABLE_NAME
				+ " B WHERE A.MODELID=B.ID  ";
		if (!StrKit.isBlank(layerId)) {
			whereForBizModelParam += " and  A.LAYERID='" + layerId + "' ";
		}
		whereForBizModelParam += " ORDER BY A.SORT ASC ";
		// 模型数据
		return BizSceneModel.dao.find("SELECT A.*,B.URL AS url  " + whereForBizModelParam);
	}

	public BizSceneModel get(String entityId) {
		BizSceneModel bizModelParam = BizSceneModel.dao.findById(entityId);
		if(bizModelParam==null) {
			return null;
		} 
		BizModel bizModel=BizModel.dao.findById(bizModelParam.getModelId());
		if(bizModel!=null) {
			bizModelParam.setUrl(bizModel.getUrl());
			return bizModelParam;
		}else {
			return null;
		}
	}

}
