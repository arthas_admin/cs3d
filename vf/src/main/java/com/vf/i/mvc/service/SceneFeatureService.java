package com.vf.i.mvc.service;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.s.common.model.scene.BizSceneFeature;

public class SceneFeatureService {
	
	public List<BizSceneFeature> gets(String layerId) {
		String whereForBizPoint = "FROM " + BizSceneFeature.TABLE_NAME + " P  WHERE 1=1 ";
		if (!StrKit.isBlank(layerId)) {
			whereForBizPoint += " and P.LAYERID ='" + layerId + "' ";
		}
		return BizSceneFeature.dao.find("SELECT P.*  " + whereForBizPoint);
	}

	public BizSceneFeature get(String entityId) {
		return BizSceneFeature.dao.findById(entityId);
	}
}
