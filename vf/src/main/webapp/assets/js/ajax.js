/**
 * Ajax请求方法二次封装
 */
var common_ajax = function() {
	
	/**
	 * ajax请求并返回结果
	 * @param url
	 * @param data
	 * @param callback
	 * @returns {String}
	 */
	var ajaxFunc = function(method,url, data, dataType, callback){
		if(dataType == undefined || dataType == null){
			dataType = "html";
		}
		var result;
		$.ajax({
			type : method|| "get",
			url : encodeURI(encodeURI(url)),
			data : data,
			dataType : dataType,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			async: false,
			cache: false,
			success:function(response){
				result = response;
				//扩展回调函数
				if( callback != null ){
					callback();
				}
			}
		});
		return result;
	};
	var ajaxAsyncFunc = function(url, data, dataType, callback){
		if(dataType == undefined || dataType == null){
			dataType = "html";
		}
		var result;
		$.ajax({
			type : "post",
			url : encodeURI(encodeURI(url)),
			data : data,
			dataType : dataType,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			async: true,
			cache: false,
			success:function(response){
				result = response;
				//扩展回调函数
				if( callback != null ){
					callback(result);
				}
			}
		});
		return result;
	};
	
	var ajaxPromise = function(url, data, dataType, callback){
		  if(dataType == undefined || dataType == null){
		   dataType = "html";
		  }
		  const p = new Promise((resolve, reject) => {
		   $.ajax({
		    type : "post",
		    url : encodeURI(encodeURI(url)),
		    data : data,
		    dataType : dataType,
		    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		    async: true,
		    cache: false,
		    success:function(response){
		     resolve(response);
		    },
		    error: function () {
		     reject(new Error('返回错误'))
		    }
		   });
		  });
		  return p;
	};
	
	return {
		ajaxFunc : ajaxFunc,
		ajaxAsyncFunc : ajaxAsyncFunc,
		ajaxPromise: ajaxPromise
	};
	
}();
