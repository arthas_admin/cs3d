var $;
var layer;
var element;
var config ;
var index ;
var admin ;
var laytpl ;
var treeSelect;
layui.config({
    version: true,   // 更新组件缓存，设为true不缓存，也可以设一个固定值
    base: 'assets/module/'
}).extend({
    formSelects: 'formSelects/formSelects-v4',
    treetable: 'treetable-lay/treetable',
    dropdown: 'dropdown/dropdown',
    notice: 'notice/notice',
    step: 'step-lay/step',
    dtree: 'dtree/dtree',
    citypicker: 'city-picker/city-picker',
    tableSelect: 'tableSelect/tableSelect',
    Cropper: 'Cropper/Cropper',
    zTree: 'zTree/zTree',
    introJs: 'introJs/introJs',
    fileChoose: 'fileChoose/fileChoose',
    tagsInput: 'tagsInput/tagsInput',
    CKEDITOR: 'ckeditor/ckeditor',
    Split: 'Split/Split',
    treeSelect: '/treeSelect/treeSelect',
    cascader: 'cascader/cascader',
    tinymce: '../../assets/plugins/tinymce/tinymce'
}).use(['config', 'layer', 'element', 'index', 'admin', 'form','treeSelect' , 'laytpl'], function () {
    $ = layui.jquery;
    layer = layui.layer;
    element = layui.element;
    config = layui.config;
    index = layui.index;
    form = layui.form;
    admin = layui.admin;
    laytpl = layui.laytpl;
    treeSelect=layui.treeSelect;
    // 加载侧边栏
    admin.req('home/menus', {}, function (res) {
    	if(res.code==200){
            laytpl(sideNav.innerHTML).render(res.data, function (html) {
                $('.layui-layout-admin .layui-side .layui-nav').html(html);
                element.render('nav');
            });
            index.regRouter(res.data);  // 注册路由
            index.loadHome({  // 加载主页
                URL: res.data[0].URL,
                NAME: '<i class="layui-icon layui-icon-home">&emsp;<cite>'+res.data[0].NAME+'</cite></i>'
            });   		
    	}

    }, 'get');
    // 移除loading动画
    setTimeout(function () {
        admin.removeLoading();
    }, 300);
});
