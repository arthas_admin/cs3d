/**
 * 基础图层组
 */
Preview.prototype.getBaseLayerGroup=function(configs){
	var _this=this;
	if(configs){
		for(var config of configs){
			if('BASE_LAYER_GROUP'==config.type){
				return config;
			}
		}
	}
}

/**
 * 图层组
 */
Preview.prototype.getLayerGroup=function(menu){
	var _this=this;
	if(menu && menu.children){
		for(var item of menu.children){
			if(item.type=='LAYER_GROUP'){
				return item;
			}
		}
	}
}

/**
 * 菜单组
 */
Preview.prototype.getMenuGroup=function(configs){
	var _this=this;
	if(configs){
		for(var config of configs){
			if('MENU_GROUP'==config.type){
				return config;
			}
		}
	}
}


/**
 * 按钮组
 */
Preview.prototype.getBtnGroup=function(menu){
	var _this=this;
	if(menu && menu.children){
		for(var item of menu.children){
			if(item.type=='BTN_GROUP'){
				return item;
			}
		}
	}
}

/**
 * 组件组
 */
Preview.prototype.getAssemblyGroup=function(menu){
	var _this=this;
	if(menu && menu.children){
		for(var item of menu.children){
			if(item.type=='ASSEMBLY_GROUP'){
				return item;
			}
		}
	}
}



