//获取场景
Preview.prototype.getScene=function(sceneId){
	var _this=this;
   var res= _this._Ajax(_this.ctx+"/i/scene/get",{
    	sceneId:sceneId,
	}, "json",null);
   if (undefined!=res && 200==res.code) {
   		return res.data;
   }
}

/**
 * 场景配置树
 */
Preview.prototype.getConfig=function(sceneId){
	var _this=this;
   var res= _this._Ajax(_this.ctx+"/i/scene/menu/gets",{
    	sceneId:sceneId,
	}, "json",null);
   if (undefined!=res && 200==res.code) {
   		return res.data;
   }
}

/**
 * 获取菜单图层资源
 */
Preview.prototype.getLayers=function(sceneId,menuId){
	var _this=this;
   var res= _this._Ajax(_this.ctx+"/i/scene/menu/getLayers",{
    	sceneId:sceneId,
    	menuId:menuId,
	}, "json",null);
   if (undefined!=res && 200==res.code) {
   		return res.data;
   }
}

/**
 * 获取图层资源明细
 */
Preview.prototype.getEntities=function(layerId){
	var _this=this;
   var res= _this._Ajax(_this.ctx+"/i/scene/layer/getEntities",{
	   layerId:layerId,
	}, "json",null);
   if (undefined!=res && 200==res.code) {
   		return res.data;
   }
}

/**
 * 获取对象
 */
Preview.prototype.getEntity=function(entityId,type){
	var _this=this;
   var res= _this._Ajax(_this.ctx+"/i/scene/layer/getEntity",{
	   entityId:entityId,
	   type:type,
	}, "json",null);
   if (undefined!=res && 200==res.code) {
   		return res.data;
   }
}

/**
 * 获取对象分组
 */
Preview.prototype.getGroup=function(sceneId,group){
	var _this=this;
   var res= _this._Ajax(_this.ctx+"/i/scene/layer/getEntity",{
	   sceneId,sceneId,
	   group:group,
	}, "json",null);
   if (undefined!=res && 200==res.code) {
   		return res.data;
   }
}

