
/**
 * 页面
 */
VFGEditor.prototype.pageForEventTb=function(objId,event,callBack){
	 var _this=this;
	 var bizPropertyTb = table.render({
        elem: '#tableBizPage',
        url: _this.ctx+'/biz/page/listData',
        page: true,
        where:{
        	objId:objId,
        	event:event,
        },
        height:300,
        cols: [[
            {type: 'numbers'},
            {field: 'name', sort: false, title: '名称',width: 200},
            {field: 'showType', sort: false, title:'显示方式',width: 100, templet: '#bizPageForShowTypeScript',align: 'center'},
            {align: 'left', toolbar: '#tableBarBizPage', title: '操作',width: 120}
        ]]
    });
    //工具条点击事件
    table.on('tool(tableBizPage)', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'del') { 
            layer.confirm('确定要删除“【<span style="color:red;">' + data.name + '</span>】”吗？', {
                skin: 'layui-layer-admin',
                shade: .1
            }, function (index) {
                layer.close(index);
                layer.load(2);
                var res = common_ajax.ajaxFunc(_this.ctx+"/biz/page/delete",{
                    id: data.id
                }, "json", null);
                layer.closeAll('loading');
                if (undefined!=res && 200==res.code) {
                    layer.msg(res.message, {icon: 1});
                    bizPropertyTb.reload({}, 'data');
                } else {
                    layer.msg(res.message, {icon: 2});
                }
            });
        }
        else if(layEvent === 'edit'){
    		_this.pageForEdit(data.id,objId,event,function(){
    			bizPropertyTb.reload({}, 'data');
    		});
        }
    });
	$('#btnAddBizPage').click(function () {
		_this.pageForEdit(null,objId,event,function(){
			bizPropertyTb.reload({}, 'data');
		});
		return false;
	});
}

/**
 * 页面编辑
 */
VFGEditor.prototype.pageForEdit=function(id,objId,event,callBack){
	var _this=this;
	_this._Ajax('get',_this.ctx+"/biz/page/edit",{id:id,sceneId:_this.sceneId,objId:objId,event:event},'html',function(e){
	    layer.open({
	        title:'页面信息',
	        type: 1,
	        area: '450px',
	        content:e,
	        shade:0.1,
	        offset: 't',
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	        	form.on('submit(submitBizPageForm)', function (data) {
	        	    layer.load(2);
	                var formData={}; 
	                for(var key in data.field){
	                	formData['bizPage.'+key]=data.field[key];
	                }
	                var res = common_ajax.ajaxFunc(_this.ctx+"/biz/page/save",formData, "json", null);
	                layer.closeAll('loading');
	                if (200==res.code) {
	                	layer.close(dIndex);
	                    layer.msg(res.message, {icon: 1});
	                    if(callBack){
	                    	callBack();
	                    }
	                } else {
	                    layer.msg(res.message, {icon: 2});
	                }
	        	    return false;
	        	});
	            form.render();
			},
    	});  
	});
}
