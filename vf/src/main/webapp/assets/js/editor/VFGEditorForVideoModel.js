/************************************模型视频融合*********************************************/
VFGEditor.prototype.videoModelForEdit=function(){
	 var _this=this;
	 if(_this.sceneLayerMap.has('modelVideo')){
		layer.msg('页面已经打开！！！', {icon: 3});
		return;
	 }	 
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/biz/model/modelForObjEdit",{sceneId:_this.sceneId},'html',function(e){
		layer.closeAll();
	    layer.open({
	    	id:'modelVideo',
	        title:'视频模型资源',
	        type: 1,
	        area: '320px',
	        content:e,
	        shade:0,
            offset: ['50px', '0px'],
            fixed:true,
            move:false,
		    skin:'layui-layer layui-layer-adminRight',
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	            _this.sceneLayerMap.set('modelVideo',dIndex);
			},
			end:function(){
				_this.sceneLayerMap.delete('modelVideo');
			}
    	});  
	});
}


VFGEditor.prototype.drawForVideoModel = function (data,callBack) {
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	var primitive=null;
	_this.isDraw=true;
	var position;
	_this.drawModel=new Cesium.DrawModel(_this.viewer,{
		leftClick:function(e){
			var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
			if(position){
				var id=VFG.Util.getUuid();
				primitive=VFG.Util.createGltfPrimitive(_this.viewer,{
					id:id,
					uri:data.url,
					name:data.name,
					posX:position.x,
					posY:position.y,
					posZ:position.z,
					type:data.type
				});
				_this.saveForModel(id,data,position);
				if(callBack){
					callBack(primitive);
				}
			}
		},
		end:function(){
			_this.isDraw=false;
			_this.drawModel=null;
			if(callBack)callBack(null);
		}
	});
};
VFGEditor.prototype.destroyForVideoModel= function () {
	var _this=this;
	if(_this.drawModel){
		_this.drawModel.destroy();
		_this.drawModel=null;
	}
	_this.isDraw=false;
};

