/************************************弹出窗体*********************************************/
VFGEditor.prototype.tipsForLayer = function (objId,event,position) {
	var _this=this;
	_Ajax('get',_this.ctx+"/biz/tips/tipsForLayer",{sceneId:this.sceneId,event:event,objId:objId},'html',function(e){
		var VFGWin; 
		layer.open({
			id:objId, 
		    type: 1,
		    title: false,
		    shade: 0,
		    content:e,
		    resize :false,
		    success: function (layero, index) {
		        layui.$(layero).children('.layui-layer-content').css('overflow', 'visible'); 
		        VFGWin=new Cesium.VFGWin(_this.viewer,{
		        	position:position,
		        	layero:layero,
		        	index:index,
		        });
		    },
		    end:function(){
		    	if(VFGWin){
		    		VFGWin.destroy();
		    		VFGWin=null;
		    	}
		    }
		});  
	});
};

VFGEditor.prototype.tipsForMessage = function (objId,position) {
	var _this=this;
	_Ajax('get',_this.ctx+"/biz/tips/message",{sceneId:this.sceneId,objId:objId},'html',function(e){
		var VFGWin; 
		layer.open({
			id:objId, 
		    type: 1,
		    title: false,
		    shade: 0,
		    content:e,
		    resize :false,
		    success: function (layero, index) {
		        layui.$(layero).children('.layui-layer-content').css('overflow', 'visible'); 
		        VFGWin=new Cesium.VFGWin(_this.viewer,{
		        	position:position,
		        	layero:layero,
		        	index:index,
		        });
		    },
		    end:function(){
		    	if(VFGWin){
		    		VFGWin.destroy();
		    		VFGWin=null;
		    	}
		    }
		});  
	});
};
