/**
 * 场景编辑
 */
VFGEditor.prototype.scenePrimitiveForEdit=function(){
	 var _this=this;
	 if(_this.sceneLayerMap.has('sceneEdit')){
		layer.msg('页面已经打开！！！', {icon: 3});
		return;
	 }	 
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/biz/scene/getEditPage",{sceneId:_this.sceneId},'html',function(e){
		layer.closeAll();
 		var layerIndex=layer.open({
 			id:'sceneEdit',
             type: 1,
             title:'场景信息',
             content:e,
             area: '320px',
             shade:0,
             offset: ['50px', '0px'],	
             fixed:true,
             move:false,
 		    skin:'layui-layer layui-layer-adminRight',
             success: function (layero, dIndex) {
                 $(layero).children('.layui-layer-content').css('overflow', 'hidden');
                 _this.sceneLayerMap.set('sceneEdit',layerIndex);
                 
 			    if( $('#modelBizScene-visualAngleImg-input').val()){
			    	 $('#modelBizScene-visualAngle').attr("src", $('#modelBizScene-visualAngleImg-input').val());
			    }
			    
			    if( $('#modelBizScene-cover-input').val()){
			    	 $('#modelBizScene-cover').attr("src", $('#modelBizScene-cover-input').val());
			   }
			    
			    $('#modelBizScene-cover-btn').click(function () {
			    	var src=VFG.Util.getSceneImg(viewer,500,312);
			        $('#modelBizScene-cover').attr("src", src);
			        $('#modelBizScene-cover-input').val(src);
			    });
			    $('#modelBizScene-visualAngle-btn').click(function () {
			        var visualPos=VFG.Util.getVisualAngle(viewer);
			        if(visualPos){
			  	    	var src=VFG.Util.getSceneImg(viewer,500,312);
				        $('#modelBizScene-visualAngle').attr("src", src);
				        $('#modelBizScene-visualAngleImg-input').val(src);
				        $('#modelBizScene-heading-input').val(visualPos.heading);
				        $('#modelBizScene-pitch-input').val(visualPos.pitch);
				        $('#modelBizScene-roll-input').val(visualPos.roll);
				        $('#modelBizScene-cameraX-input').val(visualPos.position.x);
				        $('#modelBizScene-cameraY-input').val(visualPos.position.y);
				        $('#modelBizScene-cameraZ-input').val(visualPos.position.z);
				        $('#modelBizScene-cameraZ-input').val(visualPos.position.z);
			        }
			    });
			    form.on('submit(modelBizSceneSubmit)', function (data) {
			        layer.load(2);
			        var formData={}; 
			        for(var key in data.field){
			        	formData['bizScene.'+key]=data.field[key];
			        }
			        _this.ajaxFunc(_this.ctx+"/biz/scene/save",formData, "json", function(res){
				        layer.closeAll('loading');
				        if (200==res.code) {
				            layer.msg(res.message, {icon: 1});
				        } else {
				            layer.msg(res.message, {icon: 2});
				        }
			        	
			        });
			        return false;
			    });
			    form.render(); 
 			},
 			end:function(){
 				_this.sceneLayerMap.delete('sceneEdit');
 			}
 		});	
	});
}



VFGEditor.prototype.scenePrimitiveForRemove=function(param){
	VFG.Util.removeScenePrimitive(this.viewer,param);
}

/**
 * 显示或隐藏
 */
VFGEditor.prototype.scenePrimitiveForShow=function(param){
	VFG.Util.showScenePrimitive(this.viewer,param)
}

