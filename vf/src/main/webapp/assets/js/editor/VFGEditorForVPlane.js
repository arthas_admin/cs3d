VFGEditor.prototype.showOrHideVideoPlane=function(layerId,state,type){
	var _this=this;
	if('VideoPlanes'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/video/plane/list",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(state==true){
					 _this.globe.addVPlanes(list);
				 }else{
					 _this.globe.removeVPlanes(list);
				 }
			 }
			 else{
			 }
	    });
	}
	else if ('VideoPlane'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/video/plane/getByLayerId",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var model=res.data; 
				 if(model){
					 if(state==true){
						 _this.globe.addVPlane(model);
						 _this.globe.flyToVPlaneById(model.id);
					 }else{
						 _this.globe.removeVPlane(model);
					 }
				 }
			 }
			 else{
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	} 
} 


VFGEditor.prototype.operationVideoPlane = function (screenPos,planeId,type) {
	var _this=this;
	 var plane=_this.globe.getPrimitiveById(planeId,type);
	if(plane){
		layer.load(2);
		var VFGWin;
		_this._Ajax('get',_this.ctx+"/biz/video/plane/operation",{sceneId:_this.sceneId,planeId:planeId},'html',function(e){
			layer.closeAll('loading');   
			var layerIndex= layer.open({
					id:planeId,
				    type: 1,
				    title: false,
				    shade: 0,
				    content:e,
				    resize :false,
				    success: function (layero, dIndex) {
			            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
			            var cartesian3=VFG.Util.getScreenToC3(_this.viewer, screenPos, null);
			        	VFGWin=new Cesium.VFGWin(_this.viewer,{
			            	position:cartesian3,
			            	layero:layero,
			            	index:dIndex,
			            });
			        	$('#toolForBizOperationDrag').click(function () {
			        		layer.close(dIndex);
			        		_this.dragVideoPlane(planeId,type);
			        	});	
			        	$('#toolForBizOperationEdit').click(function () {
			        		layer.close(dIndex);
			        		_this.editVideoPlane({id:planeId,name:plane.option.name});
			        	});	
			        	$('#toolForBizOperationDel').click(function () {
			        		layer.close(dIndex);
			        		_this.deleteLayer({
			        			id:planeId,
			        			type:type,
			        			name:plane.option.name,
			        		})
			        	});	
					},
					end:function(){
						if(VFGWin){
							 VFGWin.destroy();
							 VFGWin=null;
						}
					}
		    	});  
		});
	}else{
		layer.alert('信息未知！！', {icon: 1,skin: 'layer-ext-moon' })
	}
}

VFGEditor.prototype.editVideoPlane = function (treeNode) {
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	this.isDraw=true;
	layer.closeAll();
	layer.load(2);
	_this._Ajax('get',_this.ctx+"/biz/video/plane/edit",{sceneId:_this.sceneId,planeId:treeNode.id},'html',function(e){
		layer.closeAll('loading');   
		var layerIndex= layer.open({
		    	id:'editVideoPlane',
		        title: name || '编辑【'+treeNode.name+'】',
		        type: 1,
		        area: '400px',
		        content:e,
		        shade:0,
		        offset: 'r',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		            
				    $('#bizVideoPlaneGetBtnVideo').click(function () {
				    	_this.selectVideoForVideoPlane(treeNode,function(e){
				    		$("#bizVideoPlaneName-input").val(e.name);
				    		$("#bizVideoPlaneCode").val(e.code);
				    		$("#bizVideoPlanevideoId").val(e.id);
				    	});
				    });
		            slider.render({
		              elem: '#bizVideoPlaneRotationX'
		              ,input: true //输入框
		              ,min: 0 //最小值
		              ,max: 360 //最大值
		              ,value:$("#bizVideoPlaneRotationX-input").val()*1
		              ,change: function(value){
		            	  $("#bizVideoPlaneRotationX-input").val(value);
		            	  var videoPlaneId=$("#bizVideoPlaneId").val();
		            	  _this.globe.changeVPlaneRX(videoPlaneId,value*1);
	            	  }
		            });
		            slider.render({
		              elem: '#bizVideoPlaneRotationY'
		              ,input: true //输入框
		              ,min: 0 //最小值
		              ,max: 360 //最大值
		              ,value:$("#bizVideoPlaneRotationY-input").val()*1
		              ,change: function(value){
		            	  $("#bizVideoPlaneRotationY-input").val(value);
		            	  var videoPlaneId=$("#bizVideoPlaneId").val();
		            	  _this.globe.changeVPlaneRY(videoPlaneId,value*1);
	            	  }
		            });
		            
		            slider.render({
		              elem: '#bizVideoPlaneRotationZ'
		              ,input: true //输入框
		              ,min: 0 //最小值
		              ,max: 360 //最大值
		              ,value:$("#bizVideoPlaneRotationZ-input").val()*1
		              ,change: function(value){
		            	  $("#bizVideoPlaneRotationZ-input").val(value);
		            	  var videoPlaneId=$("#bizVideoPlaneId").val();
		            	  _this.globe.changeVPlaneRZ(videoPlaneId,value*1);
	            	  }
		            });
		            
                	$('#bizVideoPlaneDimensionX').on('input', function(e) {
                		var videoPlaneId=$("#bizVideoPlaneId").val();
                		_this.globe.changeVPlaneDX(videoPlaneId,$("#bizVideoPlaneDimensionX").val()*1);
                	});
                	$('#bizVideoPlaneDimensionZ').on('input', function(e) {
                		var videoPlaneId=$("#bizVideoPlaneId").val();
                		_this.globe.changeVPlaneDZ(videoPlaneId,$("#bizVideoPlaneDimensionZ").val()*1);
                	});
                	$('#bizVideoPlaneDimensionY').on('input', function(e) {
                		var videoPlaneId=$("#bizVideoPlaneId").val();
                		_this.globe.changeVPlaneDY(videoPlaneId,$("#bizVideoPlaneDimensionY").val()*1);
                	});
                	$('#bizVideoPlaneZ').on('input', function(e) {
                		var videoPlaneId=$("#bizVideoPlaneId").val();
                		_this.globe.changeVPlaneHeight(videoPlaneId,$("#bizVideoPlaneZ").val()*1);
                	});
		            
				    var isPick=false;
				    $('#bizVideoPlaneGetBtnPosition').click(function () {
				    	if(!isPick){
							this.pickPosition=new Cesium.DrawPoint(_this.viewer,{
								pick:function(e){
									var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
						    		$("#bizVideoPlaneX").val(position.x);
						    		$("#bizVideoPlaneY").val(position.y);
						    		$("#bizVideoPlaneZ").val(position.z);
					            	  var bizVideoPlaneId=$("#bizVideoPlaneId").val();
					            	  _this.globe.changeVPlanePosition(bizVideoPlaneId,{
					            		  x:position.x,
					            		  y:position.y,
					            		  z:position.z,
					            	  });
								},
								end:function(){
									isPick=false;
								}
							});
				    	}
				    });
		            
		            
		        	form.on('submit(submitbizVideoPlaneForm)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizVideoPlane.'+key]=data.field[key];
		                }
		                formData['bizVideoPlane.layerId']=data.field['layerId']||treeNode.id;
		                formData['bizVideoPlane.sceneId']=_this.sceneId;
		                _this.ajaxFunc(_this.ctx+"/biz/video/plane/save",formData, "json", function(res){
			                layer.closeAll('loading');
			                if (200==res.code) {
			                    layer.msg(res.message, {icon: 1});
			                    var data=res.data;
			                    $("#bizVideoPlaneId").val(data.id)
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }
		                });
		        	    return false;
		        	});
				    $('#settingbizVideoPlaneVisualAngle').click(function () {
				        var visualPos=VFG.Util.getVisualAngle(_this.viewer);
				        if(visualPos){
				            var formData={}; 
				            formData['bizVideoPlane.id']=treeNode.id;
				            formData['bizVideoPlane.heading']=visualPos.heading;
				            formData['bizVideoPlane.pitch']=visualPos.pitch;
				            formData['bizVideoPlane.roll']=visualPos.roll;
				            formData['bizVideoPlane.cameraX']=visualPos.position.x;
				            formData['bizVideoPlane.cameraY']=visualPos.position.y;
				            formData['bizVideoPlane.cameraZ']=visualPos.position.z;
				            _this.ajaxFunc(_this.ctx+"/biz/video/plane/save",formData, "json", function(res){
					            if (undefined!=res && 200==res.code) {
					            	 layer.msg(res.message);
					            }
				            });
				        }
				    });
		        	form.render(); 
				},
				end:function(){
					_this.layerForEdit();
					_this.isDraw=false;
				}
	    	});  
	});
};

VFGEditor.prototype.selectVideoForVideoPlane = function (treeNode,callback) {
	var _this=this;
	layer.load(2);
	_this._Ajax('get',_this.ctx+"/biz/video/selectList",{sceneId:_this.sceneId,targetId:treeNode.id},'html',function(e){
		layer.closeAll('loading');   
		var layerIndex= layer.open({
		    	id:'selectVideoForVideoPlane',
		        title: '编辑【'+treeNode.name+'】视频源',
		        type: 1,
		        area: '400px',
		        content:e,
		        shade:0,
		        offset: 'r',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		            var insTb = table.render({
		                elem: '#tableSelectBizVideo',
		                url: _this.ctx+'/biz/video/listData', 
		                page: true,
		                toolbar: false,
		                cellMinWidth: 100,
		                where:{
		                	sceneId:_this.sceneId	
		                },
		                cols: [[
		                    {field: 'name', sort: false, title: '名称'},
		                ]],
		            });
		            //监听行单击事件（双击事件为：rowDouble）
		            table.on('row(tableSelectBizVideo)', function(obj){
		            	var data = obj.data;
		              	obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
		              	if(callback){
		            	  callback(data);
		              	}
		            });
				},
				end:function(){
				}
	    	});  
	});
};

/**
 * 点拖拽
 */
VFGEditor.prototype.dragVideoPlane = function (planeId,type) {
	var _this=this;
	 var plane=_this.globe.getPrimitiveById(planeId,type);
	 if(plane){
	    _this.isDrag=true;
	    var position;
		var cesiumPick =new Cesium.DrawPoint(_this.viewer,{
			pick:function(e){
				position=VFG.Util.getC3ToLnLa(_this.viewer,e);
            	_this.globe.changeVPlanePosition(planeId,{
        		  x:position.x,
        		  y:position.y,
        		  z:position.z,
            	});
			},
			end:function(e){
				_this.isDrag=false;
				if(position){
				    var formData={}; 
				    formData['bizVideoPlane.id']=planeId;
				    formData['bizVideoPlane.x']=position.x;
				    formData['bizVideoPlane.y']=position.y;
				    formData['bizVideoPlane.z']=position.z;
				    _this.ajaxFunc(_this.ctx+"/biz/video/plane/save",formData, "json", function(res){
				        if (undefined!=res && 200==res.code) {
				        	layer.msg('坐标更新成功！');
				        }else{
				        	layer.msg('坐标更新失败！');
				        }
				    });
				}
				cesiumPick=null;
			}
		});
	 }
}




