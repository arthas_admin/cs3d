var CityBuider=function(options){
	this.ctx=options.ctx;
	this.cityId=options.cityId;
	this.container=options.container;
	this.init();
	return this;
}
CityBuider.prototype.init = function (){
	var _this=this;
	layer.load(2);
    _this.ajaxFunc(_this.ctx+"/city/menu/getList",{cityId:this.cityId}, "json", function(res){
        layer.closeAll('loading');
        if (200==res.code) {
            laytpl(sideCityNav.innerHTML).render(res.data, function (html) {
                $('#cityMenu-'+_this.cityId).html(html);
                element.render('nav');
                this.globe= new VFG.Viewer(_this.container,{});
            });
        }
    });
}

CityBuider.prototype.flyToLocation = function (){
}

CityBuider.prototype.editCity = function (){
	var _this=this;
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/city/setting",{cityId:_this.cityId},'html',function(e){
		layer.closeAll();
		layer.open({
			id:'setting',
            type: 1,
            anim:5,
            title:'图层管理',
            area: '320px',
            content:e,
            shade:0,
            offset: ['0px', '224px'],	
            fixed:true,
            move:false,
		    skin:'layui-layer  layui-layer-adminRight layui-layer-city',
            success: function (layero, dIndex) {
               $(layero).children('.layui-layer-content').css('overflow', 'hidden');
               
               form.on('submit(submitBaseBizCityForm)', function (data) {
	        	    layer.load(2);
	                var formData={}; 
	                for(var key in data.field){
	                	formData['bizCity.'+key]=data.field[key];
	                }
	                _this.ajaxFunc(_this.ctx+"/city/save",formData, "json", function(res){
		                layer.closeAll('loading');
		                if (200==res.code) {
		                    layer.msg(res.message, {icon: 1});
		                    $("#cityBuiderNameTd").html(res.data.name);
		                } else {
		                    layer.msg(res.message, {icon: 2});
		                }
	                });
	        	    return false;
	        	});
			},
			end:function(){
			}
		});	
	});		
}





CityBuider.prototype.selectResource = function(type) {
	if(type=='Layers'){
		this.selectLayer();
	}
	else if(type=='Scenes'){
		this.selectScene();
	}
}

CityBuider.prototype._Ajax = function(reqType, url, data, dataType, callback) {
	$.ajax({
		type: reqType,
		url: encodeURI(encodeURI(url)),
		data: data,
		dataType: dataType ? dataType : 'html',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		async: true,
		cache: false,
		success: function(response) {
			if (callback != null) {
				callback(response);
			}
		}
	});
}
CityBuider.prototype.ajaxFunc = function(url, data, dataType, callback){
	if(dataType == undefined || dataType == null){
		dataType = "html";
	}
	var result;
	$.ajax({
		type : "post",
		url : encodeURI(encodeURI(url)),
		data : data,
		dataType : dataType,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		async: true,
		cache: false,
		success:function(response){
			result = response;
			//扩展回调函数
			if( callback != null ){
				callback(result);
			}
		}
	});
	return result;
};
