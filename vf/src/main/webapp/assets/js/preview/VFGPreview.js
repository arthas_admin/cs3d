function VFGPreview(options){
	this.sceneId=options.sceneId;
	this.ctx=options.ctx;
	this.init();
}

VFGPreview.prototype.init=function(){
	let _this=this;
	let res = common_ajax.ajaxFunc(_this.ctx+'/i/scene/menu/gets',{
		id:this.sceneId
	}, "json", null);
	
	if(res==null || res.code!=200){
		console.log(res!=null?res.message:'网络错误！');
		return ;
	}
	
}

/**
 * 点击图层节点
 */
VFGPreview.prototype.clickForLayerTree=function(node){
	var _this=this;
}

/**
 * 勾选图层
 */
VFGPreview.prototype.checkForLayerTree=function(node){
	var _this=this;
	if('map'==node.type || 'terrain'==node.type){
		if(node.checked){
			_this.getForBizMapById(node.id)
		}else{
			VFG.Util.removeScenePrimitive(_this.viewer,{
     			id:node.id,
     			type:node.type
     		})
		}
	}else{
		VFG.Util.showScenePrimitive(_this.viewer,{
			id:node.id,
			type:node.type,
			show:node.checked
		})
	}
}



