VFG.Point=function(){}

VFG.Point.LabelCollection=new Cesium.LabelCollection();
VFG.Point.PointPrimitiveCollection=new Cesium.PointPrimitiveCollection();
VFG.Point.BillboardCollection=new Cesium.BillboardCollection();

VFG.Point.Primitives=new Map();
VFG.Point.LabelMap=new Map();
VFG.Point.PointMap=new Map();
VFG.Point.BillboardMap=new Map();

VFG.Point.add=function(viewer,item,ctx){
	if(!this.contain(item.id)){
		if(item.render && item.render=="1"){
			this.addEntity(viewer,item,ctx);
		}else{
			this.addOrUpdatePrimitive(viewer,item,ctx);
		}
		this.Primitives.set(item.point.id,{
			id:item.point.id,
			render:item.point.render||"0"
		})
	}
}

VFG.Point.addOrUpdatePrimitive=function(viewer,item,ctx){
	var param=this.packagePrimitive(viewer,item,ctx);
	var point=item.point;
	if(param.label){
		if(this.LabelMap.has(point.id)){
			var label=this.LabelMap.get(point.id);
			label.text=param.label.text|| '点';
			label.position=param.label.position,
			label.font=param.label.font;
			label.horizontalOrigin =param.label.horizontalOrigin;
			label.verticalOrigin =param.label.verticalOrigin;
			label.scale=param.label.scale;
			label.fillColor =param.label.fillColor;
			label.outlineWidth =param.label.outlineWidth;
			label.outlineColor=param.label.outlineColor;
			label.pixelOffset=param.label.pixelOffset;
			label.showBackground=param.label.showBackground;
			label.backgroundColor=param.label.backgroundColor
			label.backgroundPadding =param.label.backgroundPadding
			label.pixelOffsetScaleByDistance =param.label.pixelOffsetScaleByDistance
			label.scaleByDistance=param.label.scaleByDistance
			label.translucencyByDistance=param.label.translucencyByDistance       		
			label.distanceDisplayCondition =param.label.distanceDisplayCondition
			label.style =label.style
		}else{
	        this.LabelMap.set(point.id,this.LabelCollection.add(param.label));
		}
	}else{
		if(this.LabelMap.has(point.id)){
			this.LabelCollection.remove(this.LabelMap.get(point.id));
			this.LabelMap.delete(point.id);
		}
	}
	if(param.billboard){
		if(this.BillboardMap.has(point.id)){
			var billboard=this.BillboardMap.get(point.id);
			billboard.position=param.billboard.position;
			billboard.image=param.billboard.image;
			billboard.width=param.billboard.width
			billboard.height=param.billboard.height
			billboard.scale=param.billboard.scale
			billboard.sizeInMeters=param.billboard.sizeInMeters
			billboard.pixelOffset=param.billboard.pixelOffset
			billboard.eyeOffset=param.billboard.eyeOffset
			billboard.horizontalOrigin =param.billboard.horizontalOrigin
			billboard.verticalOrigin =param.billboard.verticalOrigin 
			billboard.pixelOffsetScaleByDistance =param.billboard.pixelOffsetScaleByDistance
			billboard.distanceDisplayCondition =param.billboard.distanceDisplayCondition
			billboard.scaleByDistance=param.billboard.scaleByDistance
			billboard.translucencyByDistance=param.billboard.translucencyByDistance
		}else{
	        this.BillboardMap.set(point.id,this.BillboardCollection.add(param.billboard));
		}
	}else{
		if(this.BillboardMap.has(point.id)){
			this.BillboardCollection.remove(this.BillboardMap.get(point.id));
			this.BillboardMap.delete(point.id)
		}
	}
	if(param.point){
		if(this.PointMap.has(point.id)){
			var point=this.PointMap.get(point.id);
			point.position=param.point.position
			point.pixelSize =param.point.pixelSize
			//heightReference=param.point.heightReference?point.heightReference: Cesium.HeightReference.NONE,
			point.color =param.point.color
			point.outlineColor=param.point.outlineColor
			point.outlineWidth=param.point.outlineWidth
			point.distanceDisplayCondition =param.point.distanceDisplayCondition
			point.scaleByDistance=param.point.scaleByDistance
			point.translucencyByDistance=param.point.translucencyByDistance
	        //disableDepthTestDistance: Number.POSITIVE_INFINITY
		}else{
	        this.PointMap.set(point.id,this.PointPrimitiveCollection.add(param.point));
		}
	}else{
		if(this.PointMap.has(point.id)){
			this.PointPrimitiveCollection.remove(this.PointMap.get(point.id));
			this.PointMap.delete(point.id);
		}
	}
}

VFG.Point.packagePrimitive=function(viewer,item,ctx){
	 var point=item.point;
	 var style=item.style;
	 var position=Cesium.Cartesian3.fromDegrees(point.x*1,point.y*1,point.z*1);
	 var param={};
	 if(style){
		 if(style.showForLabel=='1'){
			var eyeOffset=VFG.Util.getCartesian3(style.eyeOffsetXForLabel,style.eyeOffsetYForLabel,style.eyeOffsetZForLabel);
			var pixelOffset=VFG.Util.getCartesian2(style.pixelOffsetXForLabel*1,style.pixelOffsetYForLabel);
			var backgroundPadding=VFG.Util.getCartesian2(style.backgroundPaddingXForLabel,style.backgroundPaddingYForLabel);
			var pixelOffsetScaleByDistance= VFG.Util.getNearFarScalar(style.pixelOffsetScaleNearForLabel,style.pixelOffsetScaleNearValueForLabel, style.pixelOffsetScaleFarForLabel,style.pixelOffsetScaleFarValueForLabel);
		    var scaleByDistance=VFG.Util.getNearFarScalar(style.scalarNearForLabel,style.scalarNearValueForLabel, style.scalarFarForLabel,style.scalarFarValueForLabel); 
		    var translucencyByDistance =VFG.Util.getNearFarScalar(style.translucencyNearForLabel,style.translucencyNearValueForLabel, style.translucencyFarForLabel,style.translucencyFarValueForLabel); 
		    var distanceDisplayCondition =VFG.Util.getDistanceDisplayCondition(style.displayNearForLabel,style.displayFarForLabel);
		    param.label={
		    	id: point.id,
		    	text:point.name|| '点',
		    	position:position,
		        font: this.getFont(style),
		        //eyeOffset :eyeOffset?eyeOffset:Cesium.Cartesian3.ZERO ,
		        horizontalOrigin : VFG.Util.getHorizontalOrigin(style.horizontalOriginForLabel) ,
		        verticalOrigin : VFG.Util.getVerticalOrigin(style.verticalOriginForLabel) ,
		        scale: style.fontScaleForLabel?style.fontScaleForLabel:1,
		        fillColor :style.fillColorForLabel?Cesium.Color.fromCssColorString(style.fillColorForLabel):Cesium.Color.WHITE,
		        outlineWidth : style.outlineWidthForLabel?style.outlineWidthForLabel:0,
		        outlineColor:  style.outlineColorForLabel?Cesium.Color.fromCssColorString(style.outlineColorForLabel):Cesium.Color.WHITE,
		        pixelOffset: pixelOffset?pixelOffset:new Cesium.Cartesian2(0, 0),
		        showBackground: style.showBackgroundForLabel&& style.showBackgroundForLabel==1 ? true:false,
		        backgroundColor: style.backgroundColorForLabel?Cesium.Color.fromCssColorString(style.backgroundColorForLabel):new Cesium.Color(0.165, 0.165, 0.165, 0.8),
		        backgroundPadding : backgroundPadding?backgroundPadding:new Cesium.Cartesian2(0, 0),
		        pixelOffsetScaleByDistance :pixelOffsetScaleByDistance?pixelOffsetScaleByDistance:undefined,//偏移量比例
		        scaleByDistance:scaleByDistance?scaleByDistance:undefined, //缩放比例
		        translucencyByDistance:translucencyByDistance?translucencyByDistance:undefined,//半透明度比例            		
		        distanceDisplayCondition :distanceDisplayCondition?distanceDisplayCondition:undefined,
		        style :Cesium.LabelStyle.FILL_AND_OUTLINE,
		        disableDepthTestDistance: Number.POSITIVE_INFINITY
		    };
		 }
		 if(style.showForBillboard=='1'){
			var pixelOffset=VFG.Util.getCartesian2(style.pixelOffsetXForBillboard*1,style.pixelOffsetXForBillboard*1);
			var pixelOffsetScaleByDistance= VFG.Util.getNearFarScalar(style.pixelOffsetScaleNearForBillboard,style.pixelOffsetScaleNearValueForBillboard, style.pixelOffsetScaleFarForBillboard,style.pixelOffsetScaleFarValueForBillboard);
	        var distanceDisplayCondition =VFG.Util.getDistanceDisplayCondition(style.displayNearForBillboard,style.displayFarForBillboard);
	        var scaleByDistance= VFG.Util.getNearFarScalar(style.scalarNearForBillboard,style.scalarNearValueForBillboard, style.scalarFarForBillboard,style.scalarFarValueForBillboard); 
	        var translucencyByDistance =VFG.Util.getNearFarScalar(style.translucencyNearForBillboard,style.translucencyNearValueForBillboard, style.translucencyFarForBillboard,style.translucencyFarValueForBillboard); 
	        param.billboard={
	        	id: point.id,
	        	position: position,
	            image:ctx+style.imageForBillboard,
	            width: style.widthForBillboard,
	            height: style.heightForBillboard,
	            scale:style.scaleForBillboard,
	            sizeInMeters: false,
		        pixelOffset: pixelOffset?pixelOffset:new Cesium.Cartesian2(0, style.heightForBillboard),
	            eyeOffset:new Cesium.Cartesian3(0.0, 0, 0.0),
	            horizontalOrigin : Cesium.HorizontalOrigin.CENTER,
	            verticalOrigin :Cesium.VerticalOrigin.BOTTOM,
		        pixelOffsetScaleByDistance :pixelOffsetScaleByDistance?pixelOffsetScaleByDistance:undefined,//偏移量比例
	            distanceDisplayCondition :distanceDisplayCondition?distanceDisplayCondition:undefined,
	            scaleByDistance:scaleByDistance?scaleByDistance:undefined, //设置距离方位内
	            translucencyByDistance:translucencyByDistance?translucencyByDistance:undefined,
	            disableDepthTestDistance: Number.POSITIVE_INFINITY		
	    	}
		 }
		 if(style.showForPoint=='1'){
		    var distanceDisplayCondition =VFG.Util.getDistanceDisplayCondition(style.displayNearForPoint,style.displayFarForPoint);
		    var scaleByDistance= VFG.Util.getNearFarScalar(style.scalarNearForPoint,style.scalarNearValueForPoint, style.scalarFarForPoint,style.scalarFarValueForPoint); 
		    var translucencyByDistance= VFG.Util.getNearFarScalar(style.translucencyNearForPoint,style.translucencyNearValueForPoint, style.translucencyFarForPoint,style.translucencyFarValueForPoint); 
		    param.point={
		    	id: point.id,
		    	position: position,
				pixelSize : style.pixelSizeForPoint||5,
				//heightReference:point.heightReference?point.heightReference: Cesium.HeightReference.NONE,
				color :style.colorForPoint?Cesium.Color.fromCssColorString(style.colorForPoint):Cesium.Color.WHITE,
				outlineColor: style.outlineColorForPoint? Cesium.Color.fromCssColorString(style.outlineColorForPoint):Cesium.Color.BLACK,
			    outlineWidth:style.outlineWidthForPoint?style.outlineWidthForPoint:0,
		        distanceDisplayCondition : distanceDisplayCondition?distanceDisplayCondition:undefined,
		        scaleByDistance:scaleByDistance?scaleByDistance:undefined, 
		        translucencyByDistance:translucencyByDistance?translucencyByDistance:undefined, 	
		        disableDepthTestDistance: Number.POSITIVE_INFINITY
			}
		 }
		 return param;
	 }else{
	    param.point={
			  id: point.id,
	          pixelSize: 5,
	          color: Cesium.Color.RED,
	          color :new Cesium.Color ( 255 , 255 , 0 , 1 ),
	          outlineColor : Cesium.Color.YELLOW,
	          outlineWidth :2,
	          position: position
		}
	    return param;
	 }
}


VFG.Point.addEntity=function(viewer,item,ctx){
	return new Cesium.Entity(this.packageEntity(viewer,item,ctx));
}

VFG.Point.packageEntity=function(viewer,item,ctx){
	 var point=item.point;
	 var ops=item.style;
	 var position=Cesium.Cartesian3.fromDegrees(point.x*1,point.y*1,point.z*1);
	 if(ops){
			var param={
				  id: point.id,
		          position: position,
		          code: point.code,
			};
			if(ops.showForLabel=='1'){
				var eyeOffset=VFG.Util.getCartesian3(ops.eyeOffsetXForLabel,ops.eyeOffsetYForLabel,ops.eyeOffsetZForLabel);
				var pixelOffset=VFG.Util.getCartesian2(ops.pixelOffsetXForLabel*1,ops.pixelOffsetYForLabel);
				var backgroundPadding=VFG.Util.getCartesian2(ops.backgroundPaddingXForLabel,ops.backgroundPaddingYForLabel);
				var pixelOffsetScaleByDistance= VFG.Util.getNearFarScalar(ops.pixelOffsetScaleNearForLabel,ops.pixelOffsetScaleNearValueForLabel, ops.pixelOffsetScaleFarForLabel,ops.pixelOffsetScaleFarValueForLabel);
			    var scaleByDistance=VFG.Util.getNearFarScalar(ops.scalarNearForLabel,ops.scalarNearValueForLabel, ops.scalarFarForLabel,ops.scalarFarValueForLabel); 
			    var translucencyByDistance =VFG.Util.getNearFarScalar(ops.translucencyNearForLabel,ops.translucencyNearValueForLabel, ops.translucencyFarForLabel,ops.translucencyFarValueForLabel); 
			    var distanceDisplayCondition =VFG.Util.getDistanceDisplayCondition(ops.displayNearForLabel,ops.displayFarForLabel);
			    param.label={
			        font: this.getFont(ops),
	        		text: point.name||'未定义',
			        horizontalOrigin : VFG.Util.getHorizontalOrigin(ops.horizontalOriginForLabel) ,
			        verticalOrigin : VFG.Util.getVerticalOrigin(ops.verticalOriginForLabel) ,
			        scale: ops.fontScaleForLabel?ops.fontScaleForLabel:1,
			        fillColor :ops.fillColorForLabel?Cesium.Color.fromCssColorString(ops.fillColorForLabel):Cesium.Color.WHITE,
			        outlineWidth : ops.outlineWidthForLabel?ops.outlineWidthForLabel:0,
			        outlineColor:  ops.outlineColorForLabel?Cesium.Color.fromCssColorString(ops.outlineColorForLabel):Cesium.Color.WHITE,
			        pixelOffset: pixelOffset?pixelOffset:new Cesium.Cartesian2(0, 0),
			        showBackground: ops.showBackgroundForLabel&& ops.showBackgroundForLabel==1 ? true:false,
			        backgroundColor: ops.backgroundColorForLabel?Cesium.Color.fromCssColorString(ops.backgroundColorForLabel):new Cesium.Color(0.165, 0.165, 0.165, 0.8),
			        backgroundPadding : backgroundPadding?backgroundPadding:new Cesium.Cartesian2(0, 0),
			        pixelOffsetScaleByDistance :pixelOffsetScaleByDistance?pixelOffsetScaleByDistance:undefined,//偏移量比例
			        scaleByDistance:scaleByDistance?scaleByDistance:undefined, //缩放比例
			        translucencyByDistance:translucencyByDistance?translucencyByDistance:undefined,//半透明度比例            		
			        distanceDisplayCondition :distanceDisplayCondition?distanceDisplayCondition:undefined,
			        style :Cesium.LabelStyle.FILL_AND_OUTLINE,
			        disableDepthTestDistance: Number.POSITIVE_INFINITY 
			    }
			}
			if(ops.showForPoint=='1'){
			    var distanceDisplayCondition =VFG.Util.getDistanceDisplayCondition(ops.displayNearForPoint,ops.displayFarForPoint);
			    var scaleByDistance= VFG.Util.getNearFarScalar(ops.scalarNearForPoint,ops.scalarNearValueForPoint, ops.scalarFarForPoint,ops.scalarFarValueForPoint); 
			    var translucencyByDistance= VFG.Util.getNearFarScalar(ops.translucencyNearForPoint,ops.translucencyNearValueForPoint, ops.translucencyFarForPoint,ops.translucencyFarValueForPoint); 
			    param.point={
					pixelSize : ops.pixelSizeForPoint||5,
					color :ops.colorForPoint?Cesium.Color.fromCssColorString(ops.colorForPoint):Cesium.Color.WHITE,
					outlineColor: ops.outlineColorForPoint? Cesium.Color.fromCssColorString(ops.outlineColorForPoint):Cesium.Color.BLACK,
				    outlineWidth:ops.outlineWidthForPoint?ops.outlineWidthForPoint:0,
			        distanceDisplayCondition : distanceDisplayCondition?distanceDisplayCondition:undefined,
			        scaleByDistance:scaleByDistance?scaleByDistance:undefined, 
			        translucencyByDistance:translucencyByDistance?translucencyByDistance:undefined, 
	        		disableDepthTestDistance: Number.POSITIVE_INFINITY		
				}
			}
			if(ops.showForBillboard=='1'){
				var pixelOffset=VFG.Util.getCartesian2(ops.pixelOffsetXForBillboard*1,ops.pixelOffsetXForBillboard*1);
				var pixelOffsetScaleByDistance= VFG.Util.getNearFarScalar(ops.pixelOffsetScaleNearForBillboard,ops.pixelOffsetScaleNearValueForBillboard, ops.pixelOffsetScaleFarForBillboard,ops.pixelOffsetScaleFarValueForBillboard);
		        var distanceDisplayCondition =VFG.Util.getDistanceDisplayCondition(ops.displayNearForBillboard,ops.displayFarForBillboard);
		        var scaleByDistance= VFG.Util.getNearFarScalar(ops.scalarNearForBillboard,ops.scalarNearValueForBillboard, ops.scalarFarForBillboard,ops.scalarFarValueForBillboard); 
		        var translucencyByDistance =VFG.Util.getNearFarScalar(ops.translucencyNearForBillboard,ops.translucencyNearValueForBillboard, ops.translucencyFarForBillboard,ops.translucencyFarValueForBillboard); 
		        param.billboard={
		            image:ctx+ops.imageForBillboard,
		            width: ops.widthForBillboard,
		            height: ops.heightForBillboard,
		            scale:ops.scaleForBillboard,
		            sizeInMeters: false,
			        pixelOffset: pixelOffset?pixelOffset:new Cesium.Cartesian2(0, ops.heightForBillboard),
		            eyeOffset:new Cesium.Cartesian3(0.0, 0, 0.0),
		            horizontalOrigin : Cesium.HorizontalOrigin.CENTER,
		            verticalOrigin :Cesium.VerticalOrigin.BOTTOM,
			        pixelOffsetScaleByDistance :pixelOffsetScaleByDistance?pixelOffsetScaleByDistance:undefined,//偏移量比例
		            distanceDisplayCondition :distanceDisplayCondition?distanceDisplayCondition:undefined,
		            scaleByDistance:scaleByDistance?scaleByDistance:undefined, //设置距离方位内
		            translucencyByDistance:translucencyByDistance?translucencyByDistance:undefined,
            		disableDepthTestDistance: Number.POSITIVE_INFINITY
		    	}
			}
			return param;
	 }else{
		 return {
			  id: point.id,
	          position: position,
	          code: point.code,
	          point:{
				pixelSize : 5,
				color :Cesium.Color.WHITE,
			 }
		 }
	 }
}


VFG.Point.getFont=function(data){
	var font="";
	if(data.fontSizeForLabel){
		font+=data.fontSizeForLabel+"px "
	}
	if(data.fontWeightForLabel){
		font+=data.fontWeightForLabel+""
	}
	if(data.fontFamilyForLabel){
		font+=data.fontFamilyForLabel+""
	}
	return font
}

VFG.Point.show=function(viewer,param){
	if(this.contain(param.id)){
		var obj=this.Primitives.get(param.id);
		if(obj.render=="1"){
			viewer.entities.getById(id).show=param.show
		}else{
			if(VFG.Point.LabelMap.has(param.id)){
				VFG.Point.LabelMap.get(param.id).show=param.show;
			}
			if(VFG.Point.PointMap.has(param.id)){
				VFG.Point.PointMap.get(param.id).show=param.show;
			}
			if(VFG.Point.BillboardMap.has(param.id)){
				VFG.Point.BillboardMap.get(param.id).show=param.show;
			}
		}
	}
}

VFG.Point.getById=function(viewer,id){
	if(this.contain(id)){
		var obj=this.Primitives.get(id);
		if(obj.render=="1"){
			return viewer.entities.getById(id);
		}else{
			return {
				Label:this.LabelMap.get(id),
				Point:this.PointMap.get(id),
				Billboard:this.BillboardMap.get(id),
			}
		}
	}
}

VFG.Point.removeById=function(viewer,id){
	if(this.contain(id)){
		var obj=this.Primitives.get(id);
		if(obj.render=="1"){
			viewer.entities.removeById(id);
		}else{
			if(VFG.Point.LabelMap.has(id)){
				this.LabelCollection.remove(VFG.Point.LabelMap.get(id));
				this.LabelMap.delete(id);
			}
			if(VFG.Point.PointMap.has(id)){
				this.PointPrimitiveCollection.remove(VFG.Point.PointMap.get(id));
				this.PointMap.delete(id);
				
			}
			if(VFG.Point.BillboardMap.has(id)){
				this.BillboardCollection.remove(VFG.Point.BillboardMap.get(id));
				this.BillboardMap.delete(id)
			}
		}
	}
	this.Primitives.delete(id)
}

VFG.Point.contain=function(id){
	return this.Primitives.has(id)
}

VFG.Point.update=function(viewer,item,ctx){
	var point=item.point;
	if(this.contain(point.id)){
		if(point.render && point.render=="1"){
			var param=this.packageEntity(viewer,item,ctx);
			var entity=viewer.entities.getById(point.id);
			if(entity){
				entity.position=param.position;
				entity.code=param.code||null;
				entity.point=param.point||null;
				entity.label=param.label||null;
				entity.billboard=param.billboard||null;
				entity.position=param.position;
			}
		}else{
			this.addOrUpdatePrimitive(viewer,item,ctx);
		}
	}
}


