//    		new VFG.OrbitControls(viewer,{
//    			id:VFG.Util.getUuid(),
//    			position:Cesium.Cartesian3.fromDegrees(103.38398032314626,23.368851347473854,1350)
//    		});
VFG.OrbitControls=function(viewer,options){
	this.viewer=viewer;
	this.options=options;
	this.id=options.id;
	this.position=this.options.position;
	this.modelMatrix=Cesium.Transforms.eastNorthUpToFixedFrame(this.options.position);
	this.pickRay = new Cesium.Ray();
	this._init();
}
VFG.OrbitControls.prototype._init = function() {
	var _this=this;
	
    // 向上的向量
    const vectorNormalUp = new Cesium.Cartesian3()
    const vZ = new Cesium.Cartesian3(0, 0, 1)
    Cesium.Cartesian3.normalize(this.position.clone(), vectorNormalUp)

    // 向右的向量
    const vectorNormalRight = new Cesium.Cartesian3()
    // 由z轴向上 地表向上两个向量叉乘, 则可以得出, 向右的向量
    Cesium.Cartesian3.cross(vZ, vectorNormalUp, vectorNormalRight)

    // 向前的向量
    const vectorNormalFront = new Cesium.Cartesian3()
    Cesium.Cartesian3.cross(vectorNormalRight, vectorNormalUp, vectorNormalFront)
    Cesium.Cartesian3.multiplyByScalar(vectorNormalFront, -1, vectorNormalFront)
    
    
    this.axisX = {
        id: 'axisX',
        color: Cesium.Color.GREEN,
        direction: vectorNormalRight,
        unit: Cesium.Cartesian3.UNIT_X,
    }
    this.axisZ ={
        id: 'axisZ',
        color: Cesium.Color.RED,
        direction: vectorNormalUp,
        unit: Cesium.Cartesian3.UNIT_Z,
    }
    this.axisY ={
        id: 'axisY',
        color: Cesium.Color.BLUE,
        direction: vectorNormalFront,
        unit: Cesium.Cartesian3.UNIT_Y,
    }
	
    this._primitiveX=new Cesium.Primitive({
        geometryInstances : new  Cesium.GeometryInstance({
            geometry : new  Cesium.PolylineGeometry({
                positions : [
                	 Cesium.Cartesian3.ZERO,
                	 Cesium.Cartesian3.UNIT_X
                ],
                width :16,
                vertexFormat : Cesium.PolylineMaterialAppearance.VERTEX_FORMAT,
                colors : [
                	 Cesium.Color.RED,
                	 Cesium.Color.RED
                ],
                arcType:  Cesium.ArcType.NONE
            }),
            id:_this.id+"-X", 
            modelMatrix :_this.getModelMatrix(),
            pickPrimitive : this
        }),
        appearance : new Cesium.PolylineMaterialAppearance({
            material : new Cesium.Material({
                fabric : {
                    type : 'PolylineArrow',
                    uniforms : {
                        color :  Cesium.Color.RED
                    }
                }
            })
        })
    })
    
    
    this._primitiveY=new Cesium.Primitive({
        geometryInstances : new  Cesium.GeometryInstance({
            geometry : new  Cesium.PolylineGeometry({
                positions : [
                	Cesium.Cartesian3.ZERO,
                	Cesium.Cartesian3.UNIT_Y
                ],
                width :16,
                vertexFormat : Cesium.PolylineMaterialAppearance.VERTEX_FORMAT,
                colors : [
                	 Cesium.Color.GREEN,
                	 Cesium.Color.GREEN
                ],
                arcType:  Cesium.ArcType.NONE
            }),
            id:_this.id+"-Y",
            modelMatrix :_this.getModelMatrix(),
            pickPrimitive : this
        }),
        appearance : new Cesium.PolylineMaterialAppearance({
            material : new Cesium.Material({
                fabric : {
                    type : 'PolylineArrow',
                    uniforms : {
                        color :  Cesium.Color.GREEN
                    }
                }
            })
        })
    })
    
    this._primitiveZ=new Cesium.Primitive({
        geometryInstances : new  Cesium.GeometryInstance({
            geometry : new  Cesium.PolylineGeometry({
                positions : [
                	 Cesium.Cartesian3.ZERO,
                	 Cesium. Cartesian3.UNIT_Z
                ],
                width :16,
                vertexFormat : Cesium.PolylineMaterialAppearance.VERTEX_FORMAT,
                colors : [
                	 Cesium.Color.BLUE,
                	 Cesium.Color.BLUE
                ],
                arcType:  Cesium.ArcType.NONE
            }),
            id:_this.id+"-Z",
            modelMatrix :_this.getModelMatrix(),
            pickPrimitive : this
        }),
        appearance : new Cesium.PolylineMaterialAppearance({
            material : new Cesium.Material({
                fabric : {
                    type : 'PolylineArrow',
                    uniforms : {
                        color :  Cesium.Color.BLUE
                    }
                }
            })
        })
    })
    
    
    
/*   
    */

	
	this._primitiveXY=new Cesium.Primitive({
        geometryInstances : new Cesium.GeometryInstance({
            geometry : new Cesium.PlaneGeometry({
                vertexFormat : Cesium.PerInstanceColorAppearance.VERTEX_FORMAT,
            }),
            modelMatrix : this.getModelMatrixXY(),
            id:_this.id+"-XY",
            pickPrimitive : this,
            attributes : {
                color : Cesium.ColorGeometryInstanceAttribute.fromColor(new Cesium.Color(1.0, 0.0, 0.0, 1.0))
            }
        }),
        appearance : new Cesium.PerInstanceColorAppearance({
            closed: false,
            translucent: false
        })
    })
    
    
    
	this._primitiveXZ=new Cesium.Primitive({
        geometryInstances : new Cesium.GeometryInstance({
            geometry : new Cesium.PlaneGeometry({
                vertexFormat : Cesium.PerInstanceColorAppearance.VERTEX_FORMAT,
            }),
            modelMatrix : this.getModelMatrixXZ(),
            id:_this.id+"-XZ",
            pickPrimitive : this,
            attributes : {
                color : Cesium.ColorGeometryInstanceAttribute.fromColor(new Cesium.Color(1.0, 0.0, 0.0, 1.0))
            }
        }),
        appearance : new Cesium.PerInstanceColorAppearance({
            closed: false,
            translucent: false
        })
    })
	
	this._primitiveYZ=new Cesium.Primitive({
        geometryInstances : new Cesium.GeometryInstance({
            geometry : new Cesium.PlaneGeometry({
                vertexFormat : Cesium.PerInstanceColorAppearance.VERTEX_FORMAT,
            }),
            modelMatrix : this.getModelMatrixYZ(),
            id:_this.id+"-YZ",
            pickPrimitive : this,
            attributes : {
                color : Cesium.ColorGeometryInstanceAttribute.fromColor(new Cesium.Color(1.0, 0.0, 0.0, 1.0))
            }
        }),
        appearance : new Cesium.PerInstanceColorAppearance({
            closed: false,
            translucent: false
        })
    })

    this.viewer.scene.primitives.add( this._primitiveX );
    this.viewer.scene.primitives.add( this._primitiveY );
    this.viewer.scene.primitives.add( this._primitiveZ );
    this.viewer.scene.primitives.add(this._primitiveXY);
    this.viewer.scene.primitives.add(this._primitiveXZ);
    this.viewer.scene.primitives.add(this._primitiveYZ);
    
	this._handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);
	var selectedAxis;
	
	//鼠标左键按下
	this._handler.setInputAction(function(e) {
		var pick = _this.viewer.scene.pick(e.position);
		if (Cesium.defined(pick) && pick.id && (  pick.id==_this.id+"-Z" ||   pick.id==_this.id+"-X" ||  pick.id==_this.id+"-Y" ||  pick.id==_this.id+"-XY" ||  pick.id==_this.id+"-XZ" ||  pick.id==_this.id+"-YZ" )){
			_this.pickPoint = _this.viewer.scene.pickPosition(e.position);
			selectedAxis=pick;
			if(pick.id==_this.id+"-X"){
		 		_this._primitiveX.appearance.material.uniforms.color=Cesium.Color.YELLOW;
		 		_this.viewer.scene.screenSpaceCameraController.enableRotate = false;//锁定相机
		 	}
		 	else if(pick.id==_this.id+"-Y"){
		 		_this._primitiveY.appearance.material.uniforms.color=Cesium.Color.YELLOW
		 		_this.viewer.scene.screenSpaceCameraController.enableRotate = false;//锁定相机
		 	}
		 	else if(pick.id==_this.id+"-Z"){
		 		_this._primitiveZ.appearance.material.uniforms.color=Cesium.Color.YELLOW
		 		_this.viewer.scene.screenSpaceCameraController.enableRotate = false;//锁定相机
		 	}
		 	else if(pick.id==_this.id+"-XY"){
			    var attributes = _this._primitiveXY.getGeometryInstanceAttributes(pick.id);
			    if(attributes){
			    	attributes.color =Cesium.ColorGeometryInstanceAttribute.toValue(Cesium.Color.YELLOW)
			    }
		 		_this.viewer.scene.screenSpaceCameraController.enableRotate = false;//锁定相机
		 	}
		 	else if(selectedAxis.id==_this.id+"-XZ"){
			    var attributes = _this._primitiveXZ.getGeometryInstanceAttributes(selectedAxis.id);
			    if(attributes){
			    	attributes.color =Cesium.ColorGeometryInstanceAttribute.toValue(Cesium.Color.YELLOW)
			    }
			    _this.viewer.scene.screenSpaceCameraController.enableRotate = false;//锁定相机
		 	}
		 	else if(selectedAxis.id==_this.id+"-YZ"){
			    var attributes = _this._primitiveYZ.getGeometryInstanceAttributes(selectedAxis.id);
			    if(attributes){
			    	attributes.color =Cesium.ColorGeometryInstanceAttribute.toValue(Cesium.Color.YELLOW)
			    }
			    _this.viewer.scene.screenSpaceCameraController.enableRotate = false;//锁定相机
		 	}
		}
	},Cesium.ScreenSpaceEventType.LEFT_DOWN);
    
	//鼠标左键释放
	this._handler.setInputAction(function(e) {
		if(selectedAxis){
		 	if(selectedAxis.id==_this.id+"-X"){
		 		_this._primitiveX.appearance.material.uniforms.color=Cesium.Color.RED;
		 	}
		 	else if(selectedAxis.id==_this.id+"-Y"){
		 		_this._primitiveY.appearance.material.uniforms.color=Cesium.Color.GREEN
		 	}
		 	else if(selectedAxis.id==_this.id+"-Z"){
		 		_this._primitiveZ.appearance.material.uniforms.color=Cesium.Color.BLUE
		 	}
		 	else if(selectedAxis.id==_this.id+"-XY"){
			    var attributes = _this._primitiveXY.getGeometryInstanceAttributes(selectedAxis.id);
			    if(attributes){
			    	attributes.color =Cesium.ColorGeometryInstanceAttribute.toValue(Cesium.Color.RED)
			    }
		 	}
		 	else if(selectedAxis.id==_this.id+"-XZ"){
			    var attributes = _this._primitiveXZ.getGeometryInstanceAttributes(selectedAxis.id);
			    if(attributes){
			    	attributes.color =Cesium.ColorGeometryInstanceAttribute.toValue(Cesium.Color.RED)
			    }
		 	}
		 	else if(selectedAxis.id==_this.id+"-YZ"){
			    var attributes = _this._primitiveYZ.getGeometryInstanceAttributes(selectedAxis.id);
			    if(attributes){
			    	attributes.color =Cesium.ColorGeometryInstanceAttribute.toValue(Cesium.Color.RED)
			    }
		 	}
		 	selectedAxis=null;
	        this.pickPoint = null
		}
		_this.viewer.scene.screenSpaceCameraController.enableRotate = true;//解锁相机
	},Cesium.ScreenSpaceEventType.LEFT_UP);
	
	this._handler.setInputAction(function(movement) {
		if(selectedAxis){
		 	if(selectedAxis.id==_this.id+"-X"){
		 		_this._moveX(movement);
		 	}
		 	else if(selectedAxis.id==_this.id+"-Y"){
		 		_this._moveY(movement);
		 	}
		 	else if(selectedAxis.id==_this.id+"-Z"){
		 		_this._moveZ(movement);
		 	}
		 	else if(selectedAxis.id==_this.id+"-XY"){
		 		_this._precessTranslation2(movement,'XY');
		 	}
		 	else if(selectedAxis.id==_this.id+"-XZ"){
		 		_this._precessTranslation2(movement,'XZ');
		 	}
		 	else if(selectedAxis.id==_this.id+"-YZ"){
		 		_this._precessTranslation2(movement,'YZ');
		 	}
		}
	}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
	
	this._handler.setInputAction(function(movement) {
		if(_this.options.end){
			_this.options.end(_this.position);
		}
		_this.destroy();
	}, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
	
};


VFG.OrbitControls.prototype.getModelMatrix=function () {
	return Cesium.Matrix4.multiplyByUniformScale(Cesium.Transforms.eastNorthUpToFixedFrame(this.position),100, new Cesium.Matrix4())
}

VFG.OrbitControls.prototype.getModelMatrixXY=function(){
    var dimensions = new Cesium.Cartesian3(1.0, 1.0, 1.0);
    var scaleMatrix = Cesium.Matrix4.fromScale(dimensions);
    var xPlaneMatrix=Cesium.Matrix4.multiply(Cesium.Transforms.eastNorthUpToFixedFrame(this.position), scaleMatrix, new Cesium.Matrix4());
	Cesium.Matrix4.multiplyByTranslation(
		xPlaneMatrix,
        Cesium.Cartesian3.multiplyByScalar(Cesium.Cartesian3.UNIT_X, 0.5, new Cesium.Cartesian3()),
        xPlaneMatrix
    );
	Cesium.Matrix4.multiplyByTranslation(
		xPlaneMatrix,
        Cesium.Cartesian3.multiplyByScalar(Cesium.Cartesian3.UNIT_Y, 0.5, new Cesium.Cartesian3()),
        xPlaneMatrix
    );
	return xPlaneMatrix;
}

VFG.OrbitControls.prototype.getModelMatrixXZ=function(){
    var xPlaneMatrix=this.getModelMatrixXY();
    
    var mx = Cesium.Matrix3.fromRotationX(Cesium.Math.toRadians(90));
    var rotationX = Cesium.Matrix4.fromRotationTranslation(mx);
    Cesium.Matrix4.multiply(
    		xPlaneMatrix,
            rotationX,
            xPlaneMatrix
    );
    
    Cesium.Matrix4.multiplyByTranslation(
    		xPlaneMatrix,
            Cesium.Cartesian3.multiplyByScalar(Cesium.Cartesian3.UNIT_Z, 0.5, new Cesium.Cartesian3()),
            xPlaneMatrix
        );
    Cesium.Matrix4.multiplyByTranslation(
    		xPlaneMatrix,
            Cesium.Cartesian3.multiplyByScalar(Cesium.Cartesian3.UNIT_Y, 0.5, new Cesium.Cartesian3()),
            xPlaneMatrix
        );
	return xPlaneMatrix;
}

VFG.OrbitControls.prototype.getModelMatrixYZ=function(){
    var xPlaneMatrix=this.getModelMatrixXY();
    
    var mx = Cesium.Matrix3.fromRotationY(Cesium.Math.toRadians(90));
    var rotationX = Cesium.Matrix4.fromRotationTranslation(mx);
    Cesium.Matrix4.multiply(
    		xPlaneMatrix,
            rotationX,
            xPlaneMatrix
    );
    Cesium.Matrix4.multiplyByTranslation(
    		xPlaneMatrix,
            Cesium.Cartesian3.multiplyByScalar(Cesium.Cartesian3.UNIT_X, -0.5, new Cesium.Cartesian3()),
            xPlaneMatrix
        );
    Cesium.Matrix4.multiplyByTranslation(
    		xPlaneMatrix,
            Cesium.Cartesian3.multiplyByScalar(Cesium.Cartesian3.UNIT_Z, -0.5, new Cesium.Cartesian3()),
            xPlaneMatrix
        );
/*    
    Cesium.Matrix4.multiplyByTranslation(
    		xPlaneMatrix,
            Cesium.Cartesian3.multiplyByScalar(Cesium.Cartesian3.UNIT_Y, 0.5, new Cesium.Cartesian3()),
            xPlaneMatrix
        );*/
	return xPlaneMatrix;
}



/**
 * 处理选中
 * @param e{startPosition: Cesium.Cartesian2, endPosition: Cesium.Cartesian2}}
 * @param axis{ArrowPolyline}
 * @private
 */
VFG.OrbitControls.prototype._precessTranslation=function (e, axis) {

    if (!this.pickPoint) return
    this.viewer.camera.getPickRay(e.startPosition, this.pickRay)
    const startPosition = this.getPlanePosition(this.pickPoint, this.viewer.camera.position.clone(), this.pickRay, axis.direction)
    this.viewer.camera.getPickRay(e.endPosition, this.pickRay)
    const endPosition = this.getPlanePosition(this.pickPoint, this.viewer.camera.position.clone(), this.pickRay, axis.direction)
    const moveVector = new Cesium.Cartesian3()
    Cesium.Cartesian3.subtract(endPosition, startPosition, moveVector)
    const moveLength = -Cesium.Cartesian3.dot(axis.direction, moveVector)
    this.translation(moveVector, axis.unit, moveLength)

}

VFG.OrbitControls.prototype._precessTranslation2=function (e, axis) {
	if (!this.pickPoint) return
	if('XY'==axis){
	   this._moveX(e);
	   this._moveY(e);
	 }
	else if('XZ'==axis){
	   this._moveX(e);
	   this._moveZ(e);
	}else{
	   this._moveY(e);
	   this._moveZ(e);
	}
}

VFG.OrbitControls.prototype._moveX=function (e) {
	if (!this.pickPoint) return
    this.viewer.camera.getPickRay(e.startPosition, this.pickRay)
    const startPosition = this.getPlanePosition(this.pickPoint, this.viewer.camera.position.clone(), this.pickRay, this.axisX.direction)
    this.viewer.camera.getPickRay(e.endPosition, this.pickRay)
    const endPosition = this.getPlanePosition(this.pickPoint, this.viewer.camera.position.clone(), this.pickRay, this.axisX.direction)
    const moveVector = new Cesium.Cartesian3()
    Cesium.Cartesian3.subtract(endPosition, startPosition, moveVector)
    const moveLength = -Cesium.Cartesian3.dot(this.axisX.direction, moveVector)
    this.translation(moveVector,  this.axisX.unit, moveLength)
}

VFG.OrbitControls.prototype._moveY=function (e) {
	if (!this.pickPoint) return
    this.viewer.camera.getPickRay(e.startPosition, this.pickRay)
    const startPosition = this.getPlanePosition(this.pickPoint, this.viewer.camera.position.clone(), this.pickRay, this.axisY.direction)
    this.viewer.camera.getPickRay(e.endPosition, this.pickRay)
    const endPosition = this.getPlanePosition(this.pickPoint, this.viewer.camera.position.clone(), this.pickRay, this.axisY.direction)
    const moveVector = new Cesium.Cartesian3()
    Cesium.Cartesian3.subtract(endPosition, startPosition, moveVector)
    const moveLength = -Cesium.Cartesian3.dot(this.axisY.direction, moveVector)
    this.translation(moveVector,  this.axisY.unit, moveLength)
}

VFG.OrbitControls.prototype._moveZ=function (e) {
	if (!this.pickPoint) return
    this.viewer.camera.getPickRay(e.startPosition, this.pickRay)
    const startPosition = this.getPlanePosition(this.pickPoint, this.viewer.camera.position.clone(), this.pickRay, this.axisZ.direction)
    this.viewer.camera.getPickRay(e.endPosition, this.pickRay)
    const endPosition = this.getPlanePosition(this.pickPoint, this.viewer.camera.position.clone(), this.pickRay, this.axisZ.direction)
    const moveVector = new Cesium.Cartesian3()
    Cesium.Cartesian3.subtract(endPosition, startPosition, moveVector)
    const moveLength = -Cesium.Cartesian3.dot(this.axisZ.direction, moveVector)
    this.translation(moveVector,  this.axisZ.unit, moveLength)
}

/**
 * 获取平面上的位置
 * @param position{Cesium.Cartesian3} 模型位置
 * @param cameraPosition{Cesium.Cartesian3} 相机位置
 * @param pickRay{Cesium.Ray} 从相机到屏幕的射线
 * @param axisDirection{Cesium.Cartesian3} 轴的向量
 */
VFG.OrbitControls.prototype.getPlanePosition =function(position, cameraPosition, pickRay, axisDirection) {
    // 第一步, 获取相机在轴上的投影
    const cartesian3 = Cesium.Cartesian3.subtract(cameraPosition, position, new Cesium.Cartesian3())
    const length = Cesium.Cartesian3.dot(cartesian3, axisDirection)
    // 获取轴上投影的位置, 以相机到这个位置, 为平面法线
    Cesium.Cartesian3.multiplyByScalar(axisDirection, length, cartesian3);
    Cesium.Cartesian3.add(position, cartesian3, cartesian3)
    const pn = Cesium.Cartesian3.subtract(cameraPosition, cartesian3, new Cesium.Cartesian3())
    // 获取单位向量, 射线向投影向量投影
    Cesium.Cartesian3.normalize(pn, cartesian3)
    const number = Cesium.Cartesian3.dot(pickRay.direction, cartesian3)
    // 获取射线与平面相交点
    const number1 = Cesium.Cartesian3.magnitude(pn)
    Cesium.Cartesian3.multiplyByScalar(pickRay.direction, number1/number, cartesian3);
    return cartesian3
}

VFG.OrbitControls.prototype.rayPlaneIntersection=function(ray,cameraDirection,pickPoint){
  if (!pickPoint) {
	  return
  }
  var result = new Cesium.Cartesian3()
  var number = Cesium.Cartesian3.dot(cameraDirection, pickPoint);
  var number1 = Cesium.Cartesian3.dot(cameraDirection, ray.origin);
  var number2 = Cesium.Cartesian3.dot(cameraDirection, ray.direction);
  var t = (number - number1) / number2;
  return Cesium.Cartesian3.add(ray.origin, Cesium.Cartesian3.multiplyByScalar(ray.direction, t, result), result);
}

/**
 * 平移
 * @param moveVector{Cesium.Cartesian3} 移动距离
 * @param unit
 * @param moveLength
 */
VFG.OrbitControls.prototype.translation=function(moveVector, axisUnit, moveLength){
	Cesium.Matrix4.multiplyByTranslation(
        this._primitiveX.modelMatrix,
        Cesium.Cartesian3.multiplyByScalar(axisUnit, moveLength*0.01, new Cesium.Cartesian3()),
        this._primitiveX.modelMatrix
    );
	Cesium.Matrix4.multiplyByTranslation(
        this._primitiveY.modelMatrix,
        Cesium.Cartesian3.multiplyByScalar(axisUnit, moveLength*0.01, new Cesium.Cartesian3()),
        this._primitiveY.modelMatrix
    )
	Cesium.Matrix4.multiplyByTranslation(
        this._primitiveZ.modelMatrix,
        Cesium.Cartesian3.multiplyByScalar(axisUnit, moveLength*0.01, new Cesium.Cartesian3()),
        this._primitiveZ.modelMatrix
    )	
    Cesium.Matrix4.getTranslation(Cesium.Matrix4.multiplyByUniformScale(this._primitiveZ.modelMatrix,1, new Cesium.Matrix4()), this.position);
	
	this._primitiveXY.modelMatrix=this.getModelMatrixXY();
	this._primitiveXZ.modelMatrix=this.getModelMatrixXZ();
	this._primitiveYZ.modelMatrix=this.getModelMatrixYZ();
	if(this.options.callback){
		this.options.callback(this.position);
	}
	
}
VFG.OrbitControls.prototype.getPostition=function (){
	var cartesian3=new Cesium.Cartesian3 ();
	Cesium.Matrix4.getTranslation(this._primitiveZ.modelMatrix,cartesian3);
	return cartesian3
}
VFG.OrbitControls.prototype.destroy = function () {
  if(this._primitiveX){
	  this.viewer.scene.primitives.remove( this._primitiveX );
  }
  if(this._primitiveY){
	  this.viewer.scene.primitives.remove( this._primitiveY );
  }
  if(this._primitiveZ){
	  this.viewer.scene.primitives.remove( this._primitiveZ );
  }
  if(this._primitiveXY){
	  this.viewer.scene.primitives.remove( this._primitiveXY );
  }
  if(this._primitiveXZ){
	  this.viewer.scene.primitives.remove( this._primitiveXZ );
  }
  if(this._primitiveYZ){
	  this.viewer.scene.primitives.remove( this._primitiveYZ );
  }
  delete this.viewer;
  return Cesium.destroyObject(this);
};
