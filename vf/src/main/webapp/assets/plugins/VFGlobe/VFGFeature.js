VFG.Feature=function(){
}
VFG.Feature.Layers= new Cesium.PrimitiveCollection();
VFG.Feature.Map= new Map();

/**
 * id查询点
 */
VFG.Feature.getById=function(id){
	if(this.Map.has(id)){
		return this.Map.get(id);
	}
}

/**
 * 删除单体
 */
VFG.Feature.removeById=function(id){
	if(this.Map.has(id)){
		var classification =this.Map.get(id);
		if(this.Layers.remove(classification.primitive)){
			classification=null;
			this.Map.delete(id);
			return true;
		}
	}
}

VFG.Feature.add=function(params){
	var classification =new VFG.ClassificationPrimitive(params);
	if(classification && classification.primitive){
		this.Layers.add(classification.primitive);
		this.Map.set(classification.id,classification);
	}
	return classification;
}

/**
 * 平移
 */
VFG.Feature.translate=function(id,position){
	var modelFeature=this.getById(id);
	if(!modelFeature){
		return;
	}
    modelFeature.translateFeature(position);
}

/**
 * 更新尺寸
 */
VFG.Feature.setDimensions=function(id,dimensions){
	var modelFeature=this.getById(id);
	if(!modelFeature){
		return;
	}
	this.Layers.remove(modelFeature.primitive)
	modelFeature.setDimensions(dimensions);
	if(modelFeature && modelFeature.primitive){
		this.Layers.add(modelFeature.primitive);
	}
}

/**
 * 设置颜色
 */
VFG.Feature.setNormalColor=function(id,color){
	var modelFeature=this.getById(id);
	if(!modelFeature){
		return;
	}
    modelFeature.setNormalColor(color) ;
}

/**
 * 设置悬浮颜色
 */
VFG.Feature.setHoverColor=function(id,color){
	var modelFeature=this.getById(id);
	if(!modelFeature){
		return;
	}
    modelFeature.setHoverColor(color) ;
}

/**
 * 是否长显示
 */
VFG.Feature.setDisplay=function(id,isView){
	var modelFeature=this.getById(id);
	if(!modelFeature){
		return;
	}
    modelFeature.setDisplay(isView);
}

/**
 * 旋转
 */
VFG.Feature.rotate=function(id,rotation){
	var modelFeature=this.getById(id);
	if(!modelFeature){
		return;
	}
	modelFeature.rotateFeature(rotation);
}


VFG.Feature.show=function(viewer,option){
	
}

VFG.Feature.update=function(option){
	var modelFeature=this.getById(option.id);
	if(!modelFeature){
		return;
	}
	
	if(option.isView){
		this.setDisplay(option.id,option.isView);
	}
	
	if(option.position){
		this.translate(option.id,option.position);
	}
	
	if(option.dimensions){
		this.setDimensions(option.id,option.dimensions);
	}
	
	if(option.rotation){
		this.rotate(option.id,option.rotation);
	}
	
	if(option.normalColor){
		this.setNormalColor(option.id,option.normalColor);
	}
	if(option.hoverColor){
		this.setHoverColor(option.id,option.hoverColor);
	}
	
}

