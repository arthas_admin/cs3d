/**
 * 添加
 */
VFG.Viewer.prototype.add=function(option){
	var _this=this;
	if('Point'==option.type){
		_this.addPoint(option);
	}
	else if('Polyline'==option.type){
		_this.addPolyline(option)
	}
	else if('Polygon'==option.type){
		_this.addPolygon(option)
	}
	else if('Model'==option.type){
		_this.addModel(option)
	}
	else if('Map'==option.type){
		_this.addProvider(option);
	}
	else if('VideoPlane'==option.type){
		_this.addVPlane(option);
	}
	else if('VideoModel'==layer.type){
		_this.addVModel(option.entity);
	}
	else if('VideoShed3d'==layer.type){
		_this.addVideoShed3d(option.entity);
	}
	else if('Feature'==option.type){
		_this.addFeature(option);
	}
	else if('Roam'==option.type){
		_this.addRoam(option);
	}
}

/**
 * 切换样式
 * id:对象id
 * event:DEFAULT  HOVER SELECTED
 * type: Point、Polyline、Polygon、Model。。。
 */
VFG.Viewer.prototype.changeStyle=function(id,event,type){
	var _this=this;
	if('Point'==type){
		_this.changePointStyle(id,event)
	}
	else if('Polyline'==type){
		_this.changePolylineStyle(id,event)
	}
	else if('Polygon'==type){
		_this.changePolygonStyle(id,event)
	}
	else if('Model'==type){
		_this.changeModelStyle(id,event);
	}
	else if('Map'==type){
	}
	else if('VideoPlane'==type){
	}
	else if('Feature'==type){
		_this.changeFeatureStyle(id,event);
	}
}

/**
* @Description  设置样式
* @param
*/
VFG.Viewer.prototype.setStyle=function(param){
	var _this=this; 
	var style = _this.getStyle(param.styleId);
	 if(style && "1"==style.enabled){
		 if('Point'==param.type){
			 _this.setPointStyle({
				 id:param.id,
				 style:style,
				 cache:param.cache,
			 });
		 }
		 else if('Polyline'==param.type){
			 _this.setPolylineStyle({
				 id:param.id,
				 style:style,
				 cache:param.cache,
			 });
		 }
		 else if('Polygon'==param.type){
			 _this.setPolygonStyle({
				 id:param.id,
				 style:style,
				 cache:param.cache,
			 });
		 }
		 else if('Model'==param.type){
		 }
		 else if('Map'==param.type){
		 }
		 else if('VideoPlane'==param.type){
		 }
	 }
}

/**
 * 获取对象
 */
VFG.Viewer.prototype.getById=function(id,type){
	var _this=this;
	if('Point'==type){
		if(_this.cacheMessageMap.has(id)){
			return _this.cacheMessageMap.get(id);
		}else{
			return _this.getPointById(id);
		}
	}
	else if('Polyline'==type){
		return _this.getPolylineById(id);
	}
	else if('Polygon'==type){
		return _this.getPolygonById(id);
	}
	else if('Model'==type){
		return _this.getModelById(id);
	}
	else if('Map'==type){
		return _this.getProviderById(id);
	}
	else if('VideoPlane'==type){
		return _this.getVPlaneById(id);
	}
	else if('VideoModel'==type){
		return _this.getVModelById(id);
	}
	else if('Feature'==type){
		return _this.getFeatureById(id);
	}
	else if('Roam'==type){
		_this.getRoamById(id);
	}
}

/*
 * 移除对象
 */
VFG.Viewer.prototype.removeById=function(id,type){
	
	if('Point'==type){
		this.removePointById(id);
	}
	else if('Model'==type){
		this.removeModelById(id);
	}
	else if('Polyline'==type){
		this.removePolylineById(id);
	}
	else if('Polygon'==type){
		this.removePolygonById(id);
	}
	else if('Feature'==type){
		this.removeFeatureById(id);
	}
	else if('VideoPlane'==type){
		this.removeVPlaneById(id);
	}
	else if('VideoShed3d'==type){
		this.removeVideoShed3dById(id);
	}
	else if('VideoModel'==type){
		this.removeVModelById(id);
	}
	else if('Roam'==type){
		_this.removeRoamById(id);
	}
}

/**
 * 显示或隐藏
 * option:{
 *   id:对象id,
 *   type:Point..,
 *   state:true/false
 * }
 */
VFG.Viewer.prototype.show=function(option){
	var _this=this;
	var type=option.type;
	var state=option.state;
	if('Point'==type){
		_this.showPoint(option);
	}
	else if('Polyline'==type){
		_this.showPolyline(option);
	}
	else if('Polygon'==type){
		_this.showPolygon(option);
	}
	else if('Models'==type || 'GLTF'==type || '3DTILES'==type || 'Model'==type){
		_this.showModel(option);
	}
	else if('Maps'==type || 'terrain'==type || 'division'==type || 'Map'==type || 'map'==type || 'road'==type ){
		_this.showProvider(option);
	}
}

/**
 * 更新
 */
VFG.Viewer.prototype.update=function(option){
	var _this=this;
	if('Point'==option.type){
		_this.updatePoint(option);
	}
	else if('Polyline'==option.type){
		_this.updatePolyline(option);
	}
	else if('Polygon'==option.type){
		_this.updatePolyline(option);
	}
	else if('Model'==option.type){
		_this.updateModel(option);
	}
	else if('Map'==option.type){
		_this.updateMap(option);
	}
	else if('VideoPlane'==option.type){
		_this.updateVideoPlane(option);
	}
	else if('VideoModel'==option.type){
		_this.updateVideoModel(option);
	}
	else if('Feature'==option.type){
		_this.updateFeature(option);
	}
	else if('Roam'==option.type){
		return _this.updateRoam(option);
	}
}

/**
 * 是否包含
 * option：{
 *   id:对象id,
 *   type:Point、Polyline 。。
 * }
 */
VFG.Viewer.prototype.contain=function(option){
	var _this=this;
	if('Point'==option.type){
		return _this.containPoint(option.id);
	}
	else if('Polyline'==option.type){
		return _this.containPolyline(option.id);
	}
	else if('Polygon'==option.type){
		return _this.containPolyline(option.id);
	}
	else if('Model'==option.type){
		return _this.containModel(option.id);
	}
	else if('Map'==option.type){
		return _this.containMap(option.id);
	}
	else if('VideoPlane'==option.type){
		return _this.containVideoPlane(option.id);
	}
	else if('VideoModel'==option.type){
		return _this.containVideoModel(option.id);
	}
	else if('Feature'==option.type){
		return _this.containFeature(option.id);
	}
	else if('Roam'==option.type){
		return _this.containRoam(option.id);
	}
}

/**
 * 层级
 */
VFG.Viewer.prototype.getLevel=function() {
	 var tilesToRender=this.viewer.scene.globe._surface._tilesToRender;
	if(tilesToRender.length>0){
		return tilesToRender[0].level;
	}else{
		return 0;
	}
};

/**
 * 渲染
 * option{}  调用接口：http://localhost:8080/vf/i/scene/layer/getEntities(layerId)  或 getEntity(entityId)
 */
VFG.Viewer.prototype.render=function(option){
	var _this=this;
	if(!option){
		console.log('参数必填！');
		return;
	}
	if(!option.layer){
		console.log('格式有误！');
		return;
	}
	var layer=option.layer;
	
	if('Point'==layer.type){
		_this.addPoint(option.entity);
	}
	else if('Points'==layer.type){
		_this.addPoints(option.entities)
	}
	else if('Polyline'==layer.type){
		_this.addPolyline(option.entity)
	}
	else if('Polylines'==layer.type){
		_this.addPolylines(option.entities)
	}
	else if('Polygon'==layer.type){
		_this.addPolygon(option.entity)
	}
	else if('Polygons'==layer.type){
		_this.addPolygons(option.entities)
	}
	else if('Model'==layer.type){
		_this.addModel(option.entity)
	}
	else if('Models'==layer.type){
		_this.addModels(option.entities)
	}
	else if('Map'==layer.type){
		_this.addProvider(option.entity);
	}
	else if('Maps'==layer.type){
		_this.addProviders(option.entities);
	}
	else if('VideoPlane'==layer.type){
		_this.addVPlane(option.entity);
	}
	else if('VideoPlanes'==layer.type){
		_this.addVPlanes(option.entities);
	}
	else if('Feature'==layer.type){
		_this.addFeature(option.entity);
	}
	else if('Features'==layer.type){
		_this.addFeatures(option.entities);
	}
	else if('VideoModel'==layer.type){
		_this.addVModel(option.entity);
	}
	else if('VideoModels'==layer.type){
		_this.addVModels(option.entities);
	}
	else if('VideoShed3d'==layer.type){
		_this.addVideoShed3d(option.entity);
	}
	else if('VideoShed3ds'==layer.type){
		_this.addVideoShed3ds(option.entities);
	}
	else if('Roam'==layer.type){
		_this.addRoam(option.entity);
	}
}


//漫游
VFG.Viewer.prototype.creatRoam=function(option){
	return new VFG.Roam(this.viewer,option);
}

//绕点旋转
VFG.Viewer.prototype.creatAroundPoint=function(option){
	return new VFG.AroundPoint(this.viewer,option);
}

VFG.Viewer.prototype.enableRotate=function(enabled){
	this.viewer.scene.screenSpaceCameraController.enableRotate = enabled;
}

/**
 * 平移
 */
VFG.Viewer.prototype.enableTranslate=function(enabled){
	this.viewer.scene.screenSpaceCameraController.enableTranslate = enabled;
}

/**
 * 放大和缩小
 */
VFG.Viewer.prototype.enableZoom=function(enabled){
	this.viewer.scene.screenSpaceCameraController.enableZoom = enabled;
}
/**
 * 倾斜相机
 */
VFG.Viewer.prototype.enableTilt=function(enabled){
	this.viewer.scene.screenSpaceCameraController.enableZoom = enabled;
}

/**
 * 获取视角
 */
VFG.Viewer.prototype.getVisualAngle=function() {
	return {
		heading: Cesium.Math.toDegrees(this.viewer.camera.heading).toFixed(2),
		pitch:Cesium.Math.toDegrees(this.viewer.camera.pitch).toFixed(2),
		roll:Cesium.Math.toDegrees(this.viewer.camera.roll).toFixed(2),
		position:VFG.Util.getLnLaFormC3(this.viewer,this.viewer.camera.position)
	}
};

/**
 * 隐藏球体
 */
VFG.Viewer.prototype.hideGlobe=function() {
	this.viewer.scene.sun.show = false; 
	this.viewer.scene.moon.show = false;
	this.viewer.scene.skyBox.show = false;//关闭天空盒，否则会显示天空颜色
	//this.viewer.scene.undergroundMode = true; //重要，开启地下模式，设置基色透明，这样就看不见黑色地球了
	//this.viewer.scene.underGlobe.show = true;
	//this.viewer.scene.underGlobe.baseColor = new Cesium.Color(0, 0, 0, 0);
	this.viewer.scene.globe.show = false; //不显示地球，这条和地球透明度选一个就可以
	this.viewer.scene.globe.baseColor = new Cesium.Color(0, 0, 0, 0);
	this.viewer.scene.backgroundcolor = new Cesium.Color(0, 0, 0, 0)
};

/**
 * 飞行
 */
VFG.Viewer.prototype.flyTo=function(option){
	var position=option.position;
	var orientation=option.orientation;
	var param={};
	if(position){
		param.destination=Cesium.Cartesian3.fromDegrees(position.x,position.y, position.z);//x:经度,y:纬度,z:高度
	}
	if(orientation){
		param.orientation={
	        heading : Cesium.Math.toRadians(orientation.heading*1),//heading：角度
	        pitch : Cesium.Math.toRadians(orientation.pitch*1),//pitch：角度
	        roll : Cesium.Math.toRadians(orientation.roll*1),//roll：角度
	    };
	}
	if(option.complete){//飞行结束
		param.complete=option.complete(_this);
	}
	if(option.cancle){//飞行取消
		param.cancle=option.cancle(_this);
	}
	this.viewer.camera.flyTo(param);
}

/**
 * 聚合显示
 * option：调用接口：http://localhost:8080/vf/i/scene/layer/getCluster(layerId) 	 
 */
VFG.Viewer.prototype.addCluster=function(option){
	var _this=this;
	if(!_this.cacheClusterMap.has(option.layerId)){
		var points=option.points;
		var Layers=new Cesium.CustomDataSource(option.layerId);
		for(var i=0;i<points.length;i++){
			var point=points[i];
			var entity=VFG.Point.addEntity(_this.viewer,{
				point:point,
				style:point.defaultStyleId?_this.POINT_STYLE[point.defaultStyleId]:null
			},_this.url);
			Layers.entities.add(entity);
		}
		var cluster=new VFG.Layer.Cluster(_this.viewer,{
			Layers:Layers
		});
		_this.cacheClusterMap.set(option.layerId,cluster);
	}
}

/**
 * 移除聚合
 * option：{
 * 	layerId：layerId
 * }
 */
VFG.Viewer.prototype.removeCluster=function(option){
	var _this=this;
	if(_this.cacheClusterMap.has(option.layerId)){
		_this.cacheClusterMap.get(option.layerId).destroy();
		_this.cacheClusterMap.delete(option.layerId);
	}
}


