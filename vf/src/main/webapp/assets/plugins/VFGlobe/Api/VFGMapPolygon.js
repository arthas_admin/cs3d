/**********************************************************************************************************/
VFG.Viewer.prototype.cachePolygonMap=new Map();
VFG.Viewer.prototype.selectPolygonMap=new Map();

VFG.Viewer.prototype.addPolygon=function(polygon){
	var _this=this;
	 var style=_this.getStyle(polygon.defaultStyleId);
	 console.log(style);
	 if(style && "1"==style.enabled){
		VFG.Polygon.add({
			polygon:polygon,
			style:style
		},_this.url);
	 }else{
		VFG.Polygon.add({
			polygon:polygon,
			style:null
		},_this.url);
	 }
	_this.cachePolygonMap.set(polygon.id,polygon);
}

VFG.Viewer.prototype.addPolygons=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var polygon=list[i];
		 _this.addPolygon(polygon);
	 }
}

VFG.Viewer.prototype.removePolygon=function(polygon){
	var _this=this;
	if(this.containPolygon(polygon.id)){
		_this.removePolygonById(polygon.id);
	}
}

VFG.Viewer.prototype.removePolygons=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var polygon=list[i];
		 _this.removePolygon(polygon);
	 }
}

VFG.Viewer.prototype.removePolygonById=function(id){
	var _this=this;
	 VFG.Polygon.removeById(id);
	 _this.cachePolygonMap.delete(id);
	 _this.selectPolygonMap.delete(id);
}

/**
 * 移除所有
 */
VFG.Viewer.prototype.removeAllPolygons=function(){
	var _this=this;
	_this.cachePolygonMap.forEach(function(value,key){
		VFG.Polygon.removeById(key);
	});
	_this.cachePolygonMap.clear();
	_this.selectPolygonMap.clear();
}

VFG.Viewer.prototype.changePolygonStyle=function(id,event){
	var _this=this;
	if(_this.containPolygon(id)){
		 var polygon=_this.getPolygonById(id);
		 if('DEFAULT'==event && polygon.defaultStyleId){
			 var style=_this.getStyle(polygon.defaultStyleId);
			 if(style && "1"==style.enabled){
				 _this.setPolygonStyle({
					 id:polygon.id,
					 style:style,
					 cache:false,
				 });
			 }
		 }
		 else if('HOVER'==event && polygon.hoverStyleId){
			 if(!_this.selectPolygonMap.has(polygon.id)){
				 var style=_this.getStyle(polygon.hoverStyleId);
				 if(style && "1"==style.enabled){
					 _this.setPolygonStyle({
						 id:polygon.id,
						 style:style,
						 cache:false,
					 });
				 }
			 }
		 }
		 else if('SELECTED'==event && polygon.selectedStyleId){
			 var style=_this.getStyle(polygon.selectedStyleId);
			 if(style && "1"==style.enabled){
				 _this.setPolygonStyle({
					 id:polygon.id,
					 style:style,
					 cache:true,
				 });
			 }
		 }else{
			 if(polygon.selectedStyleId){
				 var style=_this.getStyle(polygon.defaultStyleId);
				 if(style && "1"==style.enabled){
					 _this.setPolygonStyle({
						 id:polygon.id,
						 style:style,
						 cache:false,
					 });
				 }
			 }
		 }
	}
}

VFG.Viewer.prototype.setPolygonStyle=function(param){
	var _this=this;
	var polygon=_this.getPolygonById(param.id);
	if(polygon && param.style){
	   VFG.Polygon.update({
		polygon:polygon,
	 	style:param.style
	   },_this.url);
	   if(param.cache==true){
		   _this.selectPolygonMap.set(polygon.id,polygon);
	   }else{
		   _this.selectPolygonMap.delete(polygon.id);
	   }
	}
}

VFG.Viewer.prototype.containPolygon=function(id){
	return this.cachePolygonMap.has(id)
}

VFG.Viewer.prototype.getPolygonById=function(id){
	var _this=this;
	if(_this.containPolygon(id)){
		 return _this.cachePolygonMap.get(id);
	}
}

VFG.Viewer.prototype.showPolygon=function(param){
	var _this=this;
	VFG.Polygon.show(param);
}

VFG.Viewer.prototype.showPolygons=function(list,show){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var polygon=list[i];
		VFG.Polygon.show({
			id:polygon.id,
			show:show,
		});
	}
} 

VFG.Viewer.prototype.flyToPolygonById=function(id){
	if(this.containPolygon(id)){
		var e=this.getPolygonById(id);
		if(e.cameraX && e.cameraY && e.cameraZ){
			this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(e.cameraX*1,e.cameraY*1, e.cameraZ*1),
			    orientation : {
			        heading : e.heading*1,
			        pitch : e.pitch*1,
			        roll : e.roll*1
			    }
			});
		}
	}
}
