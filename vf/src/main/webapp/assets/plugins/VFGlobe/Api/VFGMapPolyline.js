VFG.Viewer.prototype.cachePolylineMap=new Map();
VFG.Viewer.prototype.selectPolylineMap=new Map();

/**
 * 添加线
 */
VFG.Viewer.prototype.addPolyline=function(param){
	var _this=this;
	_this.cachePolylineMap.set(param.id,param);
	return VFG.Polyline.add({
		polyline:param,
		style:param.layerId?_this.getLayerStyle(param.layerId,'DEFAULT'):null
	},_this.url);
}

VFG.Viewer.prototype.addPolylines=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var polyline=list[i]; 
		_this.addPolyline(polyline);
	}
}

VFG.Viewer.prototype.removePolyline=function(polyline){
	var _this=this;
	if(_this.cachePolylineMap.has(polyline.id)){
		_this.removePolylineById(polyline.id);
	}
}

VFG.Viewer.prototype.removePolylines=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var polyline=list[i]; 
		_this.removePolyline(polyline);
	}
}

VFG.Viewer.prototype.removePolylineById=function(id){
	var _this=this;
	 VFG.Polyline.removeById(id);
	 _this.cachePolylineMap.delete(id);
	 _this.selectPolylineMap.delete(id);
}

VFG.Viewer.prototype.removeAllPolylines=function(){
	var _this=this;
	_this.cachePolylineMap.forEach(function(value,key){
		VFG.Polyline.removeById(key);
	});
	_this.cachePolylineMap.clear();
	_this.selectPolylineMap.clear();
}


VFG.Viewer.prototype.changePolylineStyle=function(id,event){
	var _this=this;
	 var polyline=_this.getPolylineById(id);
	 if(polyline!=null && polyline.layerId){
		 if('DEFAULT'==event && polyline.layerId){
			 var style=_this.getLayerStyle(polyline.layerId,'DEFAULT');
			 if(style && "1"==style.enabled){
				 _this.setPolylineStyle({
					 id:polyline.id,
					 style:style,
					 cache:false,
				 });
			 }
		 }
		 else if('HOVER'==event && polyline.layerId){
			 if(!_this.selectPolylineMap.has(polyline.id)){
				 var style=_this.getLayerStyle(polyline.layerId,'HOVER');
				 if(style && "1"==style.enabled){
					 _this.setPolylineStyle({
						 id:polyline.id,
						 style:style,
						 cache:false,
					 });
				 }
			 }
		 }
		 else if('SELECTED'==event && polyline.layerId){
			 var style=_this.getLayerStyle(polyline.layerId,'SELECTED');
			 if(style && "1"==style.enabled){
				 _this.setPolylineStyle({
					 id:polyline.id,
					 style:style,
					 cache:true,
				 });
			 }
		 }else{
			 if(polyline.layerId){
				 var style=_this.getLayerStyle(polyline.layerId,'DEFAULT');
				 if(style && "1"==style.enabled){
					 _this.setPolylineStyle({
						 id:polyline.id,
						 style:style,
						 cache:false,
					 });
				 }
			 }
		 }
	 }
}

VFG.Viewer.prototype.getPolylineById=function(id){
	var _this=this;
	if(_this.cachePolylineMap.has(id)){
		 return _this.cachePolylineMap.get(id);
	}
}

VFG.Viewer.prototype.setPolylineStyle=function(param){
	var _this=this;
	var polyline=_this.getPolylineById(param.id);
	if(polyline && param.style){
	   VFG.Polyline.update({
		   polyline:polyline,
		   style:param.style
	   },_this.url);
	   if(param.cache==true){
		   _this.selectPolylineMap.set(polyline.id,polygon);
	   }else{
		   _this.selectPolylineMap.delete(polyline.id);
	   }
	}
}


VFG.Viewer.prototype.showPolylines=function(list,show){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var polyline=list[i];
		VFG.Polyline.show({
			id:polyline.id,
			show:show,
		});
	}
} 

VFG.Viewer.prototype.showPolyline=function(option){
	var _this=this;
	VFG.Polyline.show(option);
}

