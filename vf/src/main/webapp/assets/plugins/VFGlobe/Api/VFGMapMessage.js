VFG.Viewer.prototype.cacheEventMap=new Map();
VFG.Viewer.prototype.cacheMessageMap=new Map();
VFG.Viewer.prototype.filterMessageMap=new Map();
VFG.Viewer.prototype.layerMessageMap=new Map();
/**
 * 添加控制图层Key
 */
VFG.Viewer.prototype.addLayerMessage=function(key,value){
	var _this=this;
	if(!this.layerMessageMap.has(key)){
		this.layerMessageMap.set(key,value);
		_this.newMessageMap().forEach(function(value,key){
			_this.addMessageEventPoint(value);
		});
	}
}


VFG.Viewer.prototype.newMessageMap=function(){
	var _this=this;
	var newMap = new Map();
	_this.cacheEventMap.forEach(function(value,key){
		newMap.set(key,value);
	});
	return newMap;
}


/**
 * 移除控制图层Key
 */
VFG.Viewer.prototype.removeLayerMessage=function(key){
	var _this=this;
	if(this.layerMessageMap.has(key)){
		this.layerMessageMap.delete(key);
		_this.newMessageMap().forEach(function(value,key){
			_this.addMessageEventPoint(value);
		});
	}
}

/**
 * 批量移除控制图层Key
 */
VFG.Viewer.prototype.removeAllLayerMessage=function(){
	var _this=this;
	_this.layerMessageMap.clear();
	_this.cacheMessageMap.forEach(function(value,key){
		VFG.Point.removePrimitiveById(key);
		_this.viewer.entities.removeById(key);
	});
	_this.cacheMessageMap.clear();
}

/**
 * 添加过滤条件
 */
VFG.Viewer.prototype.addFilterMessage=function(key,value){
	var _this=this;
	if(!this.filterMessageMap.has(key)){
		this.filterMessageMap.set(key,value);
		_this.newMessageMap().forEach(function(value,key){
			_this.addMessageEventPoint(value);
		});
	}
}

/**
 * 移除过滤条件
 */
VFG.Viewer.prototype.removeFilterMessage=function(key){
	var _this=this;
	if(this.filterMessageMap.has(key)){
		this.filterMessageMap.delete(key);
		_this.newMessageMap().forEach(function(value,key){
			_this.addMessageEventPoint(value);
		});
	}
}

/**
 * 批量移除控制图层Key
 */
VFG.Viewer.prototype.removeAllFilterMessage=function(){
	var _this=this;
	this.filterMessageMap.clear();
	_this.newMessageMap().forEach(function(value,key){
		_this.addMessageEventPoint(value);
	});
}


/**
 * 处理消息
 */
VFG.Viewer.prototype.handleMessage=function(event){
	var _this=this;
	var result=JSON.parse(event.data);
	if(result && result.code=='event'){
		var data=result.data;
		if(data){
			_this.addMessageEventPoint(data);
		}
	}
}

VFG.Viewer.prototype.addMessageEventPoint=function(data){
	var _this=this;
	if(data){
		var id=data.id||null;
		var level=data.level||null;
		var show=data.show||false;
		var code=data.code||null;
		var state=data.state||null;
		var type=data.type||null;
		var event=data.event||null;
		_this.cacheEventMap.delete(data.id);
		_this.cacheEventMap.set(data.id,data);
		if('Point'==type && event){
			if(event.x && event.x.length>0 && event.y && event.y.length>0 && event.z && event.z.length>0 ){
				VFG.Point.removePrimitiveById(event.id);
				_this.cacheMessageMap.delete(event.id) ;
				_this.viewer.entities.removeById(event.id)
				
				if(!show){
					return;
				}
				
				var filters=event.filters || null;
				var layerCode=event.layerCode || null;
				if(_this.filterMessageMap.size>0){
					if(filters && filters.length>0){
						_this.filterMessageMap.forEach(function(value,key){
							if(filters.indexOf(key)!=-1){
								if(_this.layerMessageMap.size>0 && _this.layerMessageMap.has(layerCode)){
									var key=code+state;
									var defaultStyleId='';
									var hoverStyleId='';
									var selectedStyleId='';
									var color=Cesium.Color.RED;
									if(_this.EVENT_CONFIG[key]){
										defaultStyleId=_this.EVENT_CONFIG[key].defaultStyleId;
										hoverStyleId=_this.EVENT_CONFIG[key].hoverStyleId;
										selectedStyleId=_this.EVENT_CONFIG[key].selectedStyleId;
										color=_this.EVENT_CONFIG[key].color? Cesium.Color.fromCssColorString(_this.EVENT_CONFIG[key].color) :Cesium.Color.RED;
									}
									var point={
										id:event.id||id,
										name:event.name||'',
										code:event.layerCode|| '',
										state:state,
										x:event.x*1,
										y:event.y*1,
										z:event.z*1,
										defaultStyleId:defaultStyleId,
										hoverStyleId:hoverStyleId,
										selectedStyleId:selectedStyleId,
										code:code
									};
									
									if('1'==level){
										var position= Cesium.Cartesian3.fromDegrees(event.x*1,event.y*1,event.z*1);
										_this.getEventMessagePoint(event.id,event.name,position,color);
										if(defaultStyleId){
											VFG.Point.addPrimitive(_this.viewer,{
												point:point,
												style:_this.POINT_STYLE[defaultStyleId]
											},_this.url);
										}
									}else{
										VFG.Point.addPrimitive(_this.viewer,{
											point:point,
											style:_this.POINT_STYLE[defaultStyleId]
										},_this.url);
									}
									_this.cacheMessageMap.set(event.id,point);
								}
							}
						});
					}
				}else{
					if(_this.layerMessageMap.size>0 && _this.layerMessageMap.has(layerCode)){
						var key=code+state;
						var defaultStyleId='';
						var hoverStyleId='';
						var selectedStyleId='';
						var color=Cesium.Color.RED;
						if(_this.EVENT_CONFIG[key]){
							defaultStyleId=_this.EVENT_CONFIG[key].defaultStyleId;
							hoverStyleId=_this.EVENT_CONFIG[key].hoverStyleId;
							selectedStyleId=_this.EVENT_CONFIG[key].selectedStyleId;
							color=Cesium.Color.fromCssColorString(_this.EVENT_CONFIG[key].color);
						}
						var point={
							id:event.id||id,
							name:event.name||'',
							code:event.layerCode|| '',
							state:state,
							x:event.x*1,
							y:event.y*1,
							z:event.z*1,
							defaultStyleId:defaultStyleId,
							hoverStyleId:hoverStyleId,
							selectedStyleId:selectedStyleId,
							code:code
						};
						if('1'==level){
							var position= Cesium.Cartesian3.fromDegrees(event.x*1,event.y*1,event.z*1);
							_this.getEventMessagePoint(event.id,event.name,position,color);
							if(defaultStyleId){
								VFG.Point.addPrimitive(_this.viewer,{
									point:point,
									style:_this.POINT_STYLE[defaultStyleId]
								},_this.url);
							}
						}else{
							VFG.Point.addPrimitive(_this.viewer,{
								point:point,
								style:_this.POINT_STYLE[defaultStyleId]
							},_this.url);
						}
						_this.cacheMessageMap.set(event.id,point);
					}

				}
			}
		}
	}
}

VFG.Viewer.prototype.removeAllEventMessage=function(){
	var _this=this;
	_this.cacheMessageMap.forEach(function(value,key){
		VFG.Point.removePrimitiveById(key);
		_this.viewer.entities.removeById(key)
	});
	_this.cacheMessageMap.clear();
	_this.cacheEventMap.clear();
	_this.filterMessageMap.clear();
	_this.layerMessageMap.clear();
}

VFG.Viewer.prototype.getEventMessagePoint=function(id,name,position,color){
	var _this=this;
	var x=1;
	var flog=true;
	_this.viewer.entities.removeById(id);
	_this.viewer.entities.add({
		id:id,
		name:name,
		position:position,
		point : {
			show : true,
			color :new Cesium.CallbackProperty(function () {
				if(flog){
					x=x-0.05;
					if(x<=0){
						flog=false;
					}
					}else{
						x=x+0.05;
						if(x>=1){
						flog=true;
						}
					}
					return color.withAlpha(x);
				},false),
			pixelSize : 20, // default: 1
			outlineWidth :0
		}
	});
}




