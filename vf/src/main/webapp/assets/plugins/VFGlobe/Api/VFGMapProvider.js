VFG.Viewer.prototype.cacheProviderMap=new Map();

/**
 * 添加底图
 */
VFG.Viewer.prototype.addProvider=function(provider){
	var _this=this;
	if(!provider) return;
	if(!VFG.Provider.Map.has(provider.id)){
		if('terrain'==provider.dataType){
			var terrainProvider=VFG.Provider.getTerrainProviderFromServ(provider);
			if(terrainProvider){
				VFG.Provider.hasTerrain=true;
				_this.viewer.terrainProvider=terrainProvider;
				VFG.Provider.Map.set(provider.id,terrainProvider);
				_this.cacheProviderMap.set(provider.id,provider);
			}
		}else{
			var imageryProvider=VFG.Provider.getImageryProviderFromServ(provider);
			if(imageryProvider){
				var sImgPro=_this.viewer.imageryLayers.addImageryProvider(imageryProvider);
				VFG.Provider.Map.set(provider.id,sImgPro);
				_this.cacheProviderMap.set(provider.id,provider);
			}
		}
	}
} 

/**
 * 添加底图
 */
VFG.Viewer.prototype.addProviders=function(providers){
	var _this=this;
	if(providers){
		 for(var i=0;i<providers.length;i++){
			 var provider=providers[i];
			 _this.addProvider(provider);
		 }
	}
}

/**
 * 移除所有
 */
VFG.Viewer.prototype.removeAllProviders=function(){
	var _this=this;
	_this.cacheProviderMap.forEach(function(provider,key){
		if('terrain'==provider.dataType){
			VFG.Provider.removeTerrainProviderById(_this.viewer,provider.id);
		}else{
			VFG.Provider.removeById(_this.viewer,provider.id);
		}
	});
	_this.cacheProviderMap.clear();
}

VFG.Viewer.prototype.removeProviders=function(providers){
	var _this=this;
	if(providers){
		 for(var i=0;i<providers.length;i++){
			 var provider=providers[i];
			 _this.removeProvider(provider.id);
		 }
	}
}

/**
 * 移除
 */
VFG.Viewer.prototype.removeProvider=function(id){
	var _this=this;
	if(_this.cacheProviderMap.has(id)){
		var provider=_this.cacheProviderMap.get(id);
		if('terrain'==provider.dataType){
			VFG.Provider.removeTerrainProviderById(_this.viewer,provider.id);
		}else{
			VFG.Provider.removeById(_this.viewer,id);
		}
	}
	_this.cacheProviderMap.delete(id);
}

VFG.Viewer.prototype.getProviderById=function(id){
	var _this=this;
	if(_this.cacheProviderMap.has(id)){
		 return _this.cacheProviderMap.get(id);
	}
}

VFG.Viewer.prototype.showProvider=function(param){
}	


