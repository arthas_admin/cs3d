VFG.Viewer.prototype.drawPolyline=function(option){
	return new VFG.Draw.Polyline(this.viewer,option);
}

VFG.Viewer.prototype.drawPolygon=function(option){
	return new VFG.Draw.Polygon(this.viewer,option);
}

VFG.Viewer.prototype.drawCircle=function(option){
	return new VFG.Draw.Circle(this.viewer,option);
}

VFG.Viewer.prototype.drawRectangle=function(option){
	return new VFG.Draw.Rectangle(this.viewer,option);
}

VFG.Viewer.prototype.drawPoint=function(option){
	return new VFG.Draw.Point(this.viewer,option);
}
