VFG.Viewer.prototype.cacheModelMap=new Map();
VFG.Viewer.prototype.selectModelMap=new Map();

/**
 * 移除所有
 */
VFG.Viewer.prototype.removeAllModels=function(){
	var _this=this;
	_this.cacheModelMap.forEach(function(value,key){
		VFG.Model.removeById(key);
	});
	_this.cacheModelMap.clear();
	_this.selectModelMap.clear();
}

/**
 * 添加模型
 */
VFG.Viewer.prototype.addModel=function(model){
	let _this=this;
	console.log(model);
	VFG.Model.addModel(_this.viewer,model,_this.LocalModelUrl,_this.aliveLocalService);
	_this.cacheModelMap.set(model.id,model);
} 

/**
 * 批量添加
 */
VFG.Viewer.prototype.addModels=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var model=list[i]; 
		 _this.addModel(model);
	 }
} 

VFG.Viewer.prototype.removeModels=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var model=list[i]; 
		_this.removeModelById(model.id);
	}
}

VFG.Viewer.prototype.removeModel=function(model){
	var _this=this;
	_this.removeModelById(model.id);
}

/**
 * 移除模型
 * id：模型id
 */
VFG.Viewer.prototype.removeModelById=function(id){
	var _this=this;
	VFG.Model.removeById(id);
	_this.cacheModelMap.delete(id);
	_this.selectModelMap.delete(id);
} 

VFG.Viewer.prototype.getModelById=function(id){
	if(this.cacheModelMap.has(id)){
		return this.cacheModelMap.get(id);
	}
}

VFG.Viewer.prototype.setViewModelById=function(id,complete){
	if(this.cacheModelMap.has(id)){
		this.setViewModel(this.cacheModelMap.get(id))
	}
	if(complete){
		complete(this);
	}
}

VFG.Viewer.prototype.flyToModel=function(id,complete){
	if(this.cacheModelMap.has(id)){
		var option=this.cacheModelMap.get(id);
		
		if(option.cameraX && option.cameraY){
			this.viewer.camera.flyTo({
				destination:Cesium.Cartesian3.fromDegrees(option.cameraX*1,option.cameraY*1,option.cameraZ*1),
				orientation:{
			        heading : Cesium.Math.toRadians(option.heading*1),
			        pitch : Cesium.Math.toRadians(option.pitch*1),
			        roll : Cesium.Math.toRadians(option.roll*1),
			    }
			});
		}else{
			this.viewer.camera.flyTo({
				destination:Cesium.Cartesian3.fromDegrees(option.x*1,option.y*1,option.z*1+5)
			});
		}
	}
	if(complete){
		complete(this);
	}
}

VFG.Viewer.prototype.setViewModel=function(model,complete){
	if(model.cameraX && model.cameraX && model.cameraZ && model.heading && model.pitch && model.roll){
		this.viewer.scene.camera.setView({
			destination:new Cesium.Cartesian3(model.cameraX*1,model.cameraY*1,model.cameraZ*1),
			orientation:{
		        heading : model.heading*1,
		        pitch : model.pitch*1,
		        roll : model.roll*1,
		    }
		});
		if(complete){
			complete(this);
		}
	}else{
		this.viewer.scene.camera.setView({
			destination:Cesium.Cartesian3.fromDegrees(model.x*1,model.y*1,model.z*1+10000||10000)
		});
		if(complete){
			complete(this);
		}
	}
}

VFG.Viewer.prototype.changeModelPitch=function(id,rx){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.rx=rx;
		VFG.Model.update(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelHeading=function(id,ry){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.ry=ry;
		VFG.Model.update(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelRoll=function(id,rz){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.rz=rz;
		VFG.Model.update(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelPosition=function(id,position){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.x=position.x;
		option.y=position.y;
		option.z=position.z;
		VFG.Model.update(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelHeight=function(id,height){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.z=height;
		VFG.Model.update(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelScale=function(id,scale){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.scale=scale;
		VFG.Model.updateScale(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelColor=function(id,value){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.color=value;
		VFG.Model.updateHighlight(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelColorBlendMode=function(id,value){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.colorBlendMode=value;
		VFG.Model.updateHighlight(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelSilhouetteColor=function(id,value){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.silhouetteColor=value;
		VFG.Model.updateHighlight(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelSilhouetteSize=function(id,value){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.silhouetteSize=value;
		VFG.Model.updateHighlight(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelHighlight=function(id,value){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.oftenShow=value;
		VFG.Model.updateHighlight(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.showModel=function(param){
	VFG.Model.show(param);
}	

VFG.Viewer.prototype.changeModelStyle=function(id,event){
	var _this=this;
	 var model=_this.getModelById(id);
	 if(model && "1"==model.oftenShow){
		 if('HOVER'==event){
			 if(!_this.selectModelMap.has(id)){
				 VFG.Model.select(model);
			 }
		 }
		 else if('SELECTED'==event){
			 VFG.Model.select(model);
			 _this.selectModelMap.set(id,model);
		 }else{
			 VFG.Model.unselect(model);
			 _this.selectModelMap.delete(id);
		 }
	 }
}	
