VFG.Viewer.prototype.cacheFeatureMap=new Map();
VFG.Viewer.prototype.selectFeatureMap=new Map();

VFG.Viewer.prototype.addFeature=function(option){
	var _this=this;
	var feature={
		id:option.id,
		name:option.name||'',
		code:option.code||'',
		isView:option.isView||false,
		type:'Feature',
		position:{
			x:option.x,
			y:option.y,
			z:option.z,
		},
		dimensions:{
			x:option.dimensionsX||10,
			y:option.dimensionsY||10,
			z:option.dimensionsZ||10,
		},
		rotation:{
			x:option.rotationX||0,
			y:option.rotationY||0,
			z:option.rotationZ||0,
		},
		normalColor:option.normalColor||null,
		hoverColor:option.hoverColor||null,
		heading:option.heading||null,
		pitch:option.pitch||null,
		roll:option.roll||null,
		cameraX:option.cameraX||null,
		cameraY:option.cameraY||null,
		cameraZ:option.cameraZ||null,
	}
	VFG.Feature.add(feature);	
	_this.cacheFeatureMap.set(option.id,option);
}

VFG.Viewer.prototype.addFeatures=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var feature=list[i];
		 _this.addFeature(feature);
	 }
}

VFG.Viewer.prototype.removeFeatures=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var Feature=list[i];
		_this.removeFeatureById(Feature.id);
	}
}

VFG.Viewer.prototype.showFeatures=function(list,show){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var Feature=list[i];
		VFG.Feature.show(_this.viewer,{
			id:Feature.id,
			show:show,
		});
	}
} 

VFG.Viewer.prototype.showFeature=function(option){
	var _this=this;
	VFG.Feature.show(_this.viewer,option);
}

VFG.Viewer.prototype.removeAllFeature=function(){
	var _this=this;
	_this.cacheFeatureMap.forEach(function(value,key){
		VFG.Feature.removeById(_this.viewer,key);
	});
	_this.cacheFeatureMap.clear();
	_this.selectFeatureMap.clear();
}

VFG.Viewer.prototype.removeFeatureById=function(id){
	var _this=this;
	VFG.Feature.removeById(id);
	this.cacheFeatureMap.delete(id);
	this.selectFeatureMap.delete(id);
}

VFG.Viewer.prototype.removeFeature=function(feature){
	var _this=this;
	if(feature &&feature.id){
		_this.removeFeatureById(feature.id);
		_this.cacheFeatureMap.delete(feature.id);
		_this.selectFeatureMap.delete(feature.id);
	}
}

VFG.Viewer.prototype.changeFeatureStyle=function(id,event){
	var _this=this;
	 var feature=VFG.Feature.getById(id);
	 if(feature!=null){
		 if('DEFAULT'==event){
			 feature.leave();
			 this.selectFeatureMap.delete(id);
		 }
		 else if('HOVER'==event){
			 if(!this.selectFeatureMap.has(id)){
				 feature.enter();
				 this.selectFeatureMap.delete(id);
			 }
		 }
		 else if('SELECTED'==event){
			 feature.enter();
			 this.selectFeatureMap.set(id,id);
		 }else{
			 feature.leave();
			 this.selectFeatureMap.delete(id);
		 }
	 }
}

VFG.Viewer.prototype.getFeatureById=function(id){
	var _this=this;
	if(_this.containFeature(id)){
		 return _this.cacheFeatureMap.get(id);
	}
}

VFG.Viewer.prototype.containFeature=function(id){
	return this.cacheFeatureMap.has(id)
}

VFG.Viewer.prototype.updateFeature=function(option){
	var _this=this;
	if(option.id){
		if(_this.containFeature(option.id)){
			var feature=_this.getFeatureById(option.id);
			if(feature){
				feature.name=option.name||feature.name;
				feature.code=option.code||feature.code;
				feature.isView=option.isView||feature.isView;
				feature.type=option.type||feature.type;
				feature.x=option.x||feature.x
				feature.y=option.y||feature.y
				feature.z=option.z||feature.z
				feature.dimensionsX=option.dimensionsX||feature.dimensionsX
				feature.dimensionsY=option.dimensionsY||feature.dimensionsY
				feature.dimensionsZ=option.dimensionsZ||feature.dimensionsZ
				feature.rotationX=option.rotationX||feature.rotationX
				feature.rotationY=option.rotationY||feature.rotationY
				feature.rotationZ=option.rotationZ||feature.rotationZ
				feature.normalColor=option.normalColor||feature.normalColor;
				feature.hoverColor=option.hoverColor||feature.hoverColor;
				feature.heading=option.heading||feature.heading;
				feature.pitch=option.pitch||feature.pitch;
				feature.roll=option.roll||feature.roll;
				feature.cameraX=option.cameraX||feature.cameraX;
				feature.cameraY=option.cameraY||feature.cameraY;
				feature.cameraZ=option.roll||feature.cameraZ;
				_this.cacheFeatureMap.set(option.id,feature);
				
				if(option.name){
					VFG.Feature.update({
						id:option.id,
						name:option.name||'',
					});
				}
				
				if(option.isView){
					VFG.Feature.update({
						id:option.id,
						isView:option.isView,
					});
				}
				
				if(option.x && option.y && option.z){
					VFG.Feature.update({
						id:option.id,
						position:{
							x:option.x,
							y:option.y,
							z:option.z,
						},
					});
				}
				
				if(option.dimensionsX && option.dimensionsY && option.dimensionsZ){
					VFG.Feature.update({
						id:option.id,
						dimensions:{
							x:option.dimensionsX||10,
							y:option.dimensionsY||10,
							z:option.dimensionsZ||10,
						},
					});
				}
				
				if(option.rotationX && option.rotationY && option.rotationZ){
					VFG.Feature.update({
						id:option.id,
						rotation:{
							x:option.rotationX||0,
							y:option.rotationY||0,
							z:option.rotationZ||0,
						},
					});
				}
				
				if(option.normalColor){
					VFG.Feature.update({
						id:option.id,
						normalColor:option.normalColor,
					});
				}
				if(option.hoverColor){
					VFG.Feature.update({
						id:option.id,
						hoverColor:option.hoverColor,
					});
				}
			}else{
				throw new Error("未找到对象!");
			}
		}else{
			 throw new Error("未找到对象!");
		}
	}else{
	   throw new Error("参数有误!");
	}
}


