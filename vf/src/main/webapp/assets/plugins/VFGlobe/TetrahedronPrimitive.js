function TetrahedronPrimitive(viewer,options){
	this.viewer=viewer;
	this.ctx=options.ctx;
	this.color=options.color;
	this.silhouetteColor=options.silhouetteColor;
    var position =Cesium.Cartesian3.fromDegrees(options.x*1,options.y*1,options.z*1);
    var heading = Cesium.Math.toRadians(0);
    var pitch = 0;
    var roll = 0;
    var hpr = new Cesium.HeadingPitchRoll(heading, pitch, roll);
    var orientation = Cesium.Transforms.headingPitchRollQuaternion(position, hpr);
    this.entity = this.viewer.entities.add({
        position: position,
        orientation: orientation,
        model: {
            uri: this.ctx+"/assets/plugins/Cesium/Assets/Images/1.gltf",
            minimumPixelSize: 128,
            maximumScale: 20000,
            runAnimations:true,
            clampAnimations:true,
            scale:50,
            color: Cesium.Color.fromCssColorString(this.color),
			silhouetteColor: Cesium.Color.fromCssColorString(this.silhouetteColor),		
			silhouetteSize:1,	
        }
    });
    var _this=this;
    var i=0;
    this.Interval=setInterval(function(){
        var heading = Cesium.Math.toRadians(i);
        var pitch = 0;
        var roll = 0;
        var hpr = new Cesium.HeadingPitchRoll(heading, pitch, roll);
        var orientation = Cesium.Transforms.headingPitchRollQuaternion(position, hpr);
        _this.entity.orientation =orientation;
        if(i>=360){
        	i=0;
        }
        i+=1.5
    },1);//1000为1秒钟
    
    let r1 = 50;
    let r2 = 50;
    this.circle =this.viewer.entities.add({
        position:position,
        ellipse: {
            semiMinorAxis: new Cesium.CallbackProperty(function () {
                r1 = r1 + 1;
                if (r1 >= 100) {
                    r1 = 0;
                }
                return r1;
            }, false),
            semiMajorAxis: new Cesium.CallbackProperty(function () {
                r2 = r2 + 1;
                if (r2 >= 100) {
                    r2 = 0;
                }
                return r2;
            }, false),
            height: options.z*1,
            material: new Cesium.ImageMaterialProperty({
                image:_this.ctx+'/assets/plugins/Cesium/Assets/Images/redCircle2.png',
                repeat: new Cesium.Cartesian2(1.0, 1.0),
                transparent: true,
                color:  Cesium.Color.fromCssColorString(this.color),
            })
        }
    })
}
TetrahedronPrimitive.prototype.destroy = function () {
  if(this.viewer&&this.entity){
	  this.viewer.entities.remove(this.entity);
  }
  if(this.viewer&&this.circle){
	  this.viewer.entities.remove(this.circle);
  }
  clearInterval(this.Interval);
  delete this.options,
  delete this.viewer;
  return Cesium.destroyObject(this);
};
