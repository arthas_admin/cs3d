var treeSelect;
var viewer;
var globe;
var defaultVFGPreview;
layui.config({
    version: true,   
    base: '../assets/module/'
}).extend({
    formSelects: 'formSelects/formSelects-v4',
    treetable: 'treetable-lay/treetable',
    dropdown: 'dropdown/dropdown',
    notice: 'notice/notice',
    step: 'step-lay/step',
    dtree: 'dtree/dtree',
    citypicker: 'city-picker/city-picker',
    tableSelect: 'tableSelect/tableSelect',
    Cropper: 'Cropper/Cropper',
    zTree: 'zTree/zTree',
    introJs: 'introJs/introJs',
    fileChoose: 'fileChoose/fileChoose',
    tagsInput: 'tagsInput/tagsInput',
    CKEDITOR: 'ckeditor/ckeditor',
    Split: 'Split/Split',
    cascader: 'cascader/cascader',
	treeSelect: 'treeSelect/treeSelect' 
}).use(['layer', 'element', 'config', 'index', 'admin','carousel', 'laytpl','treeSelect','contextMenu','tableX','util','form','treetable'], function () {
    var $ = layui.jquery;
    var layer = layui.layer;
    var element = layui.element;
    var config = layui.config;
    var index = layui.index;
    var admin = layui.admin;
    var carousel = layui.carousel;
	treeSelect= layui.treeSelect;
    var laytpl = layui.laytpl;
    
    var globe= new VFG.Viewer({
    	domId: 'cesiumContainer',
    	url:ctx,
    	ws:'ws://'+window.location.host+ctx+'/websocket',
    	sceneId:id,
    	geocoder:true,
    	complete:function(e){
    		viewer=e.globe.viewer;
    		preview=new VFGPreview(viewer,{
    			sceneId:id
    		});
    		e.globe.MOUSE_MOVE_PRIMITIVE(function(e){
    			console.log(e);
    		});
    		
    		e.globe.LEFT_CLICK_PRIMITIVE(function(e){
    			console.log(e);
    		});
    	}
    });
});


var _Ajax = function(reqType, url, data, dataType, callback) {
	$.ajax({
		type: reqType,
		url: encodeURI(encodeURI(url)),
		data: data,
		dataType: dataType ? dataType : 'html',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		async: false,
		cache: false,
		success: function(response) {
			if (callback != null) {
				callback(response);
			}
		}
	});
}
