var viewer;
var globe;
var defaultVFGPreview;
layui.use(['layer'], function () {
    var $ = layui.jquery;
    var layer = layui.layer;
	var ellipsoid = Cesium.Ellipsoid.WGS84;
	globe = new VFG.Globe({
		domId: 'cesiumContainer',
		geocoder: false,
		imageryProvider:new Cesium.TileMapServiceImageryProvider({
			url:ctx+"/assets/plugins/Cesium/Assets/Textures/NaturalEarthII",
		})
	});
    var viewer=globe.viewer;
    if(id){
        var res = common_ajax.ajaxFunc(ctx+"/biz/scene/getSceneById",{sceneId:id}, "json", null);
        console.log(res);
        if (undefined!=res && 200==res.code) {
        	var data=res.data;
        	var scene=data.scene;
        	var maps=data.maps;
        	var models=data.models;
        	var points=data.points;
        	var polygons=data.polygons;
        	var polyLines=data.polyLines;
        	$("#themeDivId").load(scene.pageUrl);
        	if(scene.cameraX && scene.cameraX && scene.cameraZ && scene.heading && scene.pitch && scene.roll){
        		var lnla=VFG.Util.getC3ToLnLa(viewer, new Cesium.Cartesian3(scene.cameraX*1,scene.cameraY*1,scene.cameraZ*1));
                var ln=lnla.x;
                var la=lnla.y;
                var alt=lnla.z;
        		var x=0;y=0;z=0;
        		x=ln+360;
                var interVal = window.setInterval(function() {
                	if(x>ln){
                		x--;
                        viewer.scene.camera.setView({
                            destination: new Cesium.Cartesian3.fromDegrees(x, y, z || 20000000)
                         });
                	}else{
                        clearInterval(interVal);
                        viewer.camera.flyTo({
                            destination: new Cesium.Cartesian3(scene.cameraX*1,scene.cameraY*1,scene.cameraZ*1),
                            complete: function () {
                        		viewer.scene.camera.flyTo({
                        	        duration:1,
                        			destination: new Cesium.Cartesian3(scene.cameraX*1,scene.cameraY*1,scene.cameraZ*1),
                        			orientation:{
                                		heading:scene.heading*1,
                                		pitch: scene.pitch*1,
                                		roll: scene.roll*1,
                                	}
                        		});
                            }
                        });
                        
//                       var modelVideo = new ModelVideo(viewer,{
//                    	   modelUrl:'http://39.99.162.142/model/gf/r.obj',
//                    	   videoUrl:'http://39.99.162.142/model/gf/r.mp4',
//                    	   modelScale:20,
//                    	   position:{x:103.4033454165169,y:23.413689920434148,z:0},
//                    	   rotation:{x:90,y:0,z:0}
//                       }) 
                	}
                }, 8);
        	}
            if(maps!=null && maps.length>0){
            	VFG.Provider.addImageryProvidersFromServ(viewer,maps)
            }else{
                var imageryProviderOpts={
                    url:'https://mt1.google.cn/vt/lyrs=s&hl=zh-CN&x={x}&y={y}&z={z}&s=Gali',
                    minimumLevel:1,
                    maximumLevel:20
                 };
                var imgProvider=VFG.Provider.createUrlTemplateImageryProvider(imageryProviderOpts);
                VFG.Provider.addImageryProvider(viewer,imgProvider);
            }
            
            if(models!=null && models.length>0){
            	VFG.Model.addModelProvidersFromServ(viewer,models);
            }
            
            if(polygons!=null && polygons.length>0){
            	for(var i=0;i<polygons.length;i++){
            		VFG.Util.polygonForCreate(viewer,polygons[i])
            	}
            }
            if(polyLines!=null && polyLines.length>0){
            	for(var i=0;i<polyLines.length;i++){
            		VFG.Util.polyLineForCreate(viewer,polyLines[i])
            	}
            }
            
        }
    }
    var imgProvider=VFG.Provider.createUrlTemplateImageryProvider(imageryProviderOpts);
    VFG.Provider.addImageryProvider(viewer,new VFGPointProvider(viewer,{
    	url:ctx+'/biz/marker/point/pointForGrid',
    	sceneId:id,
    	ctx:ctx,
    	sceneId:id,
    	alpha:0,
    	level:11,
    }));
	
    
});


var _Ajax = function(reqType, url, data, dataType, callback) {
	$.ajax({
		type: reqType,
		url: encodeURI(encodeURI(url)),
		data: data,
		dataType: dataType ? dataType : 'html',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		async: false,
		cache: false,
		success: function(response) {
			if (callback != null) {
				callback(response);
			}
		}
	});
}
